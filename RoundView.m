#import "RoundView.h"

@implementation RoundView

- (id)initWithFrame:(NSRect)frameRect
{
	if ((self = [super initWithFrame:frameRect]) != nil) {
		// Add initialization code here
	}
	return self;
}

- (void)drawRect:(NSRect)rect
{
	[super drawRect:(NSRect)rect];
	[[NSApp delegate] drawRoundView:rect];

}

@end
