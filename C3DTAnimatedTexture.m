//
//  C3DTAnimatedTexture.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Fri May 30 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTAnimatedTexture.m,v 1.8 2003/07/08 11:19:46 pmanna Exp $
//
// $Log: C3DTAnimatedTexture.m,v $
// Revision 1.8  2003/07/08 11:19:46  pmanna
// Modified to use a more generic generateTexture
//
// Revision 1.7  2003/06/29 20:22:40  pmanna
// Moved nextPowerOf2 to public
//
// Revision 1.6  2003/06/22 09:39:36  pmanna
// Refactoring of texture objects to consolidate common tasks
//
// Revision 1.5  2003/06/19 20:36:33  pmanna
// Added Apple extension to optimize memory usage
//
// Revision 1.4  2003/06/15 12:46:32  pmanna
// Added alpha channel and color keying
//
// Revision 1.3  2003/06/15 09:41:48  pmanna
// Moved all texture initialization here
// Finally(!) found a way to make textures use alpha channel
//
// Revision 1.2  2003/06/11 21:29:36  pmanna
// Corrected updateTexture to reverse mapping
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTAnimatedTexture.h"

@interface C3DTAnimatedTexture (PrivateMethods)
- (BOOL)_loadMovie: (NSString *)movieFile;
- (BOOL)_setupOffscreenGWorld;
- (void)_updateTexture;
@end


@implementation C3DTAnimatedTexture

+ (C3DTAnimatedTexture *)textureWithMovie: (NSString *)aName interval: (NSTimeInterval)anInterval alpha: (BOOL)alphaFlag
{
    return [[[C3DTAnimatedTexture alloc] initWithMovie: aName interval: anInterval alpha: alphaFlag] autorelease];
}


- (id)initWithMovie: (NSString*)aName interval: (NSTimeInterval)anInterval alpha: (BOOL)alphaFlag
{
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];

    if (aName == nil)
        return nil;

    if (self = [super initWithName: aName]) {
        _alphaMask = 0xFFFFFFFF;	// Initial color insures there will be no color masking
        _color.x = _color.y = _color.z = _color.w = 1.0;

        moviePaused = YES;
        if (_useAlpha = alphaFlag)
            _textureFormat = GL_RGBA;
        else
            _textureFormat = GL_RGB;
        
        if ([self _loadMovie: [resourcePath stringByAppendingFormat:@"/%@", aName ]] &&
            [self _setupOffscreenGWorld])
        {
            [self generateTextureWithBytes: _textureBytes refresh: NO];
        }

        [self setAnimateInterval: anInterval];
    }

    return self;
}


- (void)dealloc
{
    [self stop];
    
    UnlockPixels( gWorldPixMap );
    DisposeGWorld( offscreenGWorld );
    free(gWorldMem);
    DisposeMovie(theMovie);
    CloseMovieFile(movieRefNum);
    
    [super dealloc];
}

- (NSTimeInterval)animateInterval
{
    return _animateInterval;
}

- (void)setAnimateInterval: (NSTimeInterval)anInterval
{
    _animateInterval = anInterval;
}

- (void)setColor: (_C3DTVector)aColor
{
    [super setColor: aColor];
    
    _alphaMask = ((GLuint)(aColor.x * 255.0) << 24) | ((GLuint)(aColor.y * 255.0) << 16) |
        ((GLuint)(aColor.z * 255.0) << 8);
}

- (void)setColorRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    [super setColorRed: redColor green: greenColor blue: blueColor alpha: alphaValue];
    
    _alphaMask = ((GLuint)(redColor * 255.0) << 24) | ((GLuint)(greenColor * 255.0) << 16) |
        ((GLuint)(blueColor * 255.0) << 8);
}

- (BOOL)useAlpha {
    return _useAlpha;
}

- (void)setUseAlpha:(BOOL)newUseAlpha {
    _useAlpha = newUseAlpha;
}

- (void)start
{
    if (moviePaused) {
        StartMovie(theMovie);
        moviePaused = NO;
    }

    if (!_animateTimer && (_animateInterval > 0.0)) {
        _animateTimer	= [[NSTimer scheduledTimerWithTimeInterval: _animateInterval
                                                          target: self
                                                        selector: @selector( doAnimate: )
                                                        userInfo: nil
                                                         repeats: YES] retain];
        [[NSRunLoop currentRunLoop] addTimer: _animateTimer
                                     forMode: NSEventTrackingRunLoopMode];
        [[NSRunLoop currentRunLoop] addTimer: _animateTimer
                                     forMode: NSModalPanelRunLoopMode];
    }
}

- (void)stop
{
    if (!moviePaused) {
        StopMovie( theMovie );
        moviePaused = YES;
    }
    
    if (_animateTimer) {
        [_animateTimer invalidate];
        [_animateTimer release];
        _animateTimer = nil;
    }
}


- (void)doAnimate: (NSTimer *)timer
{
    // Tell interested views to refresh
    [[NSNotificationCenter defaultCenter] postNotificationName: C3DT_REFRESH object: self userInfo: nil];
}

- (void)apply
{
    glGetFloatv(GL_CURRENT_COLOR, (GLfloat *)&_savedColor);

    glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint *)&_savedTextureId);

    if(IsMovieDone(theMovie))
        GoToBeginningOfMovie(theMovie);
    
    glBindTexture(GL_TEXTURE_2D, _textureId);

    // Refresh the texture with the actual movie image
    if( !moviePaused )
        [self _updateTexture];
}

- (void)restore
{
    glBindTexture(GL_TEXTURE_2D, _savedTextureId);
    glColor4fv((GLfloat *)&_savedColor);
}


- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _animateInterval forKey: @"AnimatedTextureInterval"];
    [coder encodeBool: _useAlpha forKey: @"AnimatedTextureUseAlpha"];
}

- (id)initWithCoder:(NSCoder *)coder
{
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    
    self = [super initWithCoder: coder];

    _animateInterval	= [coder decodeFloatForKey: @"AnimatedTextureInterval"];
    _useAlpha			= [coder decodeBoolForKey: @"AnimatedTextureUseAlpha"];
    
    _alphaMask = ((GLuint)(_color.x * 255.0) << 24) | ((GLuint)(_color.y * 255.0) << 16) |
        ((GLuint)(_color.z * 255.0) << 8);

    if (_useAlpha)
        _textureFormat = GL_RGBA;
    else
        _textureFormat = GL_RGB;
    
    if ([self _loadMovie: [resourcePath stringByAppendingFormat:@"/%@", [self name]]] &&
        [self _setupOffscreenGWorld])
    {
        moviePaused = YES;

        [self generateTextureWithBytes: _textureBytes refresh: NO];
    }

    [self setAnimateInterval: _animateInterval];
    
    return self;
}

@end

@implementation C3DTAnimatedTexture (PrivateMethods)

- (BOOL)_loadMovie: (NSString *)movieFile
{
    CFURLRef	movieURLRef;
    FSRef		urlFSRef;
    FSSpec		movieFileSpec;

    movieURLRef = CFURLCreateFromFileSystemRepresentation( kCFAllocatorDefault, [movieFile cString],
                                                           [movieFile length], FALSE );
    if (movieURLRef == NULL)
    {
        return NO;
    }
    
    if (!CFURLGetFSRef(movieURLRef, &urlFSRef))
    {
        return NO;
    }
    
    if (FSGetCatalogInfo(&urlFSRef, kFSCatInfoNone, NULL, NULL, &movieFileSpec, NULL ) != noErr)
    {
        return NO;
    }
    CFRelease( movieURLRef );

    if( OpenMovieFile( &movieFileSpec, &movieRefNum, fsRdPerm ) != noErr)
    {
        return NO;
    }

    if (NewMovieFromFile( &theMovie, movieRefNum, NULL, NULL, 0, NULL ) != noErr)
    {
        return NO;
    }

    // Make sure rect starts at 0,0
    GetMovieBox( theMovie, &movieBox );
    OffsetRect( &movieBox, -movieBox.left, -movieBox.top );

    // Set the scaling of the movie
    movieBox.right = nextPowerOf2(movieBox.right);
    movieBox.bottom = nextPowerOf2(movieBox.bottom);
    
    SetMovieBox( theMovie, &movieBox );

    _textureSize.width = (float)movieBox.right;
    _textureSize.height = (float)movieBox.bottom;
    
    return YES;
}


- (BOOL)_setupOffscreenGWorld
{
    int	bytesPerPixel;
    
    if (_useAlpha)
        bytesPerPixel = 4;
    else
        bytesPerPixel = 3;
    
    if (!(gWorldMem = calloc(movieBox.right * movieBox.bottom * bytesPerPixel, 1)))
    {
        return NO;
    }

    QTNewGWorldFromPtr( &offscreenGWorld, (_useAlpha ? k32ARGBPixelFormat: k24RGBPixelFormat), &movieBox,
                        NULL, NULL, 0, gWorldMem, movieBox.right * bytesPerPixel );
    if( offscreenGWorld == NULL )
    {
        return NO;
    }
    
    SetMovieGWorld(theMovie, offscreenGWorld, NULL);
    gWorldPixMap = GetGWorldPixMap(offscreenGWorld);
    LockPixels(gWorldPixMap);
    gWorldPixBase = GetPixBaseAddr(gWorldPixMap);

    // This is the reordered buffer
    _textureBytes = calloc( movieBox.right * movieBox.bottom * bytesPerPixel, 1 );
    
    return TRUE;
}

- (void)_updateTexture
{
    register int		rowIndex;

    MoviesTask( theMovie, 0 );

    if (_useAlpha) {
        register int		colIndex;
        register uint32_t	*readPos = (uint32_t *)gWorldPixBase;
        register uint32_t	*changePos = (uint32_t *)_textureBytes + ((movieBox.bottom -1) * movieBox.right);
        
        // Swap alpha byte to the end, so ARGB become RGBA; note this is
        // big endian-centric.
        for(rowIndex = 0;
            rowIndex < movieBox.bottom;
            rowIndex++, changePos -= movieBox.right * 2) {
            for (colIndex = 0;
                 colIndex < movieBox.right;
                 colIndex++, readPos++, changePos++) {
                // Filter so that defined color is alpha = 0.0
                if ((*changePos = (*readPos & 0xFFFFFF) << 8) != _alphaMask)
                    *changePos |= ((*readPos >> 24) & 0xFF);
            }
        }
    } else {
        register int	destRowIndex;
        register int	bytesPRow = 3 * movieBox.right;
        
        for (rowIndex = movieBox.bottom - 1, destRowIndex = 0;
             rowIndex >= 0;
             rowIndex--, destRowIndex++)
        {
            // Copy the entire row in one shot
            memcpy( _textureBytes + (destRowIndex * bytesPRow),
                    gWorldPixBase + (rowIndex * bytesPRow),
                    bytesPRow);
        }
    }
    
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,  _textureSize.width,
                    _textureSize.height, _textureFormat, GL_UNSIGNED_BYTE,
                    _textureBytes);
}


@end
