//
//  C3DTInteractiveView.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue Jun 24 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTInteractiveView.m,v 1.2 2003/06/29 13:56:47 pmanna Exp $
//
// $Log: C3DTInteractiveView.m,v $
// Revision 1.2  2003/06/29 13:56:47  pmanna
// Added object picking
//
// Revision 1.1  2003/06/24 22:29:03  pmanna
// Initial release
//
//


//#import "MNCFEntity.h"
#import <IOKit/IOCFBundle.h>
//#import <CoreServices/CoreServices.h>
#import <Carbon/Carbon.h>

#import "FlowView.h"
#import "MNCoverFlow.h"

#import "C3DTTypes.h"

#import "C3DTGroup.h"
#import "C3DTPlainSurface.h"
#import "C3DTTexture.h"
#import "C3DTSpotLight.h"
#import "MNCFCover.h"
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#include <GLUT/glut.h>
#import "C3DTSpotLight.h"
#import "ScrollerView.h"
#import "Functions.h"
#import "MNNodeObject.h"
#import "C3DTTypes.h"

#define ICONCACHE 50
#define ICON_OBJECT_SIZE 64
#define ICON_TEXTURE_SIZE 128

#define COVER_PER_ROUND 29
#define FRAME_WIDTH 120/17*29
#define MOVE_PAGE 10
// 100/17 * COVER_PER_ROUND


#define ALPHA_THRESHOLD 129
#define USE_SPOTLIGHT 1

#define EVENT_LIMITTER 0.001

#define TEXTURE_DICTIONARY_BUFFER_SIZE 100

extern double SIDE_ANGLE;
extern double Z_FRONT;
extern double DURATION1;
extern double SIDE_SPACE_FACTOR;

_C3DTVector MNVectorMultiply( const _C3DTMatrix m, const _C3DTVector v);


@implementation FlowView

-(void)awakeFromNib
{
	
	imageDictionary = [[NSMutableDictionary alloc] init];
	lock = [[NSLock alloc] init];

	
	//[self setup];
	needsSetup = YES;
	
	
	[NSThread detachNewThreadSelector:@selector(textureUpdateThread:) toTarget:self withObject:nil];	

	
	
    
   // [self _fillScene];
}

-(void)textureUpdateThread:(id)obj
{
	[NSThread setThreadPriority:0.1];

	while(1)
	{
		[lock lock];
		int count = [imageDictionary count];

		if( count > 0 )
		{
			NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

			NSNumber * coverIndex = [[imageDictionary allKeys] lastObject] ;

			//NSImage *image = [imageDictionary objectForKey:coverIndex];
			//NSLog(@"getting %@",[coverIndex description]);
			
			int n = Calc_n(R,[coverIndex doubleValue]);
			
			MNNodeObject *obj = [nodes objectAtIndex:n];
			
			if( ([obj style] == unknownTexture) ||  ([obj style] == nil) )
			{
				[lock unlock];
				[obj performSelectorOnMainThread:@selector(updateTexture:) withObject:nil waitUntilDone:YES] ;
				[lock lock];

			}
			
			
			[imageDictionary removeObjectForKey: coverIndex ];

			
			[pool release];

		}else
		{
			usleep(500000);
		}
		[lock unlock];


		usleep(30000*busyStatus + 100 );
	}
}

-(void)dealloc
{

	[super dealloc];
}



- (void)setFrame:(NSRect)frameRect
{
	
	//bounds width 400
	NSRect b = frameRect;
	b.size.width = 400;
	b.size.height = frameRect.size.height/ frameRect.size.width * 400;
	
	needsCleaning = YES;


	[super setFrame:frameRect];

	
	if(  ((frameRect.size.height < 120)||(frameRect.size.width < 250)) && !smallSize )
	{
		smallSize = YES;
		[scrollerView setFrame:NSMakeRect(0,5, frameRect.size.width,32)];
		[scrollerView setSmallSize:YES];
		
	}else if( !((frameRect.size.height < 120)||(frameRect.size.width < 250)) && smallSize )
	{
		smallSize = NO;
		[scrollerView setFrame:NSMakeRect(0,10, frameRect.size.width,32)];
		[scrollerView setSmallSize:NO];

		
	}
		
		
	[self reshape];
	[self display];
}

-(void)resetNodeObjects
{
	double delta_angle = -2*pi/R;
	double currentAngle = pi;

	CoverCoordinate zc;
	zc.angle = 0;
	zc.z = 0;
	zc.rotation = 0;
	zc.spacing=0;
	zc.r=1;
	
	int num;
	for( num = 0; num < R ; num ++ )
	{

		MNNodeObject*node=[nodes objectAtIndex:num];
		
		[node setRotationX: 0.0 Y: 0.0 Z: 0.0];
		[node setTranslationX: 0 Y: 0 Z: 0];
		
		
		//setup MNNodeObject 
		[node setNmax: Nmax_plus_1 -1];
		[node setR: R];
		
		CoverCoordinate c;
		c.angle = currentAngle;
		c.r = 0.0;
		
		[node setValue:[NSNumber numberWithInt:num] forKey:@"n"];
		
		[node setCoordinate: zc ];
		[node addCoordinate: c ];
		
		if( num == 0 )
		{
			[node setZ:  Z_FRONT ];
			[node setRotation:  0.0 ];
			[node setSpacing: SIDE_SPACE_FACTOR];
			
		}else
		{
			[node setZ:  0 ];
			[node setRotation:  SIDE_ANGLE ];
			[node setSpacing: 0.0];
			
		}
		
		currentAngle += delta_angle;
	}

	
}


-(NSArray*)nodes
{
	return nodes;	
}

-(double)Ns
{
	return Ns;	
}


-(double)Ns_previous
{
	return Ns_previous;	
}


-(int)R
{
	return R;	
}

-(double)Ncenter
{
	return Ncenter;	
}

-(void)resetSpacing
{
	int hoge;
	for( hoge = 0; hoge <[nodes count]; hoge++ )
	{
		MNNodeObject *node = [nodes objectAtIndex:hoge];
		if( [node N] == Ns )
		{
			[node setSpacing:SIDE_SPACE_FACTOR];
			[node setRotation:0];
			[node setZ: Z_FRONT];

		}
		else if( [node N] < Ns )
		{
			[node setRotation:-SIDE_ANGLE];
			[node setSpacing:0];
			[node setZ: 0];

		}else
		{
			[node setRotation: SIDE_ANGLE];
			[node setSpacing:0];
			[node setZ: 0];


		}
	}
	
}

- (BOOL)animationShouldStart:(NSAnimation*)animation
{
	if( animation == fullscreenAnimation )
	{
	}else
	{
		NSImage *image = [dataSource imageForCoverAtIndexImmedeately: (unsigned)Ns];
		
		if( image )
		{
			C3DTTexture *txt = [[[C3DTTexture alloc] initWithBitmapImageRep:[self iconRepForImage:image] ] autorelease];
			[textureDictionary setObject:txt forKey:[NSNumber numberWithDouble:Ns]];
			
			if( [textureDictionary count] > TEXTURE_DICTIONARY_BUFFER_SIZE ) 
			{
				
				[textureDictionary removeObjectForKey: [[textureDictionary allKeys] objectAtIndex:0 ]];
			}
		}
		
		busyStatus = 2;
	}
	
	return YES;
}



- (void)animationDidEnd:(AngleAnimation*)anAnimation
{

	if( anAnimation == fullscreenAnimation )
	{
		//convert screen frame to root view frame
		
		NSDisableScreenUpdates();
		
		NSView* view = [[[fullWindow contentView] subviews] objectAtIndex:0];
		
		[view retain];
		[view removeFromSuperview];
		[attachedWindowTargetOriginalParentView addSubview: view];
		[view release];
		
		[view setFrame: attachedWindowTargetOriginalRect];
		NSEnableScreenUpdates();
		
		[fullWindow orderOut:nil];
		
		[[self window] display];
		[[self window] makeKeyWindow];
	}
	else
	{
		
		//Ncenter += [animation Nprogress];
		Ncenter = Ns;
		
		[self resetSpacing];
		
		busyStatus = 0;
		//[self performSelectorOnMainThread:@selector(updateTextures) withObject:nil waitUntilDone:NO];
		
		//update selected texture if needed.
		
		//NSLog(@"animationDidEnd");
		if( [[nodes objectAtIndex:Calc_n(R,Ns) ] style] ==unknownTexture )
		{
			//NSLog(@"update texture");
			[[nodes objectAtIndex:Calc_n(R,Ns) ] performSelectorOnMainThread:@selector(updateTexture:) withObject:nil waitUntilDone:YES];

		}
		[[self window] performSelectorOnMainThread:@selector(display) withObject:nil waitUntilDone:YES];

	}
	//ConvertToLinear_ResetPiPosition(-100,100);
}

- (void)animationDidStop:(AngleAnimation*)animation
{
	busyStatus = 2;
	
//NSLog(@"animation stopped [animation Nprogress] %f",[animation Nprogress]);
//NSLog(@"animation stopped current progress %f",[animation currentProgress]);

	Ncenter += [animation Nprogress];
	
//NSLog(@"New Ncentre %f",Ncenter);
	
	
	
	//debug,.. find actual Ncenter
	
	int left=0;
	int right=0;
	double rightAngle =0;
	double leftAngle  =2*pi;
	int hoge;
	for( hoge = 0; hoge <[nodes count]; hoge++ )
	{
		MNNodeObject *node = [nodes objectAtIndex:hoge];
		double nodeAngle = [node coordinate].angle;
	//NSLog(@"nodeAngle %f",nodeAngle);
		if( ( nodeAngle <= leftAngle) && ( nodeAngle >= pi) )
		{
			leftAngle = nodeAngle;
			left = hoge;
		}
		
		if( ( nodeAngle >= rightAngle) && ( nodeAngle <= pi) )
		{
			rightAngle = nodeAngle;
			right = hoge;
		}
	}
	
//NSLog(@"Right node N %d", right);
	
//NSLog(@"Left node N %d", left);
	
//NSLog(@"leftAngle - rightAngle %f = %f",(leftAngle - rightAngle), 2*pi/R);
//NSLog(@"Calculated N %f", (leftAngle-pi)/(2*pi/R) + [[nodes objectAtIndex:left] N]);
	
	Ncenter = (leftAngle-pi)/(2*pi/R) + [[nodes objectAtIndex:left] N];

}
-(void)updateView
{
	
	int hoge;
	
	for( hoge = 0; hoge < [nodes count]; hoge++)
	{
		MNNodeObject *node = [nodes objectAtIndex:hoge];
		double linearX = ConvertToLinear( 0.0,  [node coordinate],  nodes, 
										  Ns,  
										  Ns_previous ,  -FRAME_WIDTH, FRAME_WIDTH );
		
		if( hoge == 0 )
		{
		//NSLog(@"node 0 : %f", linearX);
		}
		
		[node setTranslationX:linearX Y:0 Z:[node coordinate].z ];
		
		
		[node setRotationX:90 Y:0 Z: (float)[node coordinate].rotation*360/(2*pi)];
	}
	
	
	
	[self setNeedsDisplay:YES];
}
- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	//NSLog(@"menu for event");
	if( [delegate respondsToSelector:@selector(flowView:menuForEvent:)] )
	{
		return [delegate flowView:self menuForEvent:theEvent];
	}
	
	return nil;
	
}
- (void)mouseDown:(NSEvent *)anEvent
{
    NSPoint		location	= [self convertPoint: [anEvent locationInWindow]
                                  fromView: [[self window] contentView]];
    C3DTCamera	*theCamera	= [self camera];

	if (theCamera) {
		[theCamera setSelectionMode: YES];

		[theCamera setSelectionPoint: location];
		[theCamera setSelectionSize: NSMakeSize(1.0, 1.0)];

		[[self scene] useWith: theCamera];

		
		[theCamera setSelectionMode: NO];
		
		
		//select 
		
		NSArray* selectedObjects = [[self scene] selectedObjects];
		
		

		//object selection with double cick detection
		MNNodeObject		*selectedObj;

			NSEnumerator	*enumSelection = [selectedObjects objectEnumerator];
			//MNNodeObject		*clickedObj = nil;
			
			
			while ((selectedObj = (C3DTGroup *)[enumSelection nextObject]) != nil)
			{
				if( [selectedObj isKindOfClass:[MNNodeObject class]] )
				{
					/*
					unsigned char* byte = [[selectedObj style] textureBytes];
					
					
					
					C3DTPlainSurface* aGeometry = [selectedObj geometry];

					_C3DTVector vector = [self convertFromWindowCoords:location];
					NSLog(@"1 x %f, y %f, z %f",vector.x, vector.y, vector.z);

					
					_C3DTMatrix m = matrixIdentity();
					

					matrixTranslate( &m, -[selectedObj translation].x, -[selectedObj translation].y, -[selectedObj translation].z);

				
					
					_C3DTVector newVector = MNVectorMultiply(  m,vector);
					
					NSLog(@"2 x %f, y %f, z %f",newVector.x, newVector.y, newVector.z);
					double rotation = [selectedObj rotationAngle];
					NSLog(@"2 x %f, y %f, z %f", newVector.x / cos(-rotation), newVector.y, newVector.z );

					///					NSLog(@,vector.x, vector.y, vector.z);

					NSLog(@"alpha %d",[self alphaInBitmap   :[ (C3DTTexture*)[selectedObj style] textureBytes] atX:newVector.x+ICON_TEXTURE_SIZE/2 Y:newVector.y +ICON_TEXTURE_SIZE/2]);
					
					if( [self alphaInBitmap   :[ (C3DTTexture*)[selectedObj style] textureBytes] atX:newVector.x+ICON_TEXTURE_SIZE/2 Y:newVector.y +ICON_TEXTURE_SIZE/2] >ALPHA_THRESHOLD )
						
						*/
					{
						//Ns_previous = Ns;
						double clickedNs = [(MNNodeObject*)selectedObj N];
						//NSLog(@"clickedNs %1f",clickedNs);

						if( Ns != clickedNs )
						{
							int keymod = GetCurrentKeyModifiers();
							if(  (keymod | 1024) == (2048 | 1024) ) // option
							{
										[self moveRowWithoutAnimation: (int)(clickedNs - Ns)];
							}else
							{
							
								[self moveRow: (int)(clickedNs - Ns)];
							}
						}

						
						
						if( (Ns == clickedNs) &&( [anEvent clickCount] >=2  ) )
						{
							if( [delegate respondsToSelector:@selector(flowView:doubleClickedCoverAtIndex:)] )
							[delegate flowView:self doubleClickedCoverAtIndex:Ns];
						}
						break; //only one
					
					}
					 
				}
				
			}
		
		
		/*
		
		//'click' the object
		if( !([anEvent modifierFlags] & NSCommandKeyMask) )
		{
			NSEnumerator	*enumSelection = [selectedObjects objectEnumerator];
			C3DTGroup		*selectedObj;
			
			
			while ((selectedObj = (C3DTGroup *)[enumSelection nextObject]) != nil)
			{
				if( [selectedObj isKindOfClass:[MNNodeObject class]] )
				{
					//Ns_previous = Ns;
					double clickedNs = [(MNNodeObject*)selectedObj N];
					
					if( Ns != clickedNs )
					{
					//NSLog(@" Ns - Ns_previous %f", clickedNs - Ns);
						//[self moveRow: (int)(clickedNs - Ns)];
					}
					break; //only one
				}
				
			}
		}
*/

        [self setNeedsDisplay: YES];
		
    }else
	{
		
	}
    
    [super mouseDown: anEvent];
	
}
/*
-(BOOL)isOpaque
{
	return YES;	
}*/
-(void)setNeedsCleaning:(BOOL)flag
{
	
	needsCleaning = flag;	
	
	long  order = 0;
	//[[self openGLContext] setValues:&order forParameter: NSOpenGLCPSurfaceOpacity];
	
}
-(void)drawRect:(NSRect)rect
{
	if( needsSetup ) [self setup];
	
	if( needsCleaning  )
	{
		// NSRect bounds = [self bounds];
		//	//NSLog(@"clearView in view");
		
		NSColor* color = [NSColor colorWithCalibratedWhite:0.0 alpha:0.0];
		[color set];
		NSRectFill(rect);
		needsCleaning = NO;
				
		
		if( Nmax_plus_1 >= 1 )
		{
		if( fullscreen )
		{
			NSString* string = [dataSource titleForCoverAtIndex:titleIndex];
			NSSize size = [string sizeWithAttributes: titleStyle_fullscreen];
			[string drawAtPoint:NSMakePoint(NSMidX(rect)-size.width/2,150) withAttributes: titleStyle_fullscreen];
			
			string = [dataSource subtitleForCoverAtIndex:titleIndex];
			size = [string sizeWithAttributes: subtitleStyle_fullscreen];
			[string drawAtPoint:NSMakePoint(NSMidX(rect)-size.width/2,150-size.height-15) withAttributes:subtitleStyle_fullscreen ];

			
		}else
		{
			NSString* string = [dataSource titleForCoverAtIndex:titleIndex];
			NSSize size = [string sizeWithAttributes: titleStyle];
			
			if( 0/*smallSize*/ )
				[string drawAtPoint:NSMakePoint(NSMidX(rect)-size.width/2,20) withAttributes: titleStyle];

			else
				[string drawAtPoint:NSMakePoint(NSMidX(rect)-size.width/2,50) withAttributes: titleStyle];
			
			string = [dataSource subtitleForCoverAtIndex:titleIndex];
			size = [string sizeWithAttributes: subtitleStyle];
			
			if( 0/*smallSize*/ )
				[string drawAtPoint:NSMakePoint(NSMidX(rect)-size.width/2,20-size.height-5) withAttributes:subtitleStyle ];
			else
				[string drawAtPoint:NSMakePoint(NSMidX(rect)-size.width/2,50-size.height-5) withAttributes:subtitleStyle ];

			
			
		}
			
			if( [delegate respondsToSelector:@selector(flowView: didDrawTitleViewInFullscreenMode: )] )
				[delegate flowView:self didDrawTitleViewInFullscreenMode:fullscreen  ];

		}
		
		
	}
	
	

	
	NSArray* selectedObjects = [[self scene] selectedObjects];
	NSEnumerator	*enumSelection = [selectedObjects objectEnumerator];
	C3DTGroup		*selectedObj;
	
/*
		NSBitmapImageRep* imageRep ;
		
		imageRep = [[NSBitmapImageRep alloc]  
initWithBitmapDataPlanes:NULL pixelsWide:256
			  pixelsHigh:64 bitsPerSample:8 samplesPerPixel:4  
				hasAlpha:YES isPlanar:NO colorSpaceName:NSDeviceRGBColorSpace  
			 bytesPerRow:0 bitsPerPixel:0];
		
		NSGraphicsContext *nsContext = 
			[NSGraphicsContext graphicsContextWithBitmapImageRep:imageRep];
		
		[NSGraphicsContext saveGraphicsState];
		[NSGraphicsContext setCurrentContext: nsContext];
		
		[[NSColor clearColor] set];
		NSRectFill(NSMakeRect(0,0, 64, 256) );
		
		[string drawAtPoint:NSMakePoint(-size.width/2,0) withAttributes:style];
		
		
		[NSGraphicsContext restoreGraphicsState];
		[[title style] generateTextureWithBitmapImageRep: imageRep];
		[[title style] apply];
		
		[imageRep release];
		*/
	
	NSDisableScreenUpdates();
	[super drawRect:rect];
	NSEnableScreenUpdates();
	
	
	
	while ( 0 /* draw selection frame // (selectedObj = (C3DTGroup *)[enumSelection nextObject]) != nil */)
	{

		NSArray* array = [self pointsOfObject:selectedObj];
		
		NSPoint point0 = NSPointFromString( [array objectAtIndex:0] );
		NSPoint point1 = NSPointFromString( [array objectAtIndex:1] );
		NSPoint point2 = NSPointFromString( [array objectAtIndex:2] );
		NSPoint point3 = NSPointFromString( [array objectAtIndex:3] );

		
		if( !(NSEqualPoints( point0 , NSZeroPoint  )&&
			  NSEqualPoints( point1 , NSZeroPoint  )&&
			  NSEqualPoints( point2 , NSZeroPoint  )&&
			  NSEqualPoints( point3 , NSZeroPoint  ) ))
		{
			
			// points will be affected by fov and view bounds
			
			NSBezierPath* path = [NSBezierPath bezierPathWithRect:rect];
			NSColor* color = [NSColor colorWithCalibratedWhite:0.0 alpha:0.5];
			[color set];
			//[path fill];
			
			[NSBezierPath setDefaultLineWidth:5.0];
			[[NSColor redColor] set];
			path = [NSBezierPath bezierPath];
			
			[path moveToPoint:point0];
			[path lineToPoint: point1];
			[path lineToPoint: point2];
			[path lineToPoint: point3];
			[path lineToPoint: point0];
			[path stroke];
			
			//[NSBezierPath strokeLineFromPoint:points[0] toPoint:points[1] ];
			//[NSBezierPath strokeLineFromPoint:points[1] toPoint:points[2] ];
			//[NSBezierPath strokeLineFromPoint:points[2] toPoint:points[3] ];
			//[NSBezierPath strokeLineFromPoint:points[3] toPoint:points[0] ];
			
			needsCleaning = YES;
		}
	}
					
					
}

- (void)mouseDragged:(NSEvent *)anEvent
{
    NSPoint		newLocation	= [anEvent locationInWindow];
    float		angleX, angleY;

    angleX = newLocation.x - lastClick.x;
    angleY = newLocation.y - lastClick.y;

    if ([anEvent modifierFlags] & ( NSShiftKeyMask || NSAlternateKeyMask) ) {
		
        [[self camera] setDistance: [[self camera] distance] - angleY];
        [[self camera] setFov: [[self camera] fov] + angleX];
	//NSLog(@"fov %f ",[[self camera] fov]);
    } else 
	if ([anEvent modifierFlags] & ( NSShiftKeyMask || NSCommandKeyMask)) {

		
		NSArray* selectedObjects = [[self scene] selectedObjects];
		NSEnumerator	*enumSelection = [selectedObjects objectEnumerator];
		C3DTGroup		*selectedObj;
		
		while ((selectedObj = (C3DTGroup *)[enumSelection nextObject]) != nil)
		{
					
		_C3DTVector vector =	[(MNNodeObject*)selectedObj rotation];
			[selectedObj setRotationX: vector.x - angleY Y: vector.y +angleX Z: vector.z];
				
				
		}

	}else	if ( 0/*[anEvent modifierFlags] & ( NSShiftKeyMask || NSControlKeyMask)*/ ) {

		C3DTSpotLight *aLight = [[[self scene] lights] objectAtIndex: 1];
		_C3DTVector vector =	[aLight spotDirection];

		
		[self willChangeValueForKey:@"dx"];
		[self willChangeValueForKey:@"dy"];
		[self willChangeValueForKey:@"dz"];
		
		dx = vector.x + angleY; dy = vector.y -angleX;
		[self didChangeValueForKey:@"dz"];
		
		[self didChangeValueForKey:@"dy"];
		
		[self didChangeValueForKey:@"dx"];

			[aLight setSpotDirectionX:dx Y:dy Z:dz];
	}else	if ( 0 /*[anEvent modifierFlags] & ( NSControlKeyMask)*/ ) {
		
		C3DTSpotLight *aLight = [[[self scene] lights] objectAtIndex: 1];
		_C3DTVector vector =	[aLight spotDirection];
		
		
		[self willChangeValueForKey:@"lx"];
		[self willChangeValueForKey:@"ly"];
		[self willChangeValueForKey:@"lz"];
		
		lx = vector.x + angleY; ly = vector.y -angleX;
		[self didChangeValueForKey:@"lz"];
		
		[self didChangeValueForKey:@"ly"];
		
		[self didChangeValueForKey:@"lx"];
		
		[aLight setPosX:lx Y:ly Z:lz];

		//[controller setLight:self];

    }else  if ([anEvent modifierFlags] & ( NSShiftKeyMask || NSControlKeyMask) )
	{
		

		
        [[self camera] rotateByDegreesX: angleX Y: -angleY];
	//NSLog(@"-angleY %f",[[self camera] pos].y);
    }
	
	

    [self setNeedsDisplay: YES];

    [super mouseDragged: anEvent];
}


- (BOOL)acceptsFirstResponder
{
	return YES;
}

- (void)keyDown:(NSEvent *)e
{
	//
	
	if( [e keyCode ] == 49 )
	{
		
		if( [delegate respondsToSelector:@selector(flowView:doubleClickedCoverAtIndex:)] )
			[delegate flowView:self doubleClickedCoverAtIndex:Ns];

		return;
	}
	
	
	if( ([animation currentProgress] < EVENT_LIMITTER) && [animation isAnimating] )
	{
		//[self performSelector:@selector(keyDown:) withObject:e afterDelay:0.2];
		return;
	}
	
	
	
	// every app with eye candy needs a slow mode invoked by the shift key
	
	if ([e modifierFlags] & (NSShiftKeyMask))
		[animation setDuration:DURATION1*5.0];
	else
		[animation setDuration:DURATION1];

	[animation setFrameRate:15.0];

	switch ([e keyCode])
    {
		case 123:				/* LeftArrow */
			[self moveRow:-1];
			break;
			
		case 124:				/* RightArrow */
			[self moveRow:+1];
				break;
			case 36:				/* RET */
				break;
			default:
			//NSLog (@"unhandled key event: %d\n", [e keyCode]);
				[super keyDown:e];
    }
}

- (void)scrollWheel:(NSEvent *)e
{
	if( ([animation currentProgress] < EVENT_LIMITTER) && [animation isAnimating] )
	{
		//[self performSelector:@selector(keyDown:) withObject:e afterDelay:0.2];
		return;
	}
	
	if( [e deltaY] == 0 && [e deltaX] == 0 && [e deltaZ] == 0 ) return;
	


//NSLog(@"wheel %d",(int)[e deltaY]);
	
	float val = fabsf([e deltaY]) > fabsf([e deltaX]) ? (float)[e deltaY] : (float)[e deltaX];
	

	if( abs(val) <= 1 )
	{
		int intval = (val<0? 1:-1 );
		
		
		[self moveRow: intval  ];
	}
	else 
	{
		int intval = (int)roundf(val);


		[self moveRow:-intval];
	}
}

-(void)movePage:(int)dif
{
}


-(void)moveRow:(int)dif
{
	/*
	if( dif > 1 )
	{
		Ns ++ ;
		[self moveRow:1];usleep(100);
		[self moveRow:dif-1];;
		return;
	}
	
	if( dif < -1 )
	{
		Ns -- ;
		[self moveRow:-1]; usleep(100);
		[self moveRow:dif+1];;
		return;
	}
	*/
	
	Ns_previous = Ns;

	
	//if( abs(dif) > R/2 ) 		[self resetSpacing];
	
	
	BOOL doAnimation = YES;
	
	if( dif > 0 )
	{
		if( Ns == Nmax_plus_1-1 )
		{
			doAnimation = NO;
			
		}else if(  Ns+dif >= Nmax_plus_1)
		{
			Ns = Nmax_plus_1-1;


		}else
		{
			Ns += dif;
			
		}

	}else
	{
		if( Ns == 0 )
		{
			doAnimation = NO;
			

		}
		else if( -dif > Ns) {
		{
			Ns = 0;
			

		}
		}else {
			Ns += dif;
		}

	}
	
	if( doAnimation )
	{
		//defer do animation

			[self doAnimation];

//NSLog(@"dif %d Ns %f, Ns' %f",dif, Ns, Ns_previous);

	//[scroller setFloatValue:Ns/Nmax_plus_1-1 knobProportion:1/Nmax_plus_1-1];

			needsCleaning = YES;
				titleIndex = Ns;
				[self display];
				
			if( [delegate respondsToSelector:@selector(flowView:didSelectCoverAtIndex:)] )
				[delegate flowView:self didSelectCoverAtIndex:titleIndex];

			[self adjustScroller];
		
	
	}

}


-(void)moveRowWithoutAnimation:(int)dif
{
	if( [animation isAnimating] )
	{
		[animation stopAnimation];
		
		while([animation isAnimating])
		{
			usleep(1000);
			
		}
		//[animation setCurrentProgress:1.0];
		
	}
	
	Ns_previous = Ns;
	
	
	//if( abs(dif) > R/2 ) 		[self resetSpacing];
	
	
	BOOL doAnimation = YES;
	
	if( dif > 0 )
	{
		if( Ns == Nmax_plus_1-1 )
		{
			doAnimation = NO;
			
		}else if(  Ns+dif >= Nmax_plus_1)
		{
			Ns = Nmax_plus_1-1;
			
			
		}else
		{
			Ns += dif;
			
		}
		
	}else
	{
		if( Ns == 0 )
		{
			doAnimation = NO;
			
			
		}
		else if( -dif > Ns) {
		{
			Ns = 0;
			
			
		}
		}else {
			Ns += dif;
		}
		
	}
	
	if( doAnimation )
	{

		int hoge;
		for( hoge = 0; hoge < [nodes count]; hoge++ )
		{
		MNNodeObject
			*node = [nodes objectAtIndex:hoge];
			
			
			CoverCoordinate c;
			c.angle = 2*pi/R * dif;
			c.r = 0;
			
	
			CoverCoordinate c2 = AddCoordinates( [node coordinate], c);
			[node setCoordinate: c2];
			[node updateTexture:nil];
		}
		
		Ncenter = Ns;
		[self resetSpacing];
		[self updateView];
		
		needsCleaning = YES;
		titleIndex = Ns;
		[self display];
		
		if( [delegate respondsToSelector:@selector(flowView:didSelectCoverAtIndex:)] )
			[delegate flowView:self didSelectCoverAtIndex:titleIndex];
		
		[self adjustScroller];
		
		
	}
	busyStatus = 0;
}



-(void)doAnimation
{
	if( [animation isAnimating] )
	{

		//clear spacing if Ns is not displayed
		/*
		if( (Ncenter < (Ns -R/2)) || ((Ns+R/2) < Ncenter) )
		{
		int hoge;
		for( hoge=0; hoge <R; hoge++ )
		{
			MNNodeObject *node = [nodes objectAtIndex:hoge];
			[node setSpacing:0];
		}
		}*/
		
		/*
		while([animation isAnimating])
		{
			[animation stopAnimation];

		}*/
		
		if( [animation isAnimating] )
		{
			[animation stopAnimation];
	
			while([animation isAnimating])
			{
				usleep(1000);
				
			}
			//[animation setCurrentProgress:1.0];

		}
	}
	else
	{
		ConvertToLinear_ResetPiPosition(-FRAME_WIDTH +SIDE_SPACE_FACTOR, FRAME_WIDTH +SIDE_SPACE_FACTOR);	
	}
	[animation setDuration: DURATION1];
	[animation setAnimationCurve:NSAnimationLinear];
	[animation setDelegate:self];
	//[animation setAnimationBlockingMode:NSAnimationNonblockingThreaded  ]; //個々をスレッドにすると、スレッドが増幅
	[animation setAnimationBlockingMode:NSAnimationNonblocking  ];
	
	[animation startAnimation];
}

-(void)setDataSource:(id)obj
{
	if( obj == dataSource ) return;
	
	
	[dataSource release];
	dataSource = obj;
	[dataSource retain];
}

-(void)setDelegate:(id)obj
{
	if( obj == delegate ) return;
	
	
	[delegate release];
	delegate = obj;
	[delegate retain];
}


- (void)_fillScene
{
    // Create a geometrical box, its style, and the entity that will combine them with transformation
    MNCFCover	*aPlain = [MNCFCover planeWithWidth:20.0f depth:20.0f meridians:0 parallels:0 textureRepeat:NO];
    C3DTTexture		*tubeColor= [C3DTTexture textureWithFile: @"art.tiff"];
    C3DTEntity		*tubeEntity = [C3DTEntity entityWithStyle: tubeColor geometry: aPlain];
    C3DTEntity		*cubeEntity = tubeEntity;
	
    // Setup a default scene (that has some lighting already
    C3DTScene		*aScene = [C3DTScene defaultScene];
    C3DTLight		*aLight = [C3DTLight light];
	
    // Add the entity to the scene
    [aScene addChild: cubeEntity];
	
    // add the additional light
    [aScene addLight: aLight];
	
    // Optional: sets orthographic view instead of perspective
    // WARNING: the orthographic view doesn't change with distance, object will be very small!
    //[[view camera] setUseOrtho: YES];
    
    // Last, add the scene tree to our view
    [self setScene: aScene];
}

-(C3DTTexture*)textureForN:(double)nodeN
{
	if( nodeN < 0 || nodeN >=Nmax_plus_1) return nil;
	
	C3DTTexture *txt = [textureDictionary objectForKey:[NSNumber numberWithDouble:nodeN]];
	if( txt == nil )
	{
		//check imageDictionary first
		[lock lock];

		NSImage *image = [imageDictionary objectForKey:[NSNumber numberWithDouble:nodeN]];
		
		if( !image )
		{
			image = [dataSource imageForCoverAtIndex:nodeN];
		}else
		{

			

		}
		
		if( !image  )
		{
			txt =  unknownTexture;

		}
		
		else {

			
			txt = [[[C3DTTexture alloc] initWithBitmapImageRep:[self iconRepForImage:image] ] autorelease];
			[textureDictionary setObject:txt forKey:[NSNumber numberWithDouble:nodeN]];
			
			if( [textureDictionary count] > TEXTURE_DICTIONARY_BUFFER_SIZE ) 
			{
			
				[textureDictionary removeObjectForKey: [[textureDictionary allKeys] objectAtIndex:0 ]];
			}
			
			[imageDictionary removeObjectForKey:[NSNumber numberWithDouble:nodeN] ];

		}
		
		[lock unlock];

	}
	
	return txt;
}


-(void)setup
{
	needsSetup = NO;
	
	NSDisableScreenUpdates();
	
	animation = [[AngleAnimation alloc] init];
	[animation setDelegate:self];
	
	coverFrame= [[MNCFCoverFrame alloc] init ];
	[coverFrame setFlowView:self];
	
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH  | GLUT_STENCIL);
	glAlphaFunc(GL_GREATER, 0.3);
	glEnable(GL_ALPHA_TEST);
	//glEnable(GL_POLYGON_SMOOTH);
	//glHint(GL_POLYGON_SMOOTH_HINT , GL_NICEST);
	//glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
	
	
	C3DTCamera	*aCamera = [self camera];
    _C3DTVector	cameraPos = {{ 0, -5, 160.0, 0.0 }};
	
    [aCamera setPos: cameraPos];
    [aCamera setLookAtX: 0.0f Y: 10.0f Z: 0.0f];
	
	
	
	long  order = 1;
	[[self openGLContext] setValues:&order forParameter: NSOpenGLCPSurfaceOpacity];

	 order = -1;
	[[self openGLContext] setValues:&order forParameter: NSOpenGLCPSurfaceOrder];
	
	
	
	//[[self window] setBackgroundColor: [NSColor clearColor]];
	[[self window] setAlphaValue:1.0];
	[[self window] setOpaque:NO];
	
	[[self window] useOptimizedDrawing:YES];
	
	R = COVER_PER_ROUND;
	Nmax_plus_1 = (double)[dataSource numberOfCovers];
	Ns = 0;
	Ns_previous = 0;
	Ncenter = 0;
	needsCleaning = YES;
	
//NSLog(@"Nmax_plus_1 %f",Nmax_plus_1);
	
	
	
	//set scene
	// Setup a default scene (that has some lighting already
    C3DTScene		*aScene = [C3DTScene defaultScene];
	C3DTLight		*light = [C3DTLight light];
    [aScene addLight: light];

	[aScene addChild:coverFrame];

	/*
	C3DTPlainSurface	*aPlain = [C3DTPlainSurface planeWithWidth:256.0f depth:64.0f meridians:0 parallels:0 textureRepeat:NO];
	
	
	
    C3DTTexture		*txt= [C3DTTexture textureWithFile: @"art.tiff"];
    title = [[C3DTEntity entityWithStyle: txt geometry: aPlain] retain];
	[title setRotationX:90 Y:0 Z:0];
	[title setTranslationX:0 Y:0 Z:Z_FRONT+10];
	//[aScene addChild: title];

	*/
	
    // Last, add the scene tree to our view
    [self setScene: aScene];
	
	
	NSImage* unknownImage = [[NSImage alloc] initWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"unknown" ofType:@"tiff"]];
	
	unknownTexture = [[C3DTTexture alloc] initWithBitmapImageRep:[self iconRepForImage:unknownImage]];
	if( unknownTexture == nil ) NSLog(@"unknownTexture is nil");
		
		
	[textureDictionary release];
	textureDictionary = [[NSMutableDictionary alloc] init];
	
	[lock lock];

	[imageDictionary release];
	imageDictionary = [[NSMutableDictionary alloc] init];
	[lock unlock];

	// create node objeccts
	NSMutableArray* temp_nodes = [NSMutableArray array] ;
	

	int num;
	for( num = 0; num < R ; num ++ )
	{
		// Create a geometrical box, its style, and the entity that will combine them with transformation
		MNCFCover			*aSurface = [MNCFCover planeWithWidth: ICON_OBJECT_SIZE 
																		   depth: ICON_OBJECT_SIZE 
																	   meridians: 1
																	   parallels: 1
																   textureRepeat: NO];
		
		
		MNNodeObject		*node = [MNNodeObject entityWithStyle: unknownTexture geometry: aSurface];
		[node setValue:self forKey:@"flowView"];
		
		[node setRotationX: 0.0 Y: 0.0 Z: 0.0];
		[node setTranslationX: 0 Y: 0 Z: 0];
		
		
		[coverFrame addCover: node];
		//[[self scene] addChild: surfaceEntity];

		[temp_nodes addObject: node];
		
		
	}
	[nodes release];
	nodes = [[NSArray alloc] initWithArray: temp_nodes];
	[self resetNodeObjects];

	
	/////
	
	
	
	
	
	
	
	/////
	
#ifdef USE_SPOTLIGHT
	
	C3DTSpotLight		*aLight = [C3DTSpotLight spotLight];
	[aLight setPosX:0 Y:50 Z:250 directional:NO];
	[aLight setSpotDirectionX:0.0 Y: 0.0 Z:800.0];
	[aLight setDiffuseRed:1.0 green:1.0 blue:1.0 alpha:1.0];
	//[aLight setAmbientRed:-0.5 green:-0.5 blue:-0.5 alpha:1.0];

	[aLight setSpotExponent:70];
	[aLight setSpotCutoff:65 ];

	[[self scene] addLight: aLight];
	
	glDisable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

#endif
	_C3DTVector	color = {{ 0.0, 0.0, 0.0, 0.0 }};
	[self setBackColor: color];
	
	glClearColor(1.0, 1.0, 1.0, 1.0);
	
	glEnable(GL_DEPTH_TEST);
	
	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	//glEnable(GL_CULL_FACE);
	
	
	
	// title layer setup
	NSShadow*   shadow;
	shadow = [[[NSShadow alloc] init] autorelease];
	[shadow setShadowOffset:NSMakeSize(0.0, -1.0f)];
	[shadow setShadowBlurRadius:2.5f];
	[shadow setShadowColor:[[NSColor blackColor] colorWithAlphaComponent:1.0f]];

	
	titleStyle
	= [NSDictionary dictionaryWithObjectsAndKeys:[NSColor whiteColor], NSForegroundColorAttributeName, [NSFont boldSystemFontOfSize: 14.0], NSFontAttributeName, shadow, NSShadowAttributeName, nil];
	[titleStyle retain];

	subtitleStyle
		= [NSDictionary dictionaryWithObjectsAndKeys:[NSColor whiteColor], NSForegroundColorAttributeName, [NSFont userFontOfSize: 9.0], NSFontAttributeName, shadow, NSShadowAttributeName, nil];
	[subtitleStyle retain];
	
	titleStyle_fullscreen
		= [NSDictionary dictionaryWithObjectsAndKeys:[NSColor whiteColor], NSForegroundColorAttributeName, [NSFont boldSystemFontOfSize:32.0], NSFontAttributeName, shadow, NSShadowAttributeName, nil];
	[titleStyle_fullscreen retain];
	
	subtitleStyle_fullscreen
		= [NSDictionary dictionaryWithObjectsAndKeys:[NSColor whiteColor], NSForegroundColorAttributeName, [NSFont userFontOfSize: 14.0], NSFontAttributeName, shadow, NSShadowAttributeName, nil];
	[subtitleStyle_fullscreen retain];

	
	
	NSRect frame = [self frame];
	//scroller = [[NSScroller alloc] initWithFrame:NSMakeRect(50,100,frame.size.width-50, 16 )];
	//[scroller setControlSize:NSRegularControlSize];
	//[self addSubview:scroller];
	
	
	// setup view
	
	
	
	
	//setup linear 
	
	
	ConvertToLinear_ResetPiPosition(-FRAME_WIDTH +SIDE_SPACE_FACTOR, FRAME_WIDTH +SIDE_SPACE_FACTOR);	


	


	
	
	
	//setup additiona button
	
	
	scrollerView = [[ScrollerView alloc] initWithFrame:NSMakeRect(0,10, frame.size.width,32)];
	[self addSubview:scrollerView];
	[scrollerView setDelegate:self];

	[self reloadData];

	
	NSEnableScreenUpdates();
}
#pragma mark Library

_C3DTVector MNVectorMultiply( const _C3DTMatrix m, const _C3DTVector v)
{
    _C3DTVector				r;
    
    r.x = v.x*m.flts[0] + v.y*m.flts[1] + v.z*m.flts[2] + v.w*m.flts[3];
    r.y = v.x*m.flts[4] + v.y*m.flts[5] + v.z*m.flts[6] + v.w*m.flts[7];
    r.z = v.x*m.flts[8] + v.y*m.flts[9] + v.z*m.flts[10] + v.w*m.flts[11];
	r.w = v.x*m.flts[12] + v.y*m.flts[13] + v.z*m.flts[14] + v.w*m.flts[15];

    return r;
}

- (NSPoint)convertToWindowCoords:(_C3DTVector)objVector
{
	GLint viewport[4];
	GLdouble mvmatrix[16], projmatrix[16];
	GLdouble wx, wy, wz;
	
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, projmatrix);
	
	gluProject((GLdouble)objVector.x, (GLdouble)objVector.y, objVector.z, mvmatrix, projmatrix, viewport, &wx, &wy, &wz);
	
	return NSMakePoint(wx, wy);
	
}

- (_C3DTVector)convertFromWindowCoords:(NSPoint)localPoint 
{
	GLint viewport[4];
	GLdouble mvmatrix[16], projmatrix[16];
	GLdouble wx, wy, wz;
	GLfloat z;

	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, projmatrix);

	glReadPixels((GLdouble)localPoint.x, (GLdouble)localPoint.y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);

	gluUnProject((GLdouble)localPoint.x, (GLdouble)localPoint.y, z, mvmatrix, projmatrix, viewport, &wx, &wy, &wz);
	_C3DTVector vector;
	vector.x = wx;
	vector.y = wy;
	vector.z = wz;
	vector.w = 1.0;

	return vector;
}




#pragma mark FUllscreen

-(void)fullscreen:(id)sender
{

	
	
	NSBundle* bundle = [NSBundle bundleForClass:[self class]];
	NSImage* bimg  = [[NSImage alloc] initWithContentsOfFile:
		[bundle pathForImageResource:@"fullscreen_end"]];
	
	NSButton *button = [scrollerView valueForKey:@"fullscreenButton"];
	[button setTag:11];
	[button setImage:bimg];
	
	//[scrollerView setFrame:NSMakeRect(0,50,500,32)];
	fullscreen = YES;
	
	
	//NSLog(@"fullscreenButton");	
	[self fullScreenWithMoview:NO];
	
	if( [delegate respondsToSelector:@selector(flowViewFullScreenModeOn:)] )
	{
		[delegate flowViewFullScreenModeOn:self];
	}
	
	[fullWindow makeKeyAndOrderFront:self];

}

-(void)fullScreenWithMoview:(BOOL)movieAnimation
{
	
	// Get screen frame
	NSRect  screenFrame;
	screenFrame = [[[self window] screen] frame];
	
	// Get content frame
	NSRect contentRect;
	contentRect = [[self superview] convertRect:[self frame] toView:nil];
	
	
	
	
	contentRect.origin.x += [[self window] frame].origin.x;
	contentRect.origin.y += [[self window] frame].origin.y;
	
	
	
	//adjust aspect
	if( ( contentRect.size.width / contentRect.size.height ) > ( screenFrame.size.width / screenFrame.size.height ) )
	{//もとが横長の場合
		float newWidth  = screenFrame.size.width / screenFrame.size.height * contentRect.size.height;

		contentRect. origin.x -= ( newWidth - contentRect.size.width) /2;
		contentRect. size.width = newWidth;

	}else {
		float newHeight  = screenFrame.size.width / screenFrame.size.height * contentRect.size.height;

		contentRect. origin.y -= ( newHeight - contentRect.size.height) /2;
		contentRect. size.height = newHeight;

		
		
		
	}
	
	attachedWindowTargetOriginalRectInScreen = contentRect;
	attachedWindowTargetOriginalRect  = [self frame];
	attachedWindowTargetOriginalParentView = [self superview];
	
	
	if( fullWindow == nil )
	{
		fullWindow =  [[TransparentWindow alloc] initWithContentRect:screenFrame 
														   styleMask:NSBorderlessWindowMask 
															 backing:NSBackingStoreBuffered 
															   defer:NO];
		[fullWindow setReleasedWhenClosed:NO];
		
		// Configure window characteristics.
		[fullWindow setBackgroundColor:[NSColor clearColor]];
		[fullWindow setMovableByWindowBackground:NO];
		[fullWindow setExcludedFromWindowsMenu:YES];
		[fullWindow setAlphaValue:1.0];
		[fullWindow setOpaque:NO];
		[fullWindow setHasShadow:NO];
		[fullWindow useOptimizedDrawing:YES];
		[fullWindow setLevel:NSStatusWindowLevel];
		
		[fullWindow setDelegate: self ];
		
	}
	NSDisableScreenUpdates();
	
	[[self window] makeFirstResponder:nil];
	[self retain];
	[self removeFromSuperview];
	[[fullWindow contentView] addSubview: self];
	[self release];
	
	[self setFrame: [[fullWindow contentView] frame]];
	
	
	
	
	if( fullscreenAnimation == nil )
	{
		fullscreenAnimation = [[WindowAnimation alloc] initWithDuration:0.25
											   animationCurve:NSAnimationEaseInOut];
	}
	
		[fullscreenAnimation setAnimationBlockingMode:NSAnimationNonblockingThreaded];
	
	//NSDisableScreenUpdates();
	//[fullWindow makeKeyAndOrderFront: nil];
	//NSEnableScreenUpdates();
	
	
	[fullscreenAnimation setupAnimation:contentRect end:screenFrame  target:fullWindow];
	[fullscreenAnimation setDelegate:nil];
	
	if( movieAnimation )
		[fullscreenAnimation startAnimation];	
	NSEnableScreenUpdates();
	
}

#pragma mark Scroller

-(void)scrollView:(ScrollerView*)sv buttonClicked:(NSButton*)button
{
//NSLog(@"button");
	if( [button tag] == 1 )
	{
		[self moveRow:1];
	}
	else 	if( [button tag] == -1 )
	{
		[self moveRow:-1];
	}
	
	if( [button tag] == 10 )
	{
		NSBundle* bundle = [NSBundle bundleForClass:[self class]];
		
		NSImage* bimg  = [[NSImage alloc] initWithContentsOfFile:
			[bundle pathForImageResource:@"fullscreen_end"]];
		
		
		[button setTag:11];
		[button setImage:bimg];
		
		//[scrollerView setFrame:NSMakeRect(0,50,500,32)];
		fullscreen = YES;
		
		
	//NSLog(@"fullscreenButton");	
		[self fullScreenWithMoview:YES];
		
		if( [delegate respondsToSelector:@selector(flowViewFullScreenModeOn:)] )
		{
			[delegate flowViewFullScreenModeOn:self];
		}
		
	}else if( [button tag] == 11 )
	{
		NSBundle* bundle = [NSBundle bundleForClass:[self class]];
		
		NSImage* bimg  = [[NSImage alloc] initWithContentsOfFile:
			[bundle pathForImageResource:@"fullscreen"]];
		

		[button setTag:10];
		[button setImage:bimg];
		fullscreen = NO;
		
		NSRect  screenFrame;
		screenFrame = [[[self window] screen] frame];
		//[scrollView setFrame:NSMakeRect(0,10, frame.size.width,32)];
		
		
		if( [delegate respondsToSelector:@selector(flowViewFullScreenModeOff:)] )
		{
			[delegate flowViewFullScreenModeOff:self];
		}
		
		[fullscreenAnimation setupAnimation:screenFrame end: attachedWindowTargetOriginalRectInScreen  target:fullWindow];
		[fullscreenAnimation setDelegate:self];
		[fullscreenAnimation startAnimation];
		
	//NSLog(@"fullscreenButton end");	
		
	}
	

}
-(void)scrollView:(ScrollerView*)sv hit:(NSScrollerPart)hitPart
{
//NSLog(@"hit");
	
	if( hitPart == NSScrollerKnob )
	{
		float loc = [sv floatValue];
		[self selectCoverAtIndex: (Nmax_plus_1-1)*loc];
	}
	if( hitPart == NSScrollerDecrementPage )
	{
		[self moveRow:-MOVE_PAGE];
 	
	}
	if( hitPart == NSScrollerIncrementPage )
	{
		[self moveRow:MOVE_PAGE];
		
	}
	
}

-(void)adjustScroller
{
	[scrollerView setFloatValue:Ns/(Nmax_plus_1-1) knobProportion:1/(Nmax_plus_1-1)];	
}

#pragma mark -


-(IBAction)selectNs:(id)sender
{
	Ns_previous = Ns;
	Ns = [sender doubleValue];
	[self doAnimation];
}

-(NSArray*)nodesSortedFromRear
{
	return [nodes sortedArrayUsingSelector:@selector(comparePosition:)];
	
	
}
-(unsigned char)alphaInBitmap:(unsigned char*)data atX:(int)x Y:(int)y
{
	if( x < 0 || y < 0 || x > ICON_TEXTURE_SIZE || y > ICON_TEXTURE_SIZE) return 0;
	
	int Bpr, spp;
	
	int bits = 8;
	//NSLog(@"bitsPerSample %d", bits);
	

	spp = 4;

	Bpr = ICON_TEXTURE_SIZE * 4;

	
	
	unsigned char *p;
	

	p = data + Bpr*y + spp * x;
	return p[3];
	
}
-(NSBitmapImageRep*)trimBitmap:(NSBitmapImageRep*)rep
{
	
	int Bpr, spp;
	unsigned char *data;
	int x, y, w, h;
	
	int bits = [rep bitsPerSample];
	//NSLog(@"bitsPerSample %d", bits);

	if ( bits != 8) {
		
		// usually each color component is stored
		// using 8 bits, but recently 16 bits is occasionally
		// also used ... you might want to handle that here
		return rep;
	}
	
	Bpr = [rep bytesPerRow];
	spp = [rep samplesPerPixel];
	data = [rep bitmapData];
	w = [rep pixelsWide];
	h = [rep pixelsHigh];
	
	if( spp !=4 ) return rep; // spp should be 4 as this method is called from iconRepForImage
	
	//NSLog(@"bytes per row %d",Bpr);
	
	BOOL flag = NO;
	
	
	//NSLog(@"w  %d, h %d, samplesPerPixel %d",w,h, spp);
	/*
	 1
	3 4
	 2
	 */
	
	NSRect trimRect = NSZeroRect;
	
	//(1)
	unsigned char *p;
		
	for (y=h-1; y>=  0  ; y--)
	{
		for (x=0; x<  w  ; x++)
		{
			p = data + Bpr*y + spp * x;

			
			// do your thing
			// p[0] is red, p[1] is green, p[2] is blue, p[3] can be alpha if spp==4
			
			////NSLog(@"%d,%d,%d",p[0], p[1], p[2]);
			//NSLog(@"p[3] %d",p[3]);
			
			if( p[3]  < ALPHA_THRESHOLD  )
			{
				
			}else
			{
				flag = YES;
				break;
			}
			
		}
		if( flag == YES )
			break;
	}
	
	if( flag )
	{
	//lowerst y is y	
	//return rep;

		//NSLog(@"y %d",y);
		
		y++; //ドット位置補正
		// size Bpr*h
		unsigned char * newBitmapData = malloc( Bpr*h );
		
		int hoge;
		for( hoge = 0; hoge < Bpr*h; hoge++ )
		{
			*(newBitmapData + hoge) = 0;
		}
		
		for( hoge = 0; hoge < Bpr*y; hoge++ )
		{
			*(newBitmapData + Bpr*(h-y) + hoge) = *(data + hoge);
		}
		
		NSBitmapImageRep *newRep  = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:&newBitmapData pixelsWide:w
																			pixelsHigh:h bitsPerSample:8 samplesPerPixel:4  
																			  hasAlpha:YES isPlanar:NO colorSpaceName:NSDeviceRGBColorSpace  
						
																			bytesPerRow:0 bitsPerPixel:0] ;
		
		//NSBitmapImageRep *newRepCopy = [newRep copy];
		//[newRep release];
		//free(newBitmapData);
		return [newRep autorelease];
		
	}else 
	{
		return rep;
	}


}

-(NSBitmapImageRep*)iconRepForImage:(NSImage*)image
{
	
	
	[image setSize:NSMakeSize(128,128)];
	[image setCacheMode:NSImageCacheNever];
	
	
	NSBitmapImageRep* imageRep ;
	
	imageRep = [[NSBitmapImageRep alloc]  
initWithBitmapDataPlanes:NULL pixelsWide:ICON_TEXTURE_SIZE
			  pixelsHigh:ICON_TEXTURE_SIZE bitsPerSample:8 samplesPerPixel:4  
				hasAlpha:YES isPlanar:NO colorSpaceName:NSDeviceRGBColorSpace  
			 bytesPerRow:0 bitsPerPixel:0];
	
	// Create an NSGraphicsContext that draws into the NSBitmapImageRep.  
	NSGraphicsContext *nsContext = [NSGraphicsContext  
graphicsContextWithBitmapImageRep:imageRep];
	
	// Save the previous graphics context and state, and make our bitmap  
	//context current.
	[NSGraphicsContext saveGraphicsState];
	[NSGraphicsContext setCurrentContext: nsContext];
	
	// Get a CIContext from the NSGraphicsContext, and use it to draw the  
	//[image compositeToPoint:NSZeroPoint operation:NSCompositeSourceOver];
	
	[image drawInRect:NSMakeRect(0,0,ICON_TEXTURE_SIZE,ICON_TEXTURE_SIZE)
			 fromRect:NSMakeRect(0,0,128,128)
			operation:NSCompositeSourceOver fraction:1.0];
	
	
	//[[path lastPathComponent] drawAtPoint:NSMakePoint(0,66) withAttributes:  subtitleAttr];
	// Restore the previous graphics context and state.
	[NSGraphicsContext restoreGraphicsState];
	
	[imageRep release];
	
	return [self trimBitmap:imageRep];
}

-(NSArray*)pointsOfObject:(MNNodeObject*)node
{
	
	NSPointPointer points = [node pointsOnView];

	NSArray* array = [NSArray arrayWithObjects: NSStringFromPoint( points[0] ) ,
		NSStringFromPoint( points[1] ) ,
		NSStringFromPoint( points[2] ) ,
		NSStringFromPoint( points[3] ) ,

		nil];
	
	//needsCleaning = YES;
	//NSLog(@"node %d points %@",[ node n], [array description]);
	return array;
}

-(NSArray*)pointsOfN:(double)givenN
{
	
	MNNodeObject *node;
	
	node = [nodes objectAtIndex: Calc_n(R,givenN)];
	
	if( [node N] != givenN )
	{
		return nil;	
	}
	 
	//NSLog(@"points %f node %d, %@",givenN, [node n], [[self pointsOfObject: node] description]);
	return [self pointsOfObject: node];
}


#pragma mark DEBUG
-(void)adjustCenter:(id)sender
{
	
	
	
	double delta_angle = -2*pi/R;
	double currentAngle = pi;
	
	CoverCoordinate zc;
	zc.angle = 0;
	zc.z = 0;
	zc.rotation = 0;
	zc.spacing=0;
	zc.r=1;

	int num;
	for( num = 0; num < R ; num ++ )
	{

		
		MNNodeObject		*node = [nodes objectAtIndex:num];
		
		
		CoverCoordinate c;
		c.angle = currentAngle;
		c.r = 0.0;
		

		[node setValue:[NSNumber numberWithInt:num] forKey:@"n"];
		
		[node setCoordinate: zc ];
		[node addCoordinate: c ];

		currentAngle += delta_angle;
		
		
				
		
	}
	Ns_previous = 0;
	Ncenter = 0;
	[self doAnimation];
}
-(void)dump:(id)sender
{
//NSLog(@"----- ");

//NSLog(@"Ns %f",Ns);
//NSLog(@"Ns_previous %f",Ns_previous);
//NSLog(@"Ncenter %f",Ncenter);

	CoverCoordinate midPointCoordinate;
	midPointCoordinate.r = 0;
	midPointCoordinate.angle = pi;
	double relativeMidX = ConvertToLinear_RelativeXFromMin(  0,  midPointCoordinate, nodes,  Ns,  Ns_previous,  -FRAME_WIDTH,  FRAME_WIDTH );

	
//NSLog(@"relativeMidX %f",relativeMidX);
//NSLog(@"----- ");
	
	int hoge;
	for( hoge = 0; hoge < [nodes count]; hoge++ )
	{
	//NSLog(@"----- ");
	//NSLog(@"n %d",[[nodes objectAtIndex:hoge] n]);

		CoverCoordinate c = [[nodes objectAtIndex:hoge] coordinate];
	//NSLog(@"r %f",c.r);
	//NSLog(@"angle %f",c.angle);
	//NSLog(@"N %f",[[nodes objectAtIndex:hoge] N]);
	}

}
-(void)setup:(id)sender
{
	[self reloadData];
}
@end

#pragma mark 

@implementation FlowView (Public)

-(void)reloadData
{
	if( [animation isAnimating] )
	{
		[animation stopAnimation];
		
		while([animation isAnimating])
		{
			usleep(1000);
			
		}
		//[animation setCurrentProgress:1.0];
		
	}
	
	NSDisableScreenUpdates();

	Nmax_plus_1 = (double)[dataSource numberOfCovers];
	Ns = 0;
	Ns_previous = 0;
	Ncenter = 0;
	titleIndex = 0;
	needsCleaning = YES;
	
	[textureDictionary release];
	textureDictionary = [[NSMutableDictionary alloc] init];
	
	[lock lock];

	[imageDictionary release];
	imageDictionary = [[NSMutableDictionary alloc] init];

	[lock unlock];


	[self resetNodeObjects];
	[nodes makeObjectsPerformSelector:@selector(updateTexture:) withObject:self];
	
	[self moveRow:0];
	
	[self setNeedsCleaning:YES];
	[self updateView];
	[self adjustScroller];

	[[self window] performSelectorOnMainThread:@selector(display) withObject:nil waitUntilDone:YES];
	

	NSEnableScreenUpdates();
}

-(void)reloadDataWithInsertionEffect
{
	if( [animation isAnimating] )
	{
		[animation stopAnimation];
		
		while([animation isAnimating])
		{
			usleep(1000);
			
		}
		//[animation setCurrentProgress:1.0];
		
	}
	
	
	Nmax_plus_1 = (double)[dataSource numberOfCovers];
	Ns = 0;
	Ns_previous = 0;
	Ncenter = 0;
	titleIndex = 0;
	needsCleaning = YES;
	
	[textureDictionary release];
	textureDictionary = [[NSMutableDictionary alloc] init];
	
	[lock lock];

	[imageDictionary release];
	imageDictionary = [[NSMutableDictionary alloc] init];

	[lock unlock];

	NSDisableScreenUpdates();
	[self resetNodeObjects];
	[nodes makeObjectsPerformSelector:@selector(updateTexture:) withObject:self];
	
	[self selectCoverAtIndex:1];
	
	[self setNeedsCleaning:YES];
	[self updateView];
	NSEnableScreenUpdates();
	
	[self moveRow:-1];

}


-(void)reloadDataWithoutMoving
//onely adding one cover
{
	if( [animation isAnimating] )
	{
		[animation stopAnimation];
		
		while([animation isAnimating])
		{
			usleep(1000);
			
		}
		//[animation setCurrentProgress:1.0];
		
	}
	
	
	Nmax_plus_1 = (double)[dataSource numberOfCovers];
	Ns ++;
	Ns_previous ++;
	Ncenter ++;
	titleIndex  ++;
	needsCleaning = YES;
	
	[textureDictionary release];
	textureDictionary = [[NSMutableDictionary alloc] init];
	
	[lock lock];
	
	[imageDictionary release];
	imageDictionary = [[NSMutableDictionary alloc] init];
	
	[lock unlock];
	
	NSDisableScreenUpdates();
	[self resetNodeObjects];
	[nodes makeObjectsPerformSelector:@selector(updateTexture:) withObject:self];
		
	[self setNeedsCleaning:YES];
	[self updateView];
	NSEnableScreenUpdates();
	
	
}


-(BOOL)needsCoverAtIndex:(unsigned)index;
{
	if( ( Ns_previous < Ns ) && ( Ncenter < index)) return YES;
	if( ( Ns < Ns_previous  ) && (  index < Ncenter )) return YES;

	

	if( ( (double)index  < (Ns -R/2 -5) ) || (( Ns + R/2 +5) < (double)index ) )
		return NO;
		
	return YES;
}


-(void)pushImage:(NSImage*)image forCoverAtIndex:(unsigned)index;
{
	
	if( !image ) return;

	[lock lock];
	[imageDictionary setObject:image forKey:[NSNumber numberWithDouble:index]];
	
	[lock unlock];
}

-(void)selectCoverAtIndex:(unsigned)index
{
	int dif = index - Ns;
	[self moveRow:dif];
	
}
-(void)selectCoverAtIndexWithoutAnimation:(unsigned)index
{
	int dif = index - Ns;
	[self moveRowWithoutAnimation:dif];
	
}


/*
 if( [self style] == [flowView valueForKey:@"unknownTexture"] )
	{
	 [self performSelectorOnMainThread:@selector(updateTexture:) withObject:nil waitUntilDone:YES] ;
	}
 */

@end
