//
//  NSTransformApplication.m
//  Rotated Windows
//
//  Created by Wade Tregaskis on Wed May 19 2004.
//
//  Copyright (c) 2004 Wade Tregaskis. All rights reserved.
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//    * Neither the name of Wade Tregaskis nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#import "NSTransformApplication.h"


void transformNSEvent(NSEvent *event) {    
    if (event) {
        NSWindow *window = [event window];
        
        if (window) {
            CGAffineTransform windowTransform;
            CGPoint tempCG;
            NSPoint tempNS;//, tempNS2;
            NSRect windowFrame, screenFrame;
            
            tempNS = [event locationInWindow];
            
            tempCG.x = tempNS.x;
            tempCG.y = tempNS.y;
            
            // Obtain the transform in use
            CGSGetWindowTransform(_CGSDefaultConnection(), [window windowNumber], &windowTransform);
            
            // Get some necessary information
            windowFrame = [window frame];
            screenFrame = [[window screen] frame];
            
            // Translate to screen co-ordinates
            tempCG.x += windowFrame.origin.x;
            tempCG.y = screenFrame.size.height - (windowFrame.origin.y + tempCG.y);
            
            // Apply the transform
            tempCG = CGPointApplyAffineTransform(tempCG, windowTransform);
            
            //tempNS2 = tempNS; // Debugging
            
            // tempCG is now in inverted window co-ordinates, so we need to invert the y component
            tempNS.x = tempCG.x;
            tempNS.y = windowFrame.size.height - tempCG.y;
            
            //NSLog(@"{%f, %f} -> {%f, %f}", tempNS2.x, tempNS2.y, tempNS.x, tempNS.y); // Debugging
            
            [event setLocationInWindow:tempNS];
        }
    }
}


@implementation NSTransformApplication

- (id)init {
    if (self = [super init]) {
        lastEvent = nil;
    }
    
    return self;
}

- (void)sendEvent:(NSEvent*)anEvent {
    //NSLog(@"[NSTransformApplication(-) sendEvent:%x]", anEvent);
    
    if (lastEvent != anEvent) {
        transformNSEvent(anEvent);
        lastEvent = anEvent;
    }
    
    [super sendEvent:anEvent];
}

- (NSEvent*)nextEventMatchingMask:(unsigned int)mask untilDate:(NSDate*)expiration inMode:(NSString*)mode dequeue:(BOOL)flag {
    NSEvent *result = [super nextEventMatchingMask:mask untilDate:expiration inMode:mode dequeue:flag];

    if (lastEvent != result) {
        transformNSEvent(result);
        lastEvent = result;
    }
        
    //NSLog(@"[NSTransformApplication(-) nextEventMatchingMask:%d untilDate:... inMode:%@ dequeue:%@] -> %x", mask, mode, (flag ? @"YES" : @"NO"), result);
    
    return result;
}

@end
