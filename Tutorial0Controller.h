//
//  TutorialController.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: Tutorial0Controller.h,v 1.1.1.1 2003/06/10 18:09:36 pmanna Exp $
//
// $Log: Tutorial0Controller.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:36  pmanna
// Initial import
//
//


#import <Cocoa/Cocoa.h>
#import <Cocoa3DTutorial/C3DTView.h>

@interface Tutorial0Controller : NSObject
{
    IBOutlet NSFormCell *lightX;
    IBOutlet NSFormCell *lightY;
    IBOutlet NSFormCell *lightZ;
    IBOutlet NSFormCell *cameraX;
    IBOutlet NSFormCell *cameraY;
    IBOutlet NSFormCell *cameraZ;
    IBOutlet NSSlider	*distance;
    IBOutlet C3DTView	*view;
}

- (IBAction)setPosition:(id)sender;
- (IBAction)setLight:(id)sender;
- (void)setCameraX: (float)x Y: (float)y Z: (float)z;

@end
