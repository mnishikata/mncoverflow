//
//  MNNodeObject.m
//  MNC3DT
//
//  Created by Masatoshi Nishikata on 08/02/28.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MNNodeObject.h"
#import "MNCFCover.h"

extern double SIDE_ANGLE;
extern double Z_FRONT;
extern double DURATION1;
extern double SIDE_SPACE_FACTOR;


@implementation MNNodeObject


+ (MNNodeObject *)entityWithStyle: (C3DTStyle *)aStyle
{
    return [[[MNNodeObject alloc] initWithStyle: aStyle] autorelease];
}

+ (MNNodeObject *)entityWithStyle: (C3DTStyle *)aStyle geometry: (C3DTGeometry *)aGeometry
{
    MNNodeObject	*result = [[[MNNodeObject alloc] initWithStyle: aStyle] autorelease];
	
    if (result != nil)
        [result setGeometry: aGeometry];
	
    return result;
}

- (id)initWithStyle: (C3DTStyle *)aStyle
{
    if ([super initWithStyle:aStyle] != nil) {
		
		coordinate.r=1;
		coordinate.z=0;
		coordinate.angle=0;
		coordinate.rotation=0;
		coordinate.spacing = 0;
		
				
		animatingProgress = 1.0;
		
		pointsOnView = malloc(  sizeof(NSPoint)*4 );

	}
	
    return self;
}


- (void)useWith: (id)anObject
{
//NSLog(@"n %d isVisible %d",n,[self isVisible]);
	
	if( [self isVisible] ) [super useWith:anObject];
	
	
	[self projectedPlainVerticesWithCamera:[flowView  camera] intoPoints:pointsOnView inView:flowView] ;

}
-(NSPointPointer)pointsOnView
{
	return pointsOnView;
}
/*
- (id) init {
	self = [super init];
	if (self != nil) {
		n=0;
		
		coordinate.r=1;
		coordinate.z=0;
		coordinate.angle=0;
		coordinate.rotation=0;
		
		storedCoordinate.r=0;
		storedCoordinate.z=0;
		storedCoordinate.angle=0;
		storedCoordinate.rotation=0;
		
		animatingProgress = 1.0;
	}
	return self;
}*/

-(void)setNmax:(double)newNmax
{
	Nmax = newNmax;
}
-(void)setR:(int)newR
{
	R = newR;
}

-(BOOL)isVisible
{

//NSLog(@"checking n %d",n);

	// min check
	if( coordinate.r<= 0.0 ) return NO;
	
	// max check
	int ne;
	int rmax;
	
	ne = Calc_n( R, Nmax);
	rmax = Calc_r(Nmax,R);
	
//NSLog(@"Nmax %f",Nmax);

//NSLog(@"ne %d",ne);
//NSLog(@"rmax %d",rmax);
//NSLog(@"coordinate.r %f",coordinate.r);
	if( ((coordinate.r == rmax ) && (n>ne)) || 
		(coordinate.r > rmax ))
		return NO;
	
	return YES;
}



-(void)addCoordinate:(CoverCoordinate)c inAnimation:(BOOL)flag
{
	double exr = coordinate.r;
	
	coordinate = AddCoordinates(coordinate, c);
	
	if( flag && textureUpdated ) return; // already updated status
	
	
	if( (exr != coordinate.r)  ) {
		[self rChanged];
		textureUpdated = YES;
		
	}else
	{
		textureUpdated = NO;
	}
}

-(void)addCoordinate:(CoverCoordinate)c
{
	[self addCoordinate:c inAnimation:NO];
}

-(void)rChanged
/*
端部のカバーは、addCoordinateを経由してこの関数が何度も呼ばれる。アニメーション時にリストアと加算を繰返すため。
 これを防止するため、textureUpdated フラグを追加

*/
{
	[self updateSelectionState];
	//NSLog(@"changed %f %d, %d",[self N], isSelected,isPreviouslySelected);
	[self performSelectorOnMainThread:@selector(updateTexture:) withObject:nil waitUntilDone:NO];


}

-(void)updateTexture:(id)sender
{
//NSLog(@"updateTexture");
	
	C3DTTexture *txt = [flowView textureForN:[self N]];
	if( txt == nil ){
		//NSLog(@"txtis nil ");
	}
	
	if( txt && ( [self style] != txt)  )
	{
	//NSLog(@"txtis new ");

		[self setStyle:txt];
		[flowView setNeedsDisplay:YES];
	}
}

-(void)dealloc
{
	free(pointsOnView);
	[flowView release];
	[super dealloc];
}
-(void)setFlowView:(FlowView*)aView
{
	[flowView release];
	flowView = [aView retain];
}


-(void)updateSelectionState
{
	double N = [self N];
	
		
	if( N == Ns ) isSelected = YES;
	else isSelected = NO;
	
	if( N == Ns_previous ) isPreviouslySelected = YES;
	else isPreviouslySelected = NO;
	
}

-(BOOL)isSelected
{
	return isSelected;
}
-(BOOL)isPreviouslySelected
{
	return isPreviouslySelected;
}

/* unused
-(double)__spacer
{
	if( isSelected )
		return Spacer(animatingProgress, true);
	else if( isPreviouslySelected )
		return Spacer(animatingProgress, false);
	
	return 0.0;
}
*/
-(void)setAnimation:(double)newNs previousNs:(double)newNs_previous
{
	
	Ns = newNs;
	Ns_previous = newNs_previous;
	
	// change state
	
	[self updateSelectionState];
	
	//// prepare animation
	
	
	animatingProgressOffset = 1.0 - animatingProgress;
	animatingProgress = 0;
	animationStartCoordinate = coordinate;

	//NSLog(@"off set %f",animatingProgressOffset);
}


-(void)animate:(double)time
{

	
	
	CoverCoordinate animationEndCoordinate = coordinate;
	
	double N = [self N];

	
	// set end rotation
	
	if( Ns_previous < Ns )
	{
		rotateClockwise = NO;
		
		if( N <= Ns_previous)
			animationEndCoordinate.rotation = -SIDE_ANGLE;
		if( (Ns_previous<N) && (N < Ns) ) 
		{
			animationEndCoordinate.rotation = -SIDE_ANGLE;
			rotateClockwise = NO;
		}
		
		if( N == Ns ) animationEndCoordinate.rotation= 0;
		if( N >Ns ) animationEndCoordinate.rotation =  SIDE_ANGLE;
		
		
	}else
	{
		rotateClockwise = NO;
		
		if( N < Ns ) animationEndCoordinate.rotation =  -SIDE_ANGLE;
		if( N == Ns ) animationEndCoordinate.rotation =  0;
		if( (Ns<N) && (N < Ns_previous) ){
			animationEndCoordinate.rotation =  SIDE_ANGLE;
			rotateClockwise = NO;	
		}
		if( N >= Ns_previous ) animationEndCoordinate.rotation =  SIDE_ANGLE;
	}
	
	
	
	/// set end z
	if( N == Ns )
		animationEndCoordinate.z = Z_FRONT;
	else
		animationEndCoordinate.z = 0;
	
	
	// set end spacing
	if( N == Ns )
		animationEndCoordinate.spacing = SIDE_SPACE_FACTOR;
	else
		animationEndCoordinate.spacing = 0;
	
	
	
	if( (isSelected == NO) && (isPreviouslySelected == NO) )
	{
		//旧選択Ｎ+- R/2 の場合は、アニメーションする。そうでない場合、アニメーションしない。

		if( N < ( Ns_previous -R/2 ) ||  ( Ns_previous +R/2 ) < N)
		{
			coordinate.rotation = animationEndCoordinate.rotation;
			coordinate.z = animationEndCoordinate.z;
			
			
			return;
		}
	}
	
	// do animate
	
	
	
	coordinate.rotation = fmod( Rotaion_Animation(time, animationStartCoordinate.rotation,animationEndCoordinate.rotation,rotateClockwise), 2*pi );
	coordinate.z = Z_Animation( time, animationStartCoordinate.z,  animationEndCoordinate.z);

	
	coordinate.spacing = Spacing_Animation( time, animationStartCoordinate.spacing,  animationEndCoordinate.spacing);
	
	animatingProgress =/* animatingProgressOffset +*/ time/DURATION1;
	
	//NSLog(@"animatingProgress %f",animatingProgress);
	
	if( animatingProgress > 1.0 ) animatingProgress = 1.0;
}

-(void)setZ:(double)z
{
	coordinate.z = z;

}
-(void)setRotation:(double)rotation
{
	coordinate.rotation = rotation;

}
-(double)rotationAngle
{
	return coordinate.rotation;
}
-(void)setSpacing:(double)spacing
{
	coordinate.spacing = spacing;
}

-(void)setCoordinate:(CoverCoordinate)c
{
	coordinate= c;
}

-(CoverCoordinate)coordinate
{

	return coordinate;
}

/*
-(void)setRotation:(double)newRotation
{
	coordinate.rotation = newRotation;	
}

-(void)setZ:(double)newZ
{
	coordinate.z = newZ;	
}
*/

-(int)n
{
	return n;	
}

-(double)N
{
	return R*(coordinate.r-1)+ n;
}

- (NSComparisonResult)comparePosition:(MNNodeObject*)obj
{
	CoverCoordinate  c = [obj coordinate];
	
	double angle = coordinate.angle;
	
	if( angle > pi ) angle = 2*pi - angle;
	if( c.angle > pi ) c.angle = 2*pi - c.angle;
	
	
	if( angle < c.angle ) return NSOrderedAscending;
	if( angle > c.angle ) return NSOrderedDescending;
	return NSOrderedSame;
}




#pragma mark DEBUG



-(void)drawRound:(NSValue*)val
{
	NSRect rect = [val rectValue];
	double x = ((rect.size.width/3 ) + coordinate.z) * cos( -coordinate.angle +pi/2);
	double y = ((rect.size.width/3 )+ coordinate.z) * sin( -coordinate.angle +pi/2);

	x += NSMidX(rect);
	y += NSMidY(rect);
	
	double N = [self N];
	
	NSString* string = [NSString stringWithFormat:@"n=%d,N=%f r=%f, z=%f,\n angle=%f, rot=%f",n,N, coordinate.r,coordinate.z,coordinate.angle,coordinate.rotation];
	
	NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:([self isVisible]?(isSelected?[NSColor blueColor]:(isPreviouslySelected?[NSColor greenColor]:[NSColor blackColor])):[NSColor redColor]), NSForegroundColorAttributeName,nil ];
	
	[string drawAtPoint:NSMakePoint(x,y) withAttributes:dict];

	////NSLog(@"drawRound %@",string);

	
	///
	
	
	
	
	//draw linear
	
	double time = 0;
	NSTimeInterval currentDate = [[NSDate date] timeIntervalSinceReferenceDate];
	
	


	double linearX = ConvertToLinear( [[[NSApp delegate] valueForKey:@"animationTimeForDebug"] doubleValue],  coordinate,  [[NSApp delegate] valueForKey:@"nodes"], 
									  [[[NSApp delegate] valueForKey:@"Ns"] doubleValue],  
									 [[[NSApp delegate] valueForKey:@"Ns_previous"] doubleValue] ,  0, NSMaxX(rect) );
	
	
	////NSLog(@"linearX  %d %f %f",n,linearX, [[[NSApp delegate] valueForKey:@"animationTimeForDebug"] doubleValue]);
	
	 string = [NSString stringWithFormat:@"%.0f",R*(coordinate.r-1)+n];
	
	[string drawAtPoint:NSMakePoint(NSMaxX(rect)-linearX, 20 - coordinate.z) withAttributes:nil];
			

}



#pragma mark MNC3DT

-(void)projectedPlainVerticesWithCamera:(C3DTCamera*)camera intoPoints:(NSPointPointer)points inView:(C3DTView*)view
	//four points
{
	C3DTPlainSurface* aGeometry = [self geometry];
	
	
	if( aGeometry != nil && [aGeometry isKindOfClass: [MNCFCover class]] )
	{
		float depth = [aGeometry depth];
		float width = [aGeometry width];
		
		
		_C3DTVector vertex1 = {-width/2, 0,+depth/2, 1.0};
		_C3DTVector vertex2 = {+width/2, 0,+depth/2, 1.0};
		_C3DTVector vertex3 = {+width/2, 0,-depth/2, 1.0};
		_C3DTVector vertex4 = {-width/2, 0,-depth/2, 1.0};
		
		
		glPushMatrix();
		
		
		if (_hasTransform) {
			
			// Translation
			if ((_translation.x != 0.0) || (_translation.y != 0.0) || (_translation.z != 0.0)) {
				glTranslatef(_translation.x, _translation.y, _translation.z);
			}
			
			// Scaling
			if ((_scaling.x != 1.0) || (_scaling.y != 1.0) || (_scaling.z != 1.0)) {
				glScalef(_scaling.x, _scaling.y, _scaling.z);
			}
			
			// Rotation
			glTranslatef(-_rotationCenter.x, -_rotationCenter.y, -_rotationCenter.z);
			
			if (_rotation.x != 0.0)
				glRotatef(_rotation.x, 1.0f, 0.0f, 0.0f);
			if (_rotation.y != 0.0)
				glRotatef(_rotation.y, 0.0f, 1.0f, 0.0f);
			if (_rotation.z != 0.0)
				glRotatef(_rotation.z, 0.0f, 0.0f, 1.0f);
			
			
			
			//glTranslatef(_rotationCenter.x, _rotationCenter.y, _rotationCenter.z);
		}
		
		//GLdouble projmatrix[16];
		//GL_MODELVIEW_MATRIX
		
		/*
		glGetDoublev(GL_PROJECTION_MATRIX,projmatrix);
		_C3DTVector tv1 = MNVectorTransform(vertex1, projmatrix);
		_C3DTVector tv2 = MNVectorTransform(vertex2, projmatrix);
		_C3DTVector tv3 = MNVectorTransform(vertex3, projmatrix);
		_C3DTVector tv4 = MNVectorTransform(vertex4, projmatrix);
		*/
		
		
		NSPoint point1 = 	[flowView convertToWindowCoords: vertex1];
		NSPoint point2 = 	[flowView convertToWindowCoords: vertex2];
		NSPoint point3 = 	[flowView convertToWindowCoords: vertex3];
		NSPoint point4 = 	[flowView convertToWindowCoords: vertex4];
		
		
		
		
		glPopMatrix();
		
		
		points[0] = point1;
		points[1] = point2;
		points[2] = point3;
		points[3] = point4;
		
	}else
	{
		points[0] = NSZeroPoint;
		points[1] = NSZeroPoint;
		points[2] = NSZeroPoint;
		points[3] = NSZeroPoint;
	}
	
	
	
	//return points;
}



@end
