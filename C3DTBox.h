//
//  C3DTBox.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTBox.h,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTBox.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTGeometry.h"


@interface C3DTBox : C3DTGeometry {
    float		_width;
    float		_height;
    float		_depth;
}

+ (C3DTBox *)boxWithWidth: (float)aWidth height: (float)aHeight depth: (float)aDepth;

- (id)initWithWidth: (float)aWidth height: (float)aHeight depth: (float)aDepth;

- (float)width;
- (void)setWidth: (float)aSize;
- (float)height;
- (void)setHeight: (float)aSize;
- (float)depth;
- (void)setDepth: (float)aSize;

- (void)buildBoxWithWidth: (float)aWidth height: (float)aHeight depth: (float)aDepth;

@end
