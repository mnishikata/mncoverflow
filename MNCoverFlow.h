#import "FlowView.h"

/* NOT thread safe */

@interface FlowView (Public)
-(void)reloadData;
-(void)pushImage:(NSImage*)image forCoverAtIndex:(unsigned)index;
-(BOOL)needsCoverAtIndex:(unsigned)index;
-(void)selectCoverAtIndex:(unsigned)index;
-(void)reloadDataWithInsertionEffect;
-(void)selectCoverAtIndexWithoutAnimation:(unsigned)index;

@end

@protocol FlowViewDataSource
-(NSString*)titleForCoverAtIndex:(unsigned)index;
-(NSString*)subtitleForCoverAtIndex:(unsigned)index;

-(NSImage*)imageForCoverAtIndex:(unsigned)index;

-(unsigned)numberOfCovers;

@end

@protocol FlowViewDelegate
-(void)flowView:(FlowView*)aFlowView didSelectCoverAtIndex:(unsigned)index;
-(void)flowView:(FlowView*)aFlowView doubleClickedCoverAtIndex:(unsigned)index;
-(void)flowView:(FlowView*)aFlowView didDrawTitleViewInFullscreenMode:(BOOL)flag;
-(NSMenu*)flowView:(FlowView*)aFlowView menuForEvent:(NSEvent*)theEvent;

-(void)flowViewFullScreenModeOff:(FlowView*)aFlowView;
-(void)flowViewFullScreenModeOn:(FlowView*)aFlowView;

@end
