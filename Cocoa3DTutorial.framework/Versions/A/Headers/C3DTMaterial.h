//
//  C3DTMaterial.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue May 20 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTMaterial.h,v 1.1.1.1 2003/06/10 18:09:24 pmanna Exp $
//
// $Log: C3DTMaterial.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTStyle.h"
#import	"C3DTMath.h"
#import <OpenGL/glu.h>

@interface C3DTMaterial : C3DTStyle {
    _C3DTVector	_ambient;		// Ambient values (RGBA)
    _C3DTVector	_diffuse;		// Diffuse value (RGBA)
    _C3DTVector	_specular;		// Specular value (RGBA)
    _C3DTVector	_emission;		// Light emission value (RGBA)

    _C3DTVector	_savedAmbient;
    _C3DTVector	_savedDiffuse;
    _C3DTVector	_savedSpecular;
    _C3DTVector	_savedEmission;
    
}

+ (id)material;

- (_C3DTVector)ambient;
- (void)setAmbient: (_C3DTVector)aColor;
- (void)setAmbientRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

- (_C3DTVector)diffuse;
- (void)setDiffuse: (_C3DTVector)aColor;
- (void)setDiffuseRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

- (_C3DTVector)specular;
- (void)setSpecular: (_C3DTVector)aColor;
- (void)setSpecularRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

- (_C3DTVector)emission;
- (void)setEmission: (_C3DTVector)aColor;
- (void)setEmissionRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

@end
