//
//  C3DTEntity.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTEntity.h,v 1.2 2003/07/07 09:38:37 pmanna Exp $
//
// $Log: C3DTEntity.h,v $
// Revision 1.2  2003/07/07 09:38:37  pmanna
// Added utility method for object constructor with geometry
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTTransform.h"
#import "C3DTGeometry.h"

@interface C3DTEntity : C3DTTransform {
    C3DTGeometry		*_geometry;
}

+ (C3DTEntity *)entityWithStyle: (C3DTStyle *)aStyle;
+ (C3DTEntity *)entityWithStyle: (C3DTStyle *)aStyle geometry: (C3DTGeometry *)aGeometry;

- (C3DTGeometry *)geometry;
- (void)setGeometry: (C3DTGeometry *)aGeometry;
- (NSPoint)convertToWindowCoords:(_C3DTVector)objVector;
-( NSPointPointer)projectedPlainVertices;

@end
