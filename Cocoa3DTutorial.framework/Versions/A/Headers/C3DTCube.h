//
//  C3DTCube.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTCube.h,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTCube.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTBox.h"


@interface C3DTCube : C3DTBox {
    
}

+ (C3DTCube *)cubeWithEdgeSize: (float)aSize;

- (id)initWithEdgeSize: (float)aSize;

- (float)size;
- (void)setSize: (float)aSize;

@end
