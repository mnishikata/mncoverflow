//
//  C3DTColor.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTColor.h,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTColor.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTStyle.h"
#import "C3DTMath.h"

@interface C3DTColor : C3DTStyle {
    _C3DTVector	_color;
    
    _C3DTVector	_savedColor;
}

+ (C3DTColor *)defaultColor;
+ (C3DTColor *)colorWithRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

- (id)initWithColorRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

- (_C3DTVector)color;
- (void)setColor: (_C3DTVector)aColor;
- (void)setColorRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

@end
