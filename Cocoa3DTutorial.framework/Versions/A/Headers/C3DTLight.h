//
//  C3DTLight.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTLight.h,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTLight.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTObject.h"
#import "C3DTMath.h"

@interface C3DTLight : C3DTObject {
    int			_lightId;		// Private ID from GL_LIGHT0 on, calculated by the class itself

    _C3DTVector	_pos;			// Light position (X,Y,Z + 0.0 -> infinite directional, 1.0 -> positional)
    _C3DTVector	_ambient;		// Ambient values (RGBA)
    _C3DTVector	_diffuse;		// Diffuse value (RGBA)
    _C3DTVector	_specular;		// Specular value (RGBA)
    _C3DTVector	_attenuation;	// Attenuation values (constant, linear, quadratic, unused)
}

+ (int)nextLightId;

+ (id)light;

- (id)initWithId: (int)lightId;

- (_C3DTVector)pos;
- (void)setPos: (_C3DTVector)aPos;
- (void)setPosX: (float)x Y: (float)y Z: (float)z directional: (BOOL)dFlag;

- (_C3DTVector)ambient;
- (void)setAmbient: (_C3DTVector)aColor;
- (void)setAmbientRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

- (_C3DTVector)diffuse;
- (void)setDiffuse: (_C3DTVector)aColor;
- (void)setDiffuseRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

- (_C3DTVector)specular;
- (void)setSpecular: (_C3DTVector)aColor;
- (void)setSpecularRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;

- (_C3DTVector)attenuation;
- (void)setAttenuationConstant: (float)c linear: (float)l quadratic: (float)q;

- (void)apply;

@end
