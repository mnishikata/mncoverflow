//
//  C3DTAnimatedProcTexture.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Mon Jul 07 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTAnimatedProcTexture.h,v 1.1 2003/07/08 11:25:19 pmanna Exp $
//
// $Log: C3DTAnimatedProcTexture.h,v $
// Revision 1.1  2003/07/08 11:25:19  pmanna
// Initial import in CVS
//
//

#import "C3DTProcTexture.h"

@interface C3DTAnimatedProcTexture : C3DTProcTexture {
    _C3DTVector		_totTextureSize;
    NSTimeInterval	_animateInterval;
    
    int				_animationStep;
    NSTimer			*_animateTimer;
}

+ (C3DTAnimatedProcTexture * )perlinTextureWithSize: (_C3DTVector)aSize
                                          fromColor: (_C3DTVector)startColor
                                            toColor: (_C3DTVector)endColor
                                            density: (float)aDensity
                                        persistence: (float)aPersistence
                                            octaves: (int)octaves
                                           interval: (NSTimeInterval)anInterval;

+ (C3DTAnimatedProcTexture * )waveTextureWithSize: (_C3DTVector)aSize
                                        fromColor: (_C3DTVector)startColor
                                          toColor: (_C3DTVector)endColor
                                          density: (float)aDensity
                                      persistence: (float)aPersistence
                                          octaves: (int)octaves
                                         interval: (NSTimeInterval)anInterval;

+ (C3DTAnimatedProcTexture * )grainTextureWithSize: (_C3DTVector)aSize
                                         fromColor: (_C3DTVector)startColor
                                           toColor: (_C3DTVector)endColor
                                           density: (float)aDensity
                                       persistence: (float)aPersistence
                                           octaves: (int)octaves
                                          interval: (NSTimeInterval)anInterval;

- (id)initTextureWithType: (procTextureType)aType size: (_C3DTVector)aSize
                fromColor: (_C3DTVector)startColor toColor: (_C3DTVector)endColor density: (float)aDensity
              persistence: (float)aPersistence octaves: (int)octaves interval: (NSTimeInterval)anInterval;


- (NSTimeInterval)animateInterval;
- (void)setAnimateInterval: (NSTimeInterval)anInterval;

- (void)start;
- (void)stop;

- (void)doAnimate: (NSTimer *)timer;

@end
