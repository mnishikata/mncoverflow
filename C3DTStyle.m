//
//  C3DTStyle.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTStyle.m,v 1.1.1.1 2003/06/10 18:09:24 pmanna Exp $
//
// $Log: C3DTStyle.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTStyle.h"

static	int		styleCounterId = 0;

@implementation C3DTStyle

- (id)init
{
    return [super initWithName: [NSString stringWithFormat: @"Style-%ld", styleCounterId++]];
}

// Default style does nothing
- (void)apply
{
    // ???:pmanna:20030524 Generate an NSException when called, as to make the class abstract?
}

- (void)restore
{
    // ???:pmanna:20030524 Generate an NSException when called, as to make the class abstract?
}

- (BOOL)isReference {
    return _isReference;
}

- (void)setIsReference: (BOOL)newIsReference
{
    _isReference = newIsReference;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeBool: _isReference forKey: @"StyleIsReference"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    _isReference = [coder decodeBoolForKey: @"StyleIsReference"];

    return self;
}

@end
