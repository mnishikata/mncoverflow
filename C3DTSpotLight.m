//
//  C3DTSpotLight.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTSpotLight.m,v 1.1.1.1 2003/06/10 18:09:24 pmanna Exp $
//
// $Log: C3DTSpotLight.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTSpotLight.h"
#import <OpenGL/glu.h>

@implementation C3DTSpotLight

+ (id)spotLight
{
    int		aLightId = [C3DTLight nextLightId];

//NSLog(@"aLightId %x",aLightId);
    if (aLightId >= GL_LIGHT0)
        return [[[C3DTSpotLight alloc] initWithId: aLightId] autorelease];
    else
        return nil;
}

- (id)initWithId: (int)lightId
{
    if ((self = [super initWithId: lightId]) != nil) {
        // Generates the standard values, according to the OpenGL Reference
        _spotDirection.x = _spotDirection.y = _spotDirection.w = 0.0;	_spotDirection.z = -1.0;
        _spotExponent = 0.0f;
        _spotCutoff = 180.0f;
    }

    return self;
}


- (_C3DTVector)spotDirection
{
    return _spotDirection;
}

- (void)setSpotDirection: (_C3DTVector)aDirection
{
    _spotDirection = aDirection;

    glLightfv(_lightId, GL_SPOT_DIRECTION, (float *)&_spotDirection);
}

- (void)setSpotDirectionX: (float)x Y: (float)y Z: (float)z
{
    _pos.x = x;
    _pos.y = y;
    _pos.z = z;

    glLightfv(_lightId, GL_SPOT_DIRECTION, (float *)&_spotDirection);
}

- (float)spotExponent
{
    return _spotExponent;
}

- (void)setSpotExponent: (float)aValue
{
    if ((aValue < 0.0f) || (aValue > 128.0)) {
        // $$$ Generate appropriate error
        return;
    }

    _spotExponent = aValue;

    glLightf(_lightId, GL_SPOT_EXPONENT, _spotExponent);
}

- (float)spotCutoff
{
    return _spotCutoff;
}

- (void)setSpotCutoff: (float)aValue
{
    if ((aValue < 0.0f) || ((aValue > 90.0) && (aValue != 180.0))) {
        // $$$ Generate appropriate error
        return;
    }

    _spotCutoff = aValue;

    glLightf(_lightId, GL_SPOT_CUTOFF, _spotCutoff);
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _spotExponent forKey: @"SpotLightExponent"];
    [coder encodeFloat: _spotCutoff forKey: @"SpotLightCutoff"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_spotDirection length: sizeof(_C3DTVector) forKey: @"SpotLightDirection"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_spotDirection length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    _spotExponent = [coder decodeFloatForKey: @"SpotLightExponent"];
    _spotCutoff = [coder decodeFloatForKey: @"SpotLightCutoff"];
    if ( [coder allowsKeyedCoding] ) {
        _spotDirection	= *(_C3DTVector *)[coder decodeBytesForKey: @"SpotLightDirection" returnedLength: &len];
    } else {
        _spotDirection	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }

    [self setSpotExponent: _spotExponent];
    [self setSpotCutoff: _spotCutoff];
    [self setSpotDirection: _spotDirection];
    
    return self;
}

@end
