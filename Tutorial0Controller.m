//
//  TutorialController.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: Tutorial0Controller.m,v 1.3 2003/07/07 09:40:19 pmanna Exp $
//
// $Log: Tutorial0Controller.m,v $
// Revision 1.3  2003/07/07 09:40:19  pmanna
// Modified to use new constructor for Entity
//
// Revision 1.2  2003/06/14 09:23:17  pmanna
// Added option for orthographic view
//
// Revision 1.1.1.1  2003/06/10 18:09:37  pmanna
// Initial import
//
//

#import "Tutorial0Controller.h"
#import <Cocoa3DTutorial/C3DTCube.h>
#import <Cocoa3DTutorial/C3DTColor.h>
#import <Cocoa3DTutorial/C3DTEntity.h>
#import <Cocoa3DTutorial/C3DTPlainSurface.h>
#import <Cocoa3DTutorial/C3DTTexture.h>
#import "MNCFCover.h"


@interface Tutorial0Controller (Private)

- (void)_fillScene;

@end

@implementation Tutorial0Controller

- (void)applicationDidFinishLaunching: (NSNotification *)notification
{
    C3DTCamera	*aCamera = [view camera];
    _C3DTVector	cameraPos = {{ -15.0, -10.0, 40.0, 0.0 }};

    [aCamera setPos: cameraPos];
    [aCamera setLookAtX: 0.0f Y: 0.0f Z: 0.0f];
    
    [self _fillScene];

    [lightX setFloatValue: 10.0f];
    [lightY setFloatValue: 20.0f];
    [lightZ setFloatValue: 20.0f];

    [cameraX setFloatValue: cameraPos.x];
    [cameraY setFloatValue: cameraPos.y];
    [cameraZ setFloatValue: cameraPos.z];
    
    cameraPos = cartesianToSpherical(cameraPos);
    [distance setFloatValue: cameraPos.r];
    
    [self setLight: self];
}

- (IBAction)setPosition:(id)sender
{
    _C3DTVector	cameraPos;
    
    if ([sender tag] == 3) {
        cameraPos = cartesianToSpherical([[view camera] pos]);
        
        cameraPos.r = [distance floatValue];
        
        [[view camera] rotateToAzimuth: cameraPos.theta
                             elevation: cameraPos.phi
                              distance: cameraPos.r];

        cameraPos = sphericalToCartesian(cameraPos);

        [cameraX setFloatValue: cameraPos.x];
        [cameraY setFloatValue: cameraPos.y];
        [cameraZ setFloatValue: cameraPos.z];
    } else {
        [[view camera] setPosX: [cameraX floatValue]
                             Y: [cameraY floatValue]
                             Z: [cameraZ floatValue]];

        cameraPos = cartesianToSpherical([[view camera] pos]);
        
        [distance setFloatValue: cameraPos.r];
    }

    [view setNeedsDisplay: YES];
}

- (IBAction)setLight:(id)sender
{
    C3DTLight	*aLight = [[[view scene] lights] objectAtIndex: 0];

    if (aLight == nil)
        return;

    [aLight setPosX: [lightX floatValue]
                  Y: [lightY floatValue]
                  Z: [lightZ floatValue]
        directional: YES];

    [view setNeedsDisplay: YES];
}

- (void)setCameraX: (float)x Y: (float)y Z: (float)z
{
    [[view camera] setPosX: x Y: y Z: z];
    
    [view setNeedsDisplay: YES];
}

@end


@implementation Tutorial0Controller (Private)

- (void)_fillScene
{
    // Create a geometrical box, its style, and the entity that will combine them with transformation
    MNCFCover	*aPlain = [MNCFCover planeWithWidth:20.0f depth:20.0f meridians:0 parallels:0 textureRepeat:NO];
    C3DTTexture		*tubeColor= [C3DTTexture textureWithFile: @"art.tiff"];
    C3DTEntity		*tubeEntity = [C3DTEntity entityWithStyle: tubeColor geometry: aPlain];
    C3DTEntity		*cubeEntity = tubeEntity;

    // Setup a default scene (that has some lighting already
    C3DTScene		*aScene = [C3DTScene defaultScene];
    C3DTLight		*aLight = [C3DTLight light];

    // Add the entity to the scene
    [aScene addChild: cubeEntity];

    // add the additional light
    [aScene addLight: aLight];

    // Optional: sets orthographic view instead of perspective
    // WARNING: the orthographic view doesn't change with distance, object will be very small!
    //[[view camera] setUseOrtho: YES];
    
    // Last, add the scene tree to our view
    [view setScene: aScene];
}

@end
