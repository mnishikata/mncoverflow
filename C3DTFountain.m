//
//  C3DTFountain.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Thu May 29 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTFountain.m,v 1.2 2003/06/22 09:38:11 pmanna Exp $
//
// $Log: C3DTFountain.m,v $
// Revision 1.2  2003/06/22 09:38:11  pmanna
// Modified to support ARB_point_parameters extension if present
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTFountain.h"
#import <OpenGL/glext.h>

inline void initializeParticle(_C3DTParticle *p, _C3DTVector pos, _C3DTVector dir, float size, float speed)
{
    p->position.x	= pos.x + (size * (float)((rand() % 100) - 50)) / 100.0;	// Changes pos +/- 50% size
    p->position.y	= pos.y + (size * (float)((rand() % 100) - 50)) / 100.0;
    p->position.z	= pos.z + (size * (float)((rand() % 100) - 50)) / 100.0;
    p->speed		= speed + (speed * (float)((rand() % 100) - 50)) / 100.0;	// Changes +/- 50% speed
    
    if ((dir.x == 0.0) && (dir.y == 0.0) && (dir.z == 0.0)) {
        // If direction is unspecified, make it a burst, generating a random one
        dir.x	= (float)((rand() % 100) - 50);
        dir.y	= (float)((rand() % 100) - 50);
        dir.z	= (float)((rand() % 100) - 50);
    }
    dir				= cartesianToSpherical(dir);	// Convert to spherical
    dir.r			= p->speed;						// Assign a distance
    p->direction	= sphericalToCartesian(dir);	// Convert back to coord increments
    
    p->size			= size / 2.0;
    p->life			= 1.0;														// Fixed initial value
    p->fade			= (float)((rand() % 100) + 1) / 1000.0f;					// Value from .001 to .1
}

inline void drawParticle(_C3DTParticle *p, _C3DTVector color, BOOL usePoints)
{
    glColor4f( color.x, color.y, color.z, p->life );

    if (!usePoints) {
        glBegin( GL_TRIANGLE_FAN );   // Build approx tetrahedron
        glTexCoord2f( 1.0, 1.0 );
        glVertex3f( p->position.x, p->position.y + p->size,  p->position.z );   // Top
        glTexCoord2f( 0.0, 1.0 );
        glVertex3f(  p->position.x - p->size, p->position.y - p->size,  p->position.z - p->size );   // Bot left
        glTexCoord2f( 1.0, 0.0 );
        glVertex3f( p->position.x + p->size, p->position.y - p->size,  p->position.z - p->size );   // Bot right
        glTexCoord2f( 0.0, 0.0 );
        glVertex3f( p->position.x, p->position.y - p->size,  p->position.z + p->size );   // Back
        glEnd();
    } else {
        glVertex3f( p->position.x, p->position.y,  p->position.z );   // Point
    }
}

inline void moveParticle(_C3DTParticle *p)
{
    p->position.x	+= p->direction.x;
    p->position.y	+= p->direction.y;
    p->position.z	+= p->direction.z;
}

_C3DTParticle *allocateParticles(int count, _C3DTVector pos, _C3DTVector dir, float size, float speed)
{
    _C3DTParticle	*partArray	= calloc(count, sizeof(_C3DTParticle));
    _C3DTParticle	*p			= partArray;
    int				ii;

    for (ii = 0; p && (ii < count); ii++, p++) {
        initializeParticle(p, pos, dir, size,  speed);
    }

    return partArray;
}

@implementation C3DTFountain

+ (C3DTFountain *)fountainWithCount: (int)count position: (_C3DTVector)pos direction: (_C3DTVector)dir
                               size: (float)size speed: (float)speed
{
    return [[[C3DTFountain alloc] initWithCount: count
                                       position: pos
                                      direction: dir
                                           size: size
                                          speed: speed] autorelease];
}

- (id)initWithCount: (int)count position: (_C3DTVector)pos direction: (_C3DTVector)dir
               size: (float)size speed: (float)speed
{
    const char	*extensions;
    
    if (self = [super initWithName: [NSString stringWithFormat: @"Fountain-%d", count]]) {
        _position	= pos;
        _direction	= dir;
        _size		= size;
        _speed		= speed;
        
        if (count > 0) {
            _particleCount = count;
            particles = allocateParticles(count, pos, dir, size, speed);
        }
        else {
            // !!!:pmanna:20030530 Generate appropriate error
        }

        extensions = glGetString(GL_EXTENSIONS);
        _usePoints = gluCheckExtension("GL_ARB_point_parameters", extensions);
    }

    return self;
}

- (void)dealloc
{
    if (particles)
        free(particles);
    
    [super dealloc];
}

- (_C3DTVector)position
{
    return _position;
}

- (void)setPosition: (_C3DTVector)aPos
{
    _position = aPos;
}

- (_C3DTVector)direction
{
    return _direction;
}

- (void)setDirection: (_C3DTVector)aDir
{
    _direction	= aDir;
}

- (float)size
{
    return _size;
}

- (void)setSize: (float)aSize
{
    _size = aSize;
}

- (float)speed
{
    return _speed;
}

- (void)setSpeed: (float)aSpeed
{
    _speed	= aSpeed;
}

- (void)draw
{
    _C3DTParticle	*p	= particles;
    int				ii;
    _C3DTVector		actColor;
    _C3DTVector		attenuation = { { 0.25, 0.0, 1/60.0, 0.0 } };	// Quadratic attenuation

    glDisable(GL_CULL_FACE);			// All faces are equal and visible
    glDisable(GL_LIGHTING);				// ... and particles are their own color
    glGetFloatv(GL_CURRENT_COLOR, (GLfloat *)&actColor);
    
    if (_usePoints) {
        glPointParameterfvARB(GL_POINT_DISTANCE_ATTENUATION_ARB, (GLfloat *)&attenuation);
        glPointParameterfARB(GL_POINT_FADE_THRESHOLD_SIZE_ARB, 1.0);
        glPointSize(_size);
        glBegin(GL_POINTS);
    }

    for (ii = 0; ii < _particleCount; ii++, p++) {
        // draw the actual particle
        drawParticle(p, actColor, _usePoints);
    }

    if (_usePoints)
        glEnd();

    glColor4fv( (GLfloat *)&actColor );
    glEnable(GL_LIGHTING);
    glEnable(GL_CULL_FACE);			// Go back to default
}

- (void)animate
{
    _C3DTParticle	*p	= particles;
    int				ii;

    for (ii = 0; ii < _particleCount; ii++, p++) {
        // fade the particle
        p->life -= p->fade;

        // If its life is gone, initialize a new one, else move it to next position
        if (p->life < 0.0)
            initializeParticle(p, _position, _direction, _size, _speed);
        else
            moveParticle(p);
    }
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeInt: _particleCount forKey: @"FountainCount"];
    [coder encodeFloat: _size forKey: @"FountainSize"];
    [coder encodeFloat: _speed forKey: @"FountainSpeed"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_position length: sizeof(_C3DTVector) forKey: @"FountainPosition"];
        [coder encodeBytes: (const uint8_t *)&_direction length: sizeof(_C3DTVector) forKey: @"FountainDirection"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_position length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_direction length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    _particleCount	= [coder decodeIntForKey: @"FountainCount"];
    _size			= [coder decodeFloatForKey: @"FountainSize"];
    _speed			= [coder decodeFloatForKey: @"FountainSpeed"];
    
    if ( [coder allowsKeyedCoding] ) {
        _position	= *(_C3DTVector *)[coder decodeBytesForKey: @"FountainPosition" returnedLength: &len];
        _direction	= *(_C3DTVector *)[coder decodeBytesForKey: @"FountainDirection" returnedLength: &len];
    } else {
        _position	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _direction	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }
    
    if (_particleCount > 0) {
        particles = allocateParticles(_particleCount, _position, _direction, _size, _speed);
    }
    
    return self;
}

@end
