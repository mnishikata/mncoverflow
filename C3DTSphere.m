//
//  C3DTSphere.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTSphere.m,v 1.1.1.1 2003/06/10 18:09:24 pmanna Exp $
//
// $Log: C3DTSphere.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//


#import "C3DTSphere.h"


@interface C3DTSphere (PrivateMethods)
- (void)buildSphereWithRadius: (float)aRadius;
@end

@implementation C3DTSphere

+ (C3DTSphere *)sphereWithRadius: (float)aRadius slices: (int)aSliceNumber stacks: (int)aStackNumber
{
    return [[[C3DTSphere alloc] initWithRadius: aRadius slices: aSliceNumber stacks: aStackNumber] autorelease];
}

- (id)initWithRadius: (float)aRadius slices: (int)aSliceNumber stacks: (int)aStackNumber
{
    if ((self = [super init]) != nil) {
        if (aSliceNumber > 1)
            _slices = aSliceNumber;
        else
            _slices = 32;
        
        if (aStackNumber > 1)
            _stacks = aStackNumber;
        else
            _stacks = 32;

        [self setRadius: aRadius];
    }

    return self;
}

- (float)radius
{
    return _radius;
}

- (void)setRadius: (float)aRadius
{
    _radius = aRadius;
    
    // Creates a sphere centered on {0, 0, 0}
    [self buildSphereWithRadius: aRadius];
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _radius forKey: @"SphereRadius"];
    [coder encodeInt: _slices forKey: @"SphereSlices"];
    [coder encodeInt: _stacks forKey: @"SphereStacks"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    _radius	= [coder decodeFloatForKey: @"SphereRadius"];
    _slices	= [coder decodeIntForKey: @"SphereSlices"];
    _stacks	= [coder decodeIntForKey: @"SphereStacks"];

    [self buildSphereWithRadius: _radius];
    
    return self;
}

@end

@implementation C3DTSphere (PrivateMethods)

- (void)buildSphereWithRadius: (float)aRadius
{
    [self startDisplayList];
    
    gluQuadricDrawStyle([C3DTGeometry quadric], GLU_FILL);
    gluQuadricNormals([C3DTGeometry quadric], GLU_SMOOTH);
    gluSphere([C3DTGeometry quadric], aRadius, _slices, _stacks);

    [self stopDisplayList];
}

@end
