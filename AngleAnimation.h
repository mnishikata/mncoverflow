#import <Cocoa/Cocoa.h>
#import "Functions.h"

@interface AngleAnimation : NSAnimation
{
	double Nprogress;	
	CoverCoordinate cprime;
	
	NSRunLoop *runLoop;
	BOOL stopFlag;
	double currentProgress;
}
-(double)Nprogress;
-(void)setCurrentProgress:(NSAnimationProgress)progress;
-(void)stopAnimation;
-(void)startAnimation;
-(void)__startAnimation;
-(double)currentProgress;

@end