//
//  C3DTView.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTView.m,v 1.4 2003/06/29 13:58:11 pmanna Exp $
//
// $Log: C3DTView.m,v $
// Revision 1.4  2003/06/29 13:58:11  pmanna
// Added support for mouse up event
//
// Revision 1.3  2003/06/24 22:29:56  pmanna
// Reviewed to support more controller callbacks
//
// Revision 1.2  2003/06/15 09:37:41  pmanna
// Removed Quicktime refs: they don't belong here
//
// Revision 1.1.1.1  2003/06/10 18:09:25  pmanna
// Initial import
//
//

#import "C3DTView.h"
#import <OpenGL/glu.h>

@interface C3DTView (PrivateMethods)
- (void)_initCameraAndGL;
@end

@implementation C3DTView (PrivateMethods)

- (void)_initCameraAndGL
{
    _camera = [[C3DTCamera alloc] initWithBounds: [self bounds]];
    _backColor.x = _backColor.y = _backColor.z = 0.25;	_backColor.w = 0.0;
    
    // Enable some OpenGL parts by default
    glShadeModel( GL_SMOOTH );		// Enable smooth shading
    glEnable(GL_LIGHTING);			// Enable Lighting
    glEnable(GL_DEPTH_TEST);		// Enable hidden surface removal
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);			// Don't consider "internal" faces

    // As default, we use the simplest mode to track material & color
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

    glEnable(GL_BLEND);				// Enable Blending
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_TEXTURE_2D);		// Enable textures

    // Try to use arrays
    /*
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_INDEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    */
    // Really nice perspective calculations
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
}

@end

@implementation C3DTView

- (id) initWithFrame: (NSRect) frame
{
    GLuint attribs[] =
    {
        NSOpenGLPFANoRecovery,
        NSOpenGLPFAWindow,
        NSOpenGLPFAAccelerated,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFAColorSize, 24,
        NSOpenGLPFAAlphaSize, 8,
        NSOpenGLPFADepthSize, 24,
        0
    };

    NSOpenGLPixelFormat* fmt = [[NSOpenGLPixelFormat alloc] initWithAttributes: (NSOpenGLPixelFormatAttribute*) attribs];

    if (!fmt)
	{
       //NSLog(@"No OpenGL pixel format");
	}
	
    if (self = [super initWithFrame:frame pixelFormat: [fmt autorelease]]) {
        [[self openGLContext] makeCurrentContext];
        // [self _initCameraAndGL];
    }

    return self;
}

- (id)initWithCoder: (NSCoder *)decoder
{
    // This would have been called in case a NSOpenGLView was set in IB,
    // but this just doesn't work! IB-created NSOpenGLView have lots of problems...
    if (self = [super initWithCoder: decoder]) {
        [[self openGLContext] makeCurrentContext];
        // [self _initCameraAndGL];
    }

    return self;
}

- (void)awakeFromNib
{
    [self _initCameraAndGL];
}

- (void)dealloc
{
    [self clearGLContext];
    
    [_camera release];
    [_scene release];

    [super dealloc];
}

- (void)mouseDown: (NSEvent *)anEvent
{
    if (controller && [controller respondsToSelector: @selector(mouseDown:)])
        [controller mouseDown: anEvent];
    
    lastClick = [anEvent locationInWindow];
}

- (void)mouseUp: (NSEvent *)anEvent
{
    if (controller && [controller respondsToSelector: @selector(mouseUp:)])
        [controller mouseUp: anEvent];
}

- (void)mouseDragged: (NSEvent *)anEvent
{
    if (controller && [controller respondsToSelector: @selector(mouseDragged:)])
        [controller mouseDragged: anEvent];

    lastClick = [anEvent locationInWindow];
}

- (void) keyDown: (NSEvent *)anEvent
{
    if (controller && [controller respondsToSelector: @selector(keyDown:)])
        [controller keyDown: anEvent];
}

- (void)reshape
{
    [super reshape];

    [[ self openGLContext ] update ];
    // We don't use accessor here because reshape could be called before camera is set,
    // thus we'll go in an endless loop
    [_camera setBounds: [self bounds]];

    [self setNeedsDisplay: YES];
}

- (void)drawRect: (NSRect)aRect
{
    glClearColor(_backColor.x, _backColor.y, _backColor.z, _backColor.w);
    glClearDepth(1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |GL_STENCIL_BUFFER_BIT) ;

    if (_camera)
        [_scene useWith: [self camera]];

    [[self openGLContext] flushBuffer];
}

// Camera is completely owned by the view, there is no set
// It is initialized at the first call, as we're not sure initWithFrame has been called
- (C3DTCamera *)camera
{
    if (_camera == nil) {
        [self _initCameraAndGL];
    }

    return _camera;
}

- (C3DTScene *)scene
{
    return _scene;
}

- (void)setScene: (C3DTScene *)aScene
{
    [aScene retain];
    [_scene release];
    _scene = aScene;
}

- (_C3DTVector)backColor
{
    return _backColor;
}

- (void)setBackColor: (_C3DTVector)aColor
{
    _backColor = aColor;

    [self setNeedsDisplay: YES];
}

- (void)refresh: (NSNotification *)aNotification
{
    [self setNeedsDisplay: YES];
}

- (void)registerWith: (id)aSender forNotification: (NSString *)aNotification
{
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(refresh:)
                                                 name: aNotification
                                               object: aSender];
}

- (void)unregisterWith: (id)aSender forNotification: (NSString *)aNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: aNotification
                                                  object: aSender];
}

@end
