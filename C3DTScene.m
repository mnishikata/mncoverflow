//
//  C3DTScene.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTScene.m,v 1.2 2003/06/29 13:54:49 pmanna Exp $
//
// $Log: C3DTScene.m,v $
// Revision 1.2  2003/06/29 13:54:49  pmanna
// Added handling of picking objects
//
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTScene.h"
#import "C3DTGroup.h"

#define	SELECT_BUF_SIZE	1024

@implementation C3DTScene

+ (C3DTScene *)defaultScene
{
    return [[[C3DTScene alloc] init] autorelease];
}

- (id)init
{
    if ((self = [super init]) != nil) {
        _geometries	= [[NSMutableArray array] retain];
        _styles		= [[NSMutableArray array] retain];
        _lights		= [[NSMutableArray array] retain];
        _selection	= [[NSMutableArray array] retain];

        // Adds at least a default light
        [self addLight: [C3DTLight light]];
    }

    return self;
}

- (void)dealloc
{
    [_geometries release];
    [_styles release];
    [_lights release];
    [_selection release];

    [super dealloc];
}

- (void)setParent: (C3DTNode *)newParent
{
    NSDictionary	*userInfo = [NSDictionary dictionaryWithObjectsAndKeys: self, @"Scene", newParent, @"Parent", nil];

    [[NSException exceptionWithName: NSInvalidArgumentException
                                reason: @"Scene object cannot have parent!"
                                             userInfo: userInfo] raise];
}

- (void)useWith: (id)obj
{
    C3DTCamera		*camera		= (C3DTCamera *)obj;
    C3DTLight		*light;
    NSEnumerator	*enumLights	= [[self lights] objectEnumerator];
    GLuint			selectBuffer[SELECT_BUF_SIZE];
    GLint			hits;

    if ([camera selectionMode]) {
        NSEnumerator	*enumSelection = [_selection objectEnumerator];
        C3DTGroup		*selectedObj;

        while ((selectedObj = (C3DTGroup *)[enumSelection nextObject]) != nil)
            [selectedObj setSelected: NO];
        
        [_selection removeAllObjects];

        memset(selectBuffer, 0, SELECT_BUF_SIZE * sizeof(GLuint));
        
        glSelectBuffer(SELECT_BUF_SIZE, selectBuffer);
        glRenderMode(GL_SELECT);
    }
    
    // Sets up the view matrix
    [camera useWith: self];

    if (![camera selectionMode]) {
        // Apparently, light have to be specified before every scene draw
        while ((light = [enumLights nextObject]) != nil)
            [light apply];
    }
    
    // Starts drawing the scene objects
    [super useWith: obj];

    if ([camera selectionMode]) {
        int		ii, jj, num_names;
        GLuint	*hitPtr = selectBuffer, actName;
        
        hits = glRenderMode(GL_RENDER);
        // Process what's in the selection buffer
		
		//get the first depth//
		GLuint*   p;
		GLboolean save;
		GLuint    depth = (GLuint)-1;
		GLuint    picked;

		p = selectBuffer;

		
       for (ii = 0; ii < hits; ii++) {
		   save = GL_FALSE;
		   num_names = *p;			/* number of names in this hit */
		   p++;
		   
		   if (*p <= depth) {			/* check the 1st depth value */
			   depth = *p;
			   save = GL_TRUE;
		   }
		   p++;
		   if (*p <= depth) {			/* check the 2nd depth value */
			   depth = *p;
			   save = GL_TRUE;
		   }
		   p++;
		   
		   if (save)
			   picked = *p;
		   
		   p += num_names;			/* skip over the rest of the names */   
	   }


		C3DTGroup	*selObject;
//NSLog(@"selecting %@",[NSString stringWithFormat: @"Node-%ld",picked]);

		// Mark the object as selected
		if ((selObject = (C3DTGroup *)[self findNamedObject: [NSString stringWithFormat: @"Node-%ld",
			picked]]) != nil) {
			[selObject setSelected: YES];
			[_selection addObject: selObject];
		}
          
        
    }
}

-(NSArray*)selectedObjects
//group array
{
	return _selection;
}

- (NSArray *)geometries
{
    return _geometries;
}

- (void)addGeometry: (C3DTGeometry *) aGeometry;
{
    if (aGeometry != nil) {
        [_geometries addObject: aGeometry];
        [aGeometry setIsReference: YES];
    }
}

- (void)removeGeometry:(C3DTGeometry *)aGeometry
{
    if (aGeometry != nil) {
        [aGeometry setIsReference: NO];
        [_geometries removeObject: aGeometry];
    }
}

- (void)removeGeometryNamed:(NSString *)aGeometryName
{
    [self removeGeometry: (C3DTGeometry *)[self findNamedObject: aGeometryName inArray: [self geometries]]];
}


- (NSArray *)styles
{
    return _styles;
}

- (void)addStyle: (C3DTStyle *)aStyle
{
    if (aStyle != nil) {
        [aStyle setIsReference: YES];
        [_styles addObject: aStyle];
    }
}

- (void)removeStyle:(C3DTStyle *)aStyle
{
    if (aStyle != nil) {
        [aStyle setIsReference: NO];
        [_styles removeObject: aStyle];
    }
}

- (void)removeStyleNamed:(NSString *)aStyleName
{
    [self removeStyle: (C3DTStyle *)[self findNamedObject: aStyleName inArray: [self styles]]];
}


- (NSArray *)lights
{
    return _lights;
}

- (void)addLight: (C3DTLight *) aLight
{
    if (aLight != nil)
        [_lights addObject: aLight];
}

- (void)removeLight:(C3DTLight *)aLight
{
    if (aLight != nil)
        [_lights removeObject: aLight];
}

- (void)removeLightNamed:(NSString *)aLightName
{
    [self removeLight: (C3DTLight *)[self findNamedObject: aLightName inArray: [self lights]]];
}


- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        [coder encodeObject: _geometries forKey: @"SceneGeometries"];
        [coder encodeObject: _styles forKey: @"SceneStyles"];
        [coder encodeObject: _lights forKey: @"SceneLights"];
    } else {
        [coder encodeObject: _geometries];
        [coder encodeObject: _styles];
        [coder encodeObject: _lights];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        _geometries	= [[coder decodeObjectForKey: @"SceneGeometries"] retain];
        _styles		= [[coder decodeObjectForKey: @"SceneStyles"] retain];
        _lights		= [[coder decodeObjectForKey: @"SceneLights"] retain];
    } else {
        _geometries	= [[coder decodeObject] retain];
        _styles		= [[coder decodeObject] retain];
        _lights		= [[coder decodeObject] retain];
    }

    return self;
}

@end
