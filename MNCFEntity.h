//
//  C3DTEntity.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTEntity.h,v 1.2 2003/07/07 09:38:37 pmanna Exp $
//
// $Log: C3DTEntity.h,v $
// Revision 1.2  2003/07/07 09:38:37  pmanna
// Added utility method for object constructor with geometry
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTEntity.h"
#import "C3DTCamera.h"
#import "C3DTPlainSurface.h"
#import "C3DTView.h"

@interface MNCFEntity : C3DTEntity  {
	BOOL _exposing;
	BOOL _hiding;
	BOOL _exposed;
	BOOL _smooth;
	BOOL _hidden;
	unsigned long virtualCoverNumber;

}

+ (MNCFEntity *)entityWithStyle: (C3DTStyle *)aStyle;
+ (MNCFEntity *)entityWithStyle: (C3DTStyle *)aStyle geometry: (C3DTGeometry *)aGeometry;
- (id)initWithStyle: (C3DTStyle *)aStyle;
- (void)useWith: (id)anObject;
-(void)setExposing:(BOOL)flag;
-(BOOL)isExposing;
-(BOOL)isSmooth;
-(void)setSmooth:(BOOL)flag;
-(void)setHidden:(BOOL)flag;
-(BOOL)isHidden;
-(void)setExposed:(BOOL)flag;
-(BOOL)isExposed;
-(void)setHiding:(BOOL)flag;
-(BOOL)isHiding;
-(unsigned long)virtualCoverNumber;
-(void)setVirtualCoverNumber:(unsigned long)num;
-(void)projectedPlainVerticesWithCamera:(C3DTCamera*)camera intoPoints:(NSPointPointer)points inView:(C3DTView*)view;
- (NSPoint)convertToWindowCoords:(_C3DTVector)objVector;


@end
