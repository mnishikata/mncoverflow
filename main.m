//
//  main.m
//  MNCoverFlow
//
//  Created by Masatoshi Nishikata on 08/02/29.
//  Copyright __MyCompanyName__ 2008. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
