/* ScrollerView */

#import <Cocoa/Cocoa.h>
#import "FlowView.h"

@interface MyScroller : NSScroller
{
}
-(BOOL)isOpaque;
-(void)setup;
-(void)drawRect:(NSRect)rect;
- (void)drawKnob:(NSRect)rect;
- (void)drawKnobSlotInRect:(NSRect)rect ;
@end



@interface ScrollerView : NSView
{
	MyScroller* _scroller;
	NSButton *fullscreenButton;
	FlowView* delegate;
	NSButton *rb;
	NSButton *lb;
}
- (id)initWithFrame:(NSRect)frameRect;
-(void)awakeFromNib;
-(void)button:(id)sender;
-(void)setup;
- (void)setFloatValue:(float)aFloat knobProportion:(float)knobProp;
-(float)floatValue;
-(id)verticalScroller;
-(id)horizontalScroller;
-(void)drawRect:(NSRect)rect;
-(BOOL)hasHorizontalScroller;
-(BOOL)hasVerticalScroller;
- (void) scrolled: (NSScroller *)scroller;
-(void)setDelegate:(id)obj;
-(void)dealloc;
-(void)setSmallSize:(BOOL)flag;


@end
