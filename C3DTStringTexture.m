//
//  C3DTStringTexture.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun Jun 29 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTStringTexture.m,v 1.3 2003/07/08 11:21:36 pmanna Exp $
//
// $Log: C3DTStringTexture.m,v $
// Revision 1.3  2003/07/08 11:21:36  pmanna
// Modified to use a more generic generateTexture
//
// Revision 1.2  2003/06/30 05:29:32  pmanna
// Added archiving code
//
// Revision 1.1  2003/06/29 20:23:58  pmanna
// Initial import in CVS
//
//

#import "C3DTStringTexture.h"

@interface C3DTStringTexture (PrivateMethods)
- (BOOL)_createStringTexture;
@end

@implementation C3DTStringTexture

+ (C3DTStringTexture *)textureWithString: (NSString *)aText font: (NSString *)fontName size: (float)fontSize
{
    _C3DTVector	fColor = { { 0.0, 0.0, 0.0, 1.0 } };	// Black, full alpha
    _C3DTVector	bColor = { { 1.0, 1.0, 1.0, 1.0 } };	// White, full alpha
    
    return [[[C3DTStringTexture alloc] initWithString: aText
                                                 font: fontName
                                                 size: fontSize
                                           frontColor: fColor
                                            backColor: bColor] autorelease];
}

+ (C3DTStringTexture *)textureWithString: (NSString *)aText
                                    font: (NSString *)fontName
                                    size: (float)fontSize
                              frontColor: (_C3DTVector)fColor
                               backColor: (_C3DTVector)bColor
{
    return [[[C3DTStringTexture alloc] initWithString: aText
                                                 font: fontName
                                                 size: fontSize
                                           frontColor: fColor
                                            backColor: bColor] autorelease];
}

- (id)initWithString: (NSString *)aText
                font: (NSString *)fontName
                size: (float)fontSize
          frontColor: (_C3DTVector)fColor
           backColor: (_C3DTVector)bColor
{
    if (aText == nil)
        return nil;

    if (self = [super initWithName: aText]) {
        _color.x = _color.y = _color.z = _color.w = 1.0;

        _fontName	= [fontName retain];
        _fontSize	= fontSize;
        _frontColor	= fColor;
        _backColor	= bColor;
        
        if ([self _createStringTexture])
        {
            [self generateTextureWithBytes: _textureBytes refresh: NO];
        }
    }

    return self;
}


- (void)dealloc
{
    [_fontName release];
    
    [super dealloc];
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeObject: _fontName forKey: @"StringTextureFontName"];
    [coder encodeFloat: _fontSize forKey: @"StringTextureFontSize"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_frontColor length: sizeof(_C3DTVector) forKey: @"StringTextureForeColor"];
        [coder encodeBytes: (const uint8_t *)&_backColor length: sizeof(_C3DTVector) forKey: @"StringTextureBackColor"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_frontColor length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_backColor length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    _fontName	= [[coder decodeObjectForKey: @"StringTextureFontSize"] retain];
    _fontSize	= [coder decodeFloatForKey: @"StringTextureFontSize"];
    if ( [coder allowsKeyedCoding] ) {
        _frontColor	= *(_C3DTVector *)[coder decodeBytesForKey: @"StringTextureForeColor" returnedLength: &len];
        _backColor	= *(_C3DTVector *)[coder decodeBytesForKey: @"StringTextureBackColor" returnedLength: &len];
    } else {
        _frontColor	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _backColor	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }

    if ([self _createStringTexture])
    {
        [self generateTextureWithBytes: _textureBytes refresh: NO];
    }
    
    return self;
}

@end

@implementation C3DTStringTexture (PrivateMethods)

- (BOOL)_createStringTexture
{
    NSString			*s = [self name];
    NSImage				*textImage;
    NSMutableDictionary	*textAttributes = [[NSMutableDictionary alloc] init];
    NSColor				*fColor = [NSColor colorWithCalibratedRed: _frontColor.x
                                                   green: _frontColor.y
                                                    blue: _frontColor.z
                                                   alpha: _frontColor.w];
    NSColor				*bColor = [NSColor colorWithCalibratedRed: _backColor.x
                                                   green: _backColor.y
                                                    blue: _backColor.z
                                                   alpha: _backColor.w];
    NSSize				textSize;
    NSPoint				delta;
    
    [textAttributes setObject: [NSFont fontWithName: _fontName size: _fontSize] forKey: NSFontAttributeName];
    [textAttributes setObject: fColor forKey: NSForegroundColorAttributeName];
    [textAttributes setObject: bColor forKey: NSBackgroundColorAttributeName];

    // Calculate how much space we'll actually use
    textSize = [s sizeWithAttributes: textAttributes];

    // Allocate correct size for texture
    _textureSize.width	= (float)nextPowerOf2((int)textSize.width);
    _textureSize.height	= (float)nextPowerOf2((int)textSize.height);

    // Calculate the delta to center the text in the texture
    delta.x		= (_textureSize.width - textSize.width) / 2.0;
    delta.y		= (_textureSize.height - textSize.height) / 2.0;
    
    textImage = [[[NSImage alloc] initWithSize: _textureSize] autorelease];

    [textImage lockFocus];

    [textImage setBackgroundColor: bColor];
    NSEraseRect(NSMakeRect(0.0, 0.0, _textureSize.width, _textureSize.height));
    
    [s drawInRect: NSMakeRect(delta.x, delta.y, textSize.width, textSize.height) withAttributes: textAttributes];

    [textImage unlockFocus];

    [textAttributes release];
    
    return [self bitmapFromImageRep: [NSBitmapImageRep imageRepWithData: [textImage TIFFRepresentation]]];
}

@end

