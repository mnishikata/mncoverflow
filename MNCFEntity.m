//
//  C3DTEntity.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTEntity.m,v 1.4 2003/07/07 09:38:37 pmanna Exp $
//
// $Log: C3DTEntity.m,v $
// Revision 1.4  2003/07/07 09:38:37  pmanna
// Added utility method for object constructor with geometry
//
// Revision 1.3  2003/06/29 13:55:13  pmanna
// Added handling of picking objects
//
// Revision 1.2  2003/06/21 19:23:26  pmanna
// Corrected with deletion of check for -animate
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "MNCFEntity.h"


@implementation MNCFEntity

_C3DTVector MNVectorTransform(const _C3DTVector v, const GLdouble* m);



+ (MNCFEntity *)entityWithStyle: (C3DTStyle *)aStyle
{
    return [[[MNCFEntity alloc] initWithStyle: aStyle] autorelease];
}

+ (MNCFEntity *)entityWithStyle: (C3DTStyle *)aStyle geometry: (C3DTGeometry *)aGeometry
{
    MNCFEntity	*result = [[[MNCFEntity alloc] initWithStyle: aStyle] autorelease];

    if (result != nil)
        [result setGeometry: aGeometry];

    return result;
}

- (id)initWithStyle: (C3DTStyle *)aStyle
{
    if ([super initWithStyle:aStyle] != nil) {

		virtualCoverNumber = ULONG_MAX;
		_hidden = NO;
		
	}
	
    return self;
}


- (void)useWith: (id)anObject
{

	if( !_hidden ) [super useWith:anObject];
}

-(void)setExposing:(BOOL)flag
{
	_exposing = flag;
}


-(BOOL)isExposing
{
	return _exposing;
}

-(BOOL)isSmooth
{
	return _smooth;
}

-(void)setSmooth:(BOOL)flag
{
	_smooth = flag;
}



-(void)setHidden:(BOOL)flag
{
	_hidden = flag;
}


-(BOOL)isHidden
{
	return _hidden;
}

-(void)setExposed:(BOOL)flag
{
	_exposed = flag;
}
-(BOOL)isExposed
{
	return _exposed;	
}

-(void)setHiding:(BOOL)flag
{
	_hiding = flag;
}

-(BOOL)isHiding
{
	return _hiding;
}

-(unsigned long)virtualCoverNumber
{
	return virtualCoverNumber;	
}


-(void)setVirtualCoverNumber:(unsigned long)num
{
	 virtualCoverNumber = num;	
}

#pragma mark MNC3DT

-(void)projectedPlainVerticesWithCamera:(C3DTCamera*)camera intoPoints:(NSPointPointer)points inView:(C3DTView*)view
//four points
{
	C3DTPlainSurface* aGeometry = [self geometry];

	
	if( aGeometry != nil && [aGeometry isKindOfClass: [C3DTPlainSurface class]] )
	{
		float depth = [aGeometry depth];
		float width = [aGeometry width];
		
		//camera _frustum
		
		_C3DTVector frustum = [camera frustum];
		NSSize viewSize = [view frame].size;
			
			
		_C3DTVector vertex1 = {-width/2, 0,+depth/2, 1.0};
		_C3DTVector vertex2 = {+width/2, 0,+depth/2, 1.0};
		_C3DTVector vertex3 = {+width/2, 0,-depth/2, 1.0};
		_C3DTVector vertex4 = {-width/2, 0,-depth/2, 1.0};

		
		glPushMatrix();

		if (_hasTransform) {
			
			// Translation
			if ((_translation.x != 0.0) || (_translation.y != 0.0) || (_translation.z != 0.0)) {
				glTranslatef(_translation.x, _translation.y, _translation.z);
			}
			
			// Scaling
			if ((_scaling.x != 1.0) || (_scaling.y != 1.0) || (_scaling.z != 1.0)) {
				glScalef(_scaling.x, _scaling.y, _scaling.z);
			}
			
			// Rotation
			glTranslatef(-_rotationCenter.x, -_rotationCenter.y, -_rotationCenter.z);
			
			if (_rotation.x != 0.0)
				glRotatef(_rotation.x, 1.0f, 0.0f, 0.0f);
			if (_rotation.y != 0.0)
				glRotatef(_rotation.y, 0.0f, 1.0f, 0.0f);
			if (_rotation.z != 0.0)
				glRotatef(_rotation.z, 0.0f, 0.0f, 1.0f);
			
			

			//glTranslatef(_rotationCenter.x, _rotationCenter.y, _rotationCenter.z);
		}
		
		GLdouble projmatrix[16];
		glGetDoublev(GL_PROJECTION_MATRIX,projmatrix);
		_C3DTVector tv1 = MNVectorTransform(vertex1, projmatrix);
		_C3DTVector tv2 = MNVectorTransform(vertex2, projmatrix);
		_C3DTVector tv3 = MNVectorTransform(vertex3, projmatrix);
		_C3DTVector tv4 = MNVectorTransform(vertex4, projmatrix);



		NSPoint point1 = 	[self convertToWindowCoords: tv1];
		NSPoint point2 = 	[self convertToWindowCoords: tv2];
		NSPoint point3 = 	[self convertToWindowCoords: tv3];
		NSPoint point4 = 	[self convertToWindowCoords: tv4];

		

		glPopMatrix();

		
		points[0] = point1;
		points[1] = point2;
		points[2] = point3;
		points[3] = point4;

	}else
	{
		points[0] = NSZeroPoint;
		points[1] = NSZeroPoint;
		points[2] = NSZeroPoint;
		points[3] = NSZeroPoint;
	}
		


	return points;
}

_C3DTVector MNVectorTransform(const _C3DTVector v, const GLdouble* m)
{
    _C3DTVector				r;
    
    r.x = v.x*m[0] + v.y*m[4] + v.z*m[8] + m[12];
    r.y = v.x*m[1] + v.y*m[5] + v.z*m[9] + m[13];
    r.z = v.x*m[2] + v.y*m[6] + v.z*m[10] + m[14];
	
    return r;
}

- (NSPoint)convertToWindowCoords:(_C3DTVector)objVector
{
	GLint viewport[4];
	GLdouble mvmatrix[16], projmatrix[16];
	GLdouble wx, wy, wz;
	
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, projmatrix);
	
	gluProject((GLdouble)objVector.x, (GLdouble)objVector.y, objVector.z, mvmatrix, projmatrix, viewport, &wx, &wy, &wz);
	
	return NSMakePoint(wx, wy);
	
}



@end
