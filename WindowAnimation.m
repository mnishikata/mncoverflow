//
//  WindowAnimation.m
//  Docs
//
//  Created by Masatoshi Nishikata on 07/12/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "WindowAnimation.h"


@implementation WindowAnimation

-(void)setupAnimation:(NSRect)startWindowFrame end:(NSRect)endWindowFrame  target:(NSWindow*)window 
{
	scaleFactor = startWindowFrame .size.width / endWindowFrame.size.width;
	s_frame = startWindowFrame;
	e_frame = endWindowFrame;
	
	 [fullWindow release];
	 fullWindow = [window retain];

	 scaleDown = ( s_frame.size.width > e_frame.size.width )? YES:NO;
	 
	 
	 
	 //MNLog(@"setup %@ %@ %d",NSStringFromRect(s_frame), NSStringFromRect(e_frame), scaleDown);
}

- (void)startAnimation
{
	[self setCurrentProgress:0];
	[super startAnimation];
}


- (void)setCurrentProgress:(NSAnimationProgress)progress

{
    NSDisableScreenUpdates();

	if( ![fullWindow isVisible] ) {
		
		[fullWindow makeKeyAndOrderFront: nil];
	}
	
	[fullWindow reset];
	
	// move origin
	NSPoint origin = NSMakePoint( (NSMidX(s_frame) + (NSMidX(e_frame) - NSMidX(s_frame))*progress) - [fullWindow frame].size.width/2,
								  (NSMidY(s_frame) + (NSMidY(e_frame) - NSMidY(s_frame))*progress) -[fullWindow frame].size.height/2);
	
	[fullWindow setFrameOrigin:origin];
	

	double scale;
	
	if( scaleDown )
		scale = (1/scaleFactor-1)*progress +  1;

	else
		scale = (1-scaleFactor) * progress + scaleFactor;
	
	
	//

	
	[fullWindow scaleX: scale
					 Y: scale ];
	

	
	NSEnableScreenUpdates();

	[fullWindow displayIfNeeded];
		


	
}
@end
