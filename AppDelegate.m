#import "AppDelegate.h"


extern double DURATION1 ;
extern double SIDE_SPACE_FACTOR ;
extern double SIDE_ANGLE ;
extern double Z_FRONT;


@implementation AppDelegate


-(double)DURATION1
{
	return DURATION1;
}

-(double)SIDE_SPACE_FACTOR
{
	return SIDE_SPACE_FACTOR;
}

-(double)SIDE_ANGLE
{
	return SIDE_ANGLE;
}

-(double)Z_FRONT
{
	return Z_FRONT;
}

-(void)setDURATION1:(double)v
{
	DURATION1 = v;
}

-(void)setSIDE_SPACE_FACTOR:(double)v
{
	 SIDE_SPACE_FACTOR = v;
}

-(double)SIDE_ANGLE:(double)v
{
	SIDE_ANGLE = v;
}

-(double)Z_FRONT:(double)v
{
	Z_FRONT = v;
}




-(void)awakeFromNib
{
	[NSThread detachNewThreadSelector:@selector(iconThread:) toTarget:self withObject:nil];	

	unknownImage = [[NSImage imageNamed:@"unknown.tiff"] retain];
	//FlowView* newFlowView =[[FlowView alloc] initWithFrame:[view frame]];
	//[[window contentView] addSubview:newFlowView];
	
	//[newFlowView setup];
	animation = [[AngleAnimation alloc] init];
}

- (void)willChangeValueForKey:(NSString *)key
{
	[super willChangeValueForKey:key];
	
	if( [key isEqualToString:@"Ns"] )
	{
		Ns_previous = Ns;
	}
}

-(void)updateView
{
	[window display];
}

-(NSArray*)nodes
{
	return nodes;	
}
-(double)Ns_previous
{
	return Ns_previous;	
}

-(double)Ns
{
	return Ns;	
}

-(int)R
{
	return R;	
}

-(double)Ncenter
{
	return Ncenter;	
}

- (void)animationDidEnd:(AngleAnimation*)animation
{
	Ncenter += [animation Nprogress];

}

- (void)animationDidStop:(AngleAnimation*)animation
{
	Ncenter += [animation Nprogress];

}


- (void)didChangeValueForKey:(NSString *)key
{
	[super didChangeValueForKey:key];
	//NSLog(@"set %@",key);
	
	if( [key isEqualToString:@"Ns"] )
	{

		if( [animation isAnimating] )
			[animation stopAnimation];
		else
		{
			NSRect rect = [view bounds];
			ConvertToLinear_ResetPiPosition(0, rect.size.width);	
		}
		


		[animation setDuration: DURATION1];
		[animation setAnimationCurve:NSAnimationLinear];
		[animation setDelegate:self];
		[animation setAnimationBlockingMode:NSAnimationNonblocking ];
		
		
		[animation startAnimation];
			

			
		

		
	}else
	{
		
		[nodes release];
		nodes = [[NSMutableArray array] retain];
		
		int hoge;
		double delta_angle = -2*pi/R;
		double currentAngle = pi;
		
		for( hoge = 0; hoge<R; hoge++ )
		{
			MNNodeObject *node = [[[MNNodeObject alloc] initWithStyle:nil] autorelease];
			
			[node setNmax: Nmax_plus_1-1];
			[node setR: R];

			CoverCoordinate c;
			c.angle = currentAngle;
			c.r = 0.0;
				
			[node setValue:[NSNumber numberWithInt:hoge] forKey:@"n"];
			[node addCoordinate: c ];

			currentAngle += delta_angle;
			
			[nodes addObject: node];	
		}
		
		Ns = 0;
		Ns_previous = 0;
		Ncenter = 0;
		
		[window display];
	
	}
}

///
-(void)	drawRoundView:(NSRect)rect
{
	NSRect square;
	if( rect.size.width > rect.size.height )
	square = NSMakeRect(0,0, rect.size.height*0.8, rect.size.height*0.8);
	else
		square = NSMakeRect(0,0, rect.size.width*0.8, rect.size.width*0.8);

	
	[[NSColor blackColor] set];
	
	NSBezierPath *path = [NSBezierPath bezierPath];
	[path appendBezierPathWithOvalInRect: square];
	
	[path stroke];
	
	[nodes makeObjectsPerformSelector:@selector(drawRound:) withObject:[NSValue valueWithRect:square]];
}


/*
	//NSLog(@"drawLinearView");
	
	double time = 0;
	NSTimeInterval currentDate = [[NSDate date] timeIntervalSinceReferenceDate];
	
	
	
	// ANIMATION
	while( [[NSDate date] timeIntervalSinceReferenceDate] < ( currentDate+DURATION1 ) )
	{
		
		int hoge ;
		for( hoge = 0; hoge < R; hoge++ )
		{
			MNNodeObject* node = [nodes objectAtIndex:hoge];
			double linearX = ConvertToLinear( time,  [node coordinate],  nodes,  Ns,  Ns_previous,  0, NSMaxX(rect) );
			
			
			
			NSString* string = [NSString stringWithFormat:@"%d",hoge];
			
			[string drawAtPoint:NSMakePoint(linearX, NSMidY(rect)) withAttributes:nil];
			
		}
		
		time = [[NSDate date] timeIntervalSinceReferenceDate] - currentDate;
		//[window display];
		
	}
	
	
	//NSLog(@"drawLinearView e"); 
 */

#pragma mark Datasource

-(unsigned)numberOfCovers
{
	[paths release];
	paths = [[NSFileManager defaultManager] directoryContentsAtPath:@"/Users/Masa/Desktop/"];
	[paths retain];
	

	return [paths count];
} 
 
-(NSImage*)imageForCoverAtIndexImmedeately:(unsigned)index
{
	NSImage *iconImage =  [[NSWorkspace sharedWorkspace] iconForFile:  [@"/Users/Masa/Desktop/" stringByAppendingPathComponent: [paths objectAtIndex: index] ]];

	
	
	return iconImage;
}



-(NSImage*)imageForCoverAtIndex:(unsigned)index
{
	[lock lock];
	[iconRequest addObject:[NSNumber numberWithInt:index]];
	[lock unlock];
	


	return nil;
}

-(NSString*)titleForCoverAtIndex:(unsigned)index
{
	return [paths objectAtIndex: index];
}

-(NSString*)subtitleForCoverAtIndex:(unsigned)index
{
	return [@"/Users/Masa/Desktop/" stringByAppendingPathComponent: [paths objectAtIndex: index] ];
}

-(void)flowView:(FlowView*)aFlowView didSelectCoverAtIndex:(unsigned)index
{
}
#pragma mark
-(void)insertCover:(id)sender
{
	
	[view reloadDataWithInsertionEffect];
 
}


-(void)iconThread:(id)obj
{
	
	iconRequest = [[NSMutableArray alloc] init];
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	lock = [[NSLock alloc] init];
	[NSThread setThreadPriority:0.1];
	
	while(1)
	{
		int count;
		[lock lock];
		count = [iconRequest count];
		[lock unlock];
		
		if( [iconRequest count] > 0 )
		{
			
			
			unsigned index;
			
			[lock lock];{
				index = [[iconRequest objectAtIndex:0] intValue];
			}[lock unlock];
			
			//NSLog(@"[iconRequest count] %d, idx %d",[iconRequest count], index);
			
			if( [view needsCoverAtIndex:index] )
			{
				//				[qlDatabase beginExclusiveTransaction];
				
				
				NSImage *iconImage =  [[NSWorkspace sharedWorkspace] iconForFile:  [@"/Users/Masa/Desktop/" stringByAppendingPathComponent: [paths objectAtIndex: index] ]];

				
				//[self performSelectorOnMainThread:@selector(pushImage:) withObject:info waitUntilDone:NO];
				[view pushImage:iconImage forCoverAtIndex:index];

				
			}else
			{
				NSLog(@"dont need");	
			}
			[lock lock];
			[iconRequest removeObject: [iconRequest objectAtIndex:0] ];
			
			if( [iconRequest count] == 0 )
			{
				[pool release];
				pool = [[NSAutoreleasePool alloc] init];
				
			}
			[lock unlock];
			
		}else
		{
			usleep(10000);
		}
		
		
		
		
		usleep(10000);
		
	}
	
	[pool release];
	
	
}

#pragma mark Template

-(void)template:(id)sender
{
	double template[2] = {0,1};
	
	//sqrt(1- pow(time-1, 2));
	NSMutableString* str = [[[NSMutableString alloc] init] autorelease];
	int hoge = 0;
	double time;
	for(time = 0; time <= 1.01; time +=0.01)
	{
		
		//NSString* str2 = [NSString stringWithFormat:@"%f, ",  
		//	( sqrt(1- pow(time-1, 2)) + sin(time*pi/2) )/2 ];
		NSString* str2 = [NSString stringWithFormat:@"%f, ",  
			 sqrt(1- pow(time-1, 2))   ];

		[str appendString: str2];
		//NSLog(@"Time %f, Result %f", time, sqrt(1- pow(time-1, 2)));
			hoge++;
	}
	
	NSLog(@"\n\static double template[101] = {%@  1.0};",str);
	
}

@end
