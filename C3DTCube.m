//
//  C3DTCube.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTCube.m,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTCube.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTCube.h"

@implementation C3DTCube

+ (C3DTCube *)cubeWithEdgeSize: (float)aSize
{
    return [[[C3DTCube alloc] initWithEdgeSize: aSize] autorelease];
}

- (id)initWithEdgeSize: (float)aSize
{
    return (self = [super initWithWidth: aSize
                                 height: aSize
                                  depth: aSize]);
}

- (float)size
{
    return _depth;
}

- (void)setSize: (float)aSize
{
    _width	= aSize;
    _height	= aSize;
    
    [super setDepth: aSize];
}

@end
