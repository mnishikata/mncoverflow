//
//  AngleAnimation.m
//  MNCoverFlow
//
//  Created by Masatoshi Nishikata on 08/03/01.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "AngleAnimation.h"
#import "FlowView.h"
#import "MNNodeObject.h"

extern double DURATION1;
extern double DURATIOND;
extern double DURATION0;


@implementation AngleAnimation

- (id)initWithDuration:(NSTimeInterval)duration animationCurve:(NSAnimationCurve)animationCurve
{
	//NSLog(@"init");

	self = [super initWithDuration:(NSTimeInterval)duration animationCurve:(NSAnimationCurve)animationCurve];
	if (self != nil) {

	}
	return self;
}






-(double)Nprogress
{
	return Nprogress;
}

-(double)currentProgress
{
	return 	currentProgress;
}
-(void)setCurrentProgress:(NSAnimationProgress)progress
{
	currentProgress= progress;
	
	double duration = [self duration];
	// progress = RegularizedIntegral( progress,  0.2,  duration,  duration-0.5);

		
		
	Nprogress = 0;
	double endN = [(FlowView*)[self delegate] Ns];
	double startN = [(FlowView*)[self delegate] Ncenter];
	int R = [(FlowView*)[self delegate] R];
	
	double time = progress * duration;
	
	NSArray* nodes = [(FlowView*)[self delegate] nodes];
	
	

	double modifiedProgress = RegularizedIntegral( time,  DURATION0, duration,  duration-DURATIOND);


	
	CoverCoordinate c = Angle_Animation( modifiedProgress,  startN,  endN, R , &Nprogress);

	//NSLog(@"progress %f, N %f",modifiedProgress, startN+Nprogress);

	CoverCoordinate cdif = SubtractCoordinates(c, cprime);

		
	
	int hoge;
	for( hoge = 0; hoge < R; hoge++ )
	{
		
		MNNodeObject *node = [nodes objectAtIndex:hoge];
		
		// angle
		[node addCoordinate: cdif ];

		//[node addCoordinate: c inAnimation:YES];

		// rotation and z
		[node animate:time];
		
		
	}
	
	cprime = c;
	
	[(FlowView*)[self delegate] updateView];

	//[super setCurrentProgress:progress]; コレを呼ぶとうごきがおかしくなる
}

/*
-(void)stopAnimation
{
	stopFlag = YES;
}
*/

-(void)startAnimation
{
	currentProgress = 0;
	
	NSArray* nodes = [(FlowView*)[self delegate] nodes];
	
	double Ns;
	
	Ns = [[self delegate] Ns];

	//adjust duration
	double endN = [(FlowView*)[self delegate] Ns];
	double startN = [(FlowView*)[self delegate] Ncenter];
	
	int hoge;
	for( hoge = 0; hoge < [nodes count]; hoge++ )
	{
		
		MNNodeObject *node = [nodes objectAtIndex:hoge];
		[node setAnimation:endN previousNs:startN];
		
	}
	
//NSLog(@"starting animation %f -> %f", startN, Ns);
	
	


	double originalDuration = [self duration];
	double sa = abs(endN - startN )  ;
	if( sa  > 100 )
	{
		[self setDuration: originalDuration *2 ];
	}else if( sa > 50 )
	{
		[self setDuration: originalDuration *(sa/50) ];
		
	}
	
	
		
		
	int R = [(FlowView*)[self delegate] R];

	cprime = Angle_Animation( 0,  startN,  endN, R , &Nprogress);

	[super startAnimation];
	stopFlag = NO;
	
	/*
	if( [[self delegate] animationShouldStart:self] )
		[NSThread detachNewThreadSelector:@selector(animationLoop) toTarget:self withObject:nil];
*/
	
}

#pragma mark Unused 
-(void)animationLoop
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	[NSThread setThreadPriority:0.1];
	
	double duration = [self duration];
	NSTimeInterval start = [[NSDate date] timeIntervalSinceReferenceDate];
	BOOL success = NO;
	
	while( !stopFlag )	
	{
		NSTimeInterval current = [[NSDate date] timeIntervalSinceReferenceDate];
		if( current > start + duration ) {
			[self setCurrentProgress:1.0];
			success = YES;
			break;
		}
		
		[self performSelectorOnMainThread:@selector(setCurrentProgress:) withObject:[NSNumber numberWithDouble:(current - start)/ duration] waitUntilDone:YES ];
		
		
	}
	
	if( success )
		[[self delegate] performSelectorOnMainThread:@selector(animationDidEnd:) withObject:self waitUntilDone:YES ];
	
	else
		[[self delegate] performSelectorOnMainThread:@selector(animationDidStop:) withObject:self waitUntilDone:YES ];

	[pool release];

}

-(void)__startAnimation
{
	NSArray* nodes = [(FlowView*)[self delegate] nodes];
	[nodes makeObjectsPerformSelector:@selector(storeAngle)];
	
	double Ns, Ns_previous;
	
	Ns = [[self delegate] Ns];
	Ns_previous = [[self delegate] Ns_previous];
	
	int hoge;
	for( hoge = 0; hoge < [nodes count]; hoge++ )
	{
		
		MNNodeObject *node = [nodes objectAtIndex:hoge];
		[node setAnimation:Ns previousNs:Ns_previous];
		
	}
	
	
	//adjust duration
	double endN = [(FlowView*)[self delegate] Ns];
	double startN = [(FlowView*)[self delegate] Ncenter];
	
	double duration = [self duration];
	double sa = abs(endN - startN )  ;
	
	if( sa  > 100 )
	{
		duration=  duration *2 ;
	}else if( sa > 50 )
	{
		duration =  duration *(sa/50) ;
		
	}
	
	
	///////////////
	
	
	// Blocking animation start
	


	int R = [(FlowView*)[self delegate] R];
	
	
	//NSArray* nodes = [(FlowView*)[self delegate] nodes];
	
	
	
	
	NSDate* startDate = [NSDate date];
	
	while( [[NSDate date] timeIntervalSinceDate: startDate] <= duration ) 
	{
		endN = [(FlowView*)[self delegate] Ns];
		startN = [(FlowView*)[self delegate] Ncenter];
		
		
		
		Nprogress = 0;
		
		[nodes makeObjectsPerformSelector:@selector(restoreAngle)];

		double time = [[NSDate date] timeIntervalSinceDate: startDate];
		double progress = RegularizedIntegral( time,  DURATION0, duration,  duration-DURATIOND);

		//NSLog(@"progress  %f",progress);
		CoverCoordinate c = Angle_Animation( progress,  startN,  endN, R , &Nprogress);
		
		int hoge;
		for( hoge = 0; hoge < R; hoge++ )
		{
			
			MNNodeObject *node = [nodes objectAtIndex:hoge];
			
			// angle
			
			[node addCoordinate: c ];
			
			// rotation and z
			[node animate:time];
			
			
		}
		
		
		
		[(FlowView*)[self delegate] updateView];
	}
	

	
	
	
}
@end
