#import "ScrollerView.h"

#define WIDTH 200
#define BUTTON_WIDTH 28
#define BUTTON_OVERWRAP 10

#define THICKNESS 18

static NSImage* _knobHLImage = nil;
static NSImage* _knobHMImage = nil;
static NSImage* _knobHRImage = nil;
static NSImage* _knobSlotImage = nil;

static NSRect   _knobHLRect = {{0, 0}, {0, 0}};
static NSRect   _knobHMRect = {{0, 0}, {0, 0}};
static NSRect   _knobHRRect = {{0, 0}, {0, 0}};
static NSRect   _knobSlotRect = {{0, 0}, {0, 0}};

static Boolean isVisible = true;
@implementation ScrollerView

- (id)initWithFrame:(NSRect)frameRect
{
	if ((self = [super initWithFrame:frameRect]) != nil) {
		// Add initialization code here
	//NSLog(@"init");
		
		[self setup];
	
	}
	return self;
}
-(void)awakeFromNib
{
	//NSLog(@"awakeFromNib");
	
	[self setup];
}
-(void)button:(id)sender
{
	if( [delegate respondsToSelector:@selector(scrollView:buttonClicked:)] )
	[delegate scrollView:self buttonClicked:sender];
	
//NSLog(@"button -1, 1, 10, 11");
}
-(void)setup
{
	[self setAutoresizingMask: NSViewWidthSizable | NSViewMaxYMargin];

	
	NSRect 	frame = [self frame];
	
	_scroller = [[MyScroller alloc] initWithFrame:NSMakeRect(80+BUTTON_WIDTH-BUTTON_OVERWRAP,(THICKNESS-16)/2, frame.size.width - (80+BUTTON_WIDTH-BUTTON_OVERWRAP)*2, 16)];
	[_scroller setTarget:self];
	[_scroller setAction:@selector(scrolled:)];	
	[_scroller setArrowsPosition:NSScrollerArrowsNone ];
	[_scroller setAutoresizingMask: NSViewWidthSizable | NSViewMaxYMargin];
	[_scroller setup];
	
	 lb = [[NSButton alloc] initWithFrame: NSMakeRect(80,0, BUTTON_WIDTH,THICKNESS)];
	 rb = [[NSButton alloc] initWithFrame: NSMakeRect(frame.size.width-80 - BUTTON_WIDTH,0, BUTTON_WIDTH,THICKNESS)];
	 
	NSBundle* bundle = [NSBundle bundleForClass:[self class]];
	 
	NSImage* rbimage  = [[NSImage alloc] initWithContentsOfFile:
		 [bundle pathForImageResource:@"scrollerButtonR"]];
	NSImage* lbimage  = [[NSImage alloc] initWithContentsOfFile:
		[bundle pathForImageResource:@"scrollerButton"]];

	 
	[rb setAutoresizingMask: NSViewMinXMargin | NSViewMaxYMargin];
	[rb setAction:@selector(button:)];
	[rb setTarget:self];
	[rb setImage:rbimage];

	[rb setButtonType:NSMomentaryChangeButton ];
	[rb setBordered:NO];
	[rb setTag: +1];
	
	[lb setAutoresizingMask: NSViewMaxXMargin | NSViewMaxYMargin];
	[lb setAction:@selector(button:)];
	[lb setTarget:self];
	[lb setImage:lbimage];
	[lb setButtonType:NSMomentaryChangeButton ];

	[lb setBordered:NO];
	[lb setTag: -1];

	[self addSubview:lb];
	[self addSubview:rb];
	[self addSubview:_scroller];

	
	[_scroller setEnabled:YES];
	
	//[_scroller setFloatValue:0.1 knobProportion:0.3];
	
	
	
	//fullscreen
	NSImage* bimg  = [[NSImage alloc] initWithContentsOfFile:
		[bundle pathForImageResource:@"fullscreen"]];
	
	fullscreenButton = [[NSButton alloc] initWithFrame: NSMakeRect( frame.size.width-50, 7, [bimg size].width,[bimg size].height)];
	
	[fullscreenButton setAutoresizingMask: NSViewMinXMargin | NSViewMaxYMargin];
	[fullscreenButton setAction:@selector(button:)];
	[fullscreenButton setTarget:self];
	[fullscreenButton setImage:bimg];
	[fullscreenButton setBordered:NO];
	[fullscreenButton setTag:10];
	[fullscreenButton setButtonType:NSMomentaryChangeButton ];
	
	[self addSubview:fullscreenButton];
	
}

- (void)setFloatValue:(float)aFloat knobProportion:(float)knobProp{
	[_scroller setFloatValue:aFloat knobProportion:knobProp];
	if( knobProp > 0.4 ) {
		isVisible = false;
		[rb setHidden:YES];
		[lb setHidden:YES];

	}
	else {
		isVisible = true;
		[rb setHidden:NO];
		[lb setHidden:NO];

	}
	
	[delegate setNeedsCleaning:YES];
	[[self window] display];
}
-(float)floatValue
{
	return [_scroller floatValue];
}


-(id)verticalScroller
{
	return nil;
	
}
-(id)horizontalScroller
{
	return _scroller;
	
}

-(void)drawRect:(NSRect)rect
{
	if( isVisible )
	{
	NSRect slotRect = [_scroller frame];
	slotRect.origin.x  += BUTTON_OVERWRAP;
	slotRect.size.height = THICKNESS;
	slotRect.size.width  -=BUTTON_OVERWRAP*2;
	slotRect.origin.y -= (THICKNESS-16)/2;

	[_knobSlotImage drawInRect:slotRect fromRect:_knobSlotRect operation:NSCompositeSourceOver fraction:1.0f];
	}else
	{
		//NSColor* color = [NSColor colorWithCalibratedWhite:0.0 alpha:0.0];
		//[color set];
		//NSRectFill(rect);
	}
}

-(BOOL)hasHorizontalScroller{
	return YES;
}
-(BOOL)hasVerticalScroller{
	return NO;
}
- (void) scrolled: (NSScroller *)scroller
{
	if( [delegate respondsToSelector:@selector(scrollView:hit:)] )
		[delegate scrollView:self hit:[scroller hitPart]];

//NSLog(@"scrolled %d",[scroller hitPart]);
//NSLog(@"float value %f",[_scroller floatValue]);

    float knobPosition;
	
    switch ([scroller hitPart]) {
        case NSScrollerIncrementLine:
			// Include code here for the case where the down arrow is pressed
            break;
        case NSScrollerIncrementPage:
			// Include code here for the case where CTRL + down arrow is pressed, or the space the scroll knob moves in is pressed
            break;
        case NSScrollerDecrementLine:
			// Include code here for the case where the up arrow is pressed
            break;
        case NSScrollerDecrementPage:
			// Include code here for the case where CTRL + up arrow is pressed, or the space the scroll knob moves in is pressed
            break;
        case NSScrollerKnob:
			// This case is when the knob itself is pressed
            knobPosition = [scroller floatValue];
            // Do something with the view
        default:
            break;
    }
}

-(void)setDelegate:(id)obj
{
	[delegate release];
	delegate = obj;
	[delegate retain];
}

-(void)dealloc
{
	[delegate release];
	[super dealloc];
}
-(void)setSmallSize:(BOOL)flag
{
	
	if( flag )
	{
		NSRect 	frame = [self frame];
		
		[lb setFrameOrigin: NSMakePoint(10,0)];
		[rb setFrameOrigin: NSMakePoint(frame.size.width-40 - BUTTON_WIDTH,0)];
		[_scroller setFrame:NSMakeRect(10+BUTTON_WIDTH-BUTTON_OVERWRAP,(THICKNESS-16)/2, frame.size.width - (BUTTON_WIDTH-BUTTON_OVERWRAP)*2-40-10, 16) ];
		
		
		[fullscreenButton setFrameOrigin: NSMakePoint( frame.size.width-40, 0)];

		
	}else
	{
		NSRect 	frame = [self frame];
		
		[lb setFrameOrigin: NSMakePoint(80,0)];
		[rb setFrameOrigin: NSMakePoint(frame.size.width-80 - BUTTON_WIDTH,0)];
		[_scroller setFrame:NSMakeRect(80+BUTTON_WIDTH-BUTTON_OVERWRAP,(THICKNESS-16)/2, frame.size.width - (80+BUTTON_WIDTH-BUTTON_OVERWRAP)*2, 16) ];
		
		
		[fullscreenButton setFrameOrigin: NSMakePoint( frame.size.width-50, 7)];
		
	}

	
	
}
@end


@implementation MyScroller

-(BOOL)isOpaque
{
	return NO;
}
-(void)setup
{
	NSBundle*   bundle;
	bundle = [NSBundle bundleForClass:[self class]];

	
	_knobHLImage = [[NSImage alloc] initWithContentsOfFile:
		[bundle pathForImageResource:@"scrollerHL"]];
	_knobHMImage = [[NSImage alloc] initWithContentsOfFile:
		[bundle pathForImageResource:@"scrollerHM"]];
	_knobHRImage = [[NSImage alloc] initWithContentsOfFile:
		[bundle pathForImageResource:@"scrollerHR"]];

	_knobSlotImage = [[NSImage alloc] initWithContentsOfFile:
		[bundle pathForImageResource:@"scrollerSlot"]];
	
	_knobHLRect.size = [_knobHLImage size];
	_knobHMRect.size = [_knobHMImage size];
	_knobHRRect.size = [_knobHRImage size];
	
	_knobSlotRect.size = [_knobSlotImage size];
}
-(void)drawRect:(NSRect)rect
{
	
if( isVisible )
{
	
	NSRect knobRect = [self rectForPart:NSScrollerKnob];
//NSLog(@"knobRect %@",NSStringFromRect(knobRect));
	[self drawKnob:knobRect];
}
	
}

- (void)drawKnob:(NSRect)rect
{
    // Check flip
    
    NSRect  imageRect;
 
    
    // Draw knob right
    imageRect.origin.y = rect.origin.y ;
    imageRect.origin.x = rect.origin.x + rect.size.width - _knobHRRect.size.width  ;
    imageRect.size.width  = _knobHRRect.size.width;
	imageRect.size.height = rect.size.height;
	

	 [_knobHRImage setFlipped:YES];
    [_knobHRImage drawInRect:imageRect fromRect:_knobHRRect operation:NSCompositeSourceOver fraction:1.0f];
    
    // Draw knob middle
    imageRect.origin.y = rect.origin.y;
    imageRect.origin.x = rect.origin.x + _knobHLRect.size.width;
	
	imageRect.size.height = rect.size.height;
    imageRect.size.width  = rect.size.width - _knobHLRect.size.width - _knobHRRect.size.width - 0;

	[_knobHMImage setFlipped:YES];
    [_knobHMImage drawInRect:imageRect fromRect:_knobHMRect operation:NSCompositeSourceOver fraction:1.0f];
    
    // Draw knob left
    imageRect.origin.y = rect.origin.y ;
    imageRect.origin.x = rect.origin.x ;
	imageRect.size.width = _knobHLRect.size.width;
	imageRect.size.height = rect.size.height;
	
	[_knobHLImage setFlipped:YES];
    [_knobHLImage drawInRect:imageRect fromRect:_knobHLRect operation:NSCompositeSourceOver fraction:1.0f];
}

- (void)drawKnobSlotInRect:(NSRect)rect 
{
	[_knobSlotImage drawInRect:rect fromRect:_knobSlotRect operation:NSCompositeSourceOver fraction:1.0f];

}
@end
