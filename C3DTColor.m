//
//  C3DTColor.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTColor.m,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTColor.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTColor.h"
#import <OpenGL/glu.h>

// $$$ To do: verify the ranges of color & alpha

@implementation C3DTColor

+ (C3DTColor *)defaultColor
{
    return [[[C3DTColor alloc] initWithColorRed: 1.0 green: 1.0 blue: 1.0 alpha: 1.0] autorelease];
}

+ (C3DTColor *)colorWithRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    return [[[C3DTColor alloc] initWithColorRed: redColor
                                          green: greenColor
                                           blue: blueColor
                                          alpha: alphaValue] autorelease];
}

-(id)initWithColorRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    if ((self = [super init]) != nil) {
        [self setColorRed: redColor green: greenColor blue: blueColor alpha: alphaValue];
    }

    return self;
}

- (void)apply
{
    glGetFloatv(GL_CURRENT_COLOR, (GLfloat *)&_savedColor);
    glColor4fv((GLfloat *)&_color);
}

- (void)restore
{
    glColor4fv((GLfloat *)&_savedColor);
}

- (_C3DTVector)color;
{
    return _color;
}

- (void)setColor: (_C3DTVector)aColor
{
    _color = aColor;
}

- (void)setColorRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _color.x = redColor;
    _color.y = greenColor;
    _color.z = blueColor;
    _color.w = alphaValue;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_color length: sizeof(_C3DTVector) forKey: @"ColorColor"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_color length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        _color	= *(_C3DTVector *)[coder decodeBytesForKey: @"ColorColor" returnedLength: &len];
    } else {
        _color	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }

    return self;
}

@end
