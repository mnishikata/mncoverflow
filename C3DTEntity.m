//
//  C3DTEntity.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTEntity.m,v 1.4 2003/07/07 09:38:37 pmanna Exp $
//
// $Log: C3DTEntity.m,v $
// Revision 1.4  2003/07/07 09:38:37  pmanna
// Added utility method for object constructor with geometry
//
// Revision 1.3  2003/06/29 13:55:13  pmanna
// Added handling of picking objects
//
// Revision 1.2  2003/06/21 19:23:26  pmanna
// Corrected with deletion of check for -animate
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTEntity.h"
#import "C3DTCamera.h"
#import "C3DTPlainSurface.h"
#import "C3DTView.h"


@implementation C3DTEntity


+ (C3DTEntity *)entityWithStyle: (C3DTStyle *)aStyle
{
    return [[[C3DTEntity alloc] initWithStyle: aStyle] autorelease];
}

+ (C3DTEntity *)entityWithStyle: (C3DTStyle *)aStyle geometry: (C3DTGeometry *)aGeometry
{
    C3DTEntity	*result = [[[C3DTEntity alloc] initWithStyle: aStyle] autorelease];

    if (result != nil)
        [result setGeometry: aGeometry];

    return result;
}

- (id)initWithStyle: (C3DTStyle *)aStyle
{
    if ([super initWithStyle:aStyle] != nil) {
		_geometry= nil;
    
	}
	
    return self;
}


- (void)dealloc
{
    [_geometry release];

    [super dealloc];
}

// All these are overridden, as this is a leaf node
- (void)setChildren: (NSMutableArray *)newChildren
{
    // $$$ Generate appropriate error
}

- (void)addChild: (C3DTNode *)child
{
    // $$$ Generate appropriate error
}

- (void)insertAboveNode: (C3DTNode *)aNode
{
    // $$$ Generate appropriate error
}

- (void)useWith: (id)anObject
{
	
    C3DTCamera	*camera	= (C3DTCamera *)anObject;
    
    if (_hasTransform) {
        glPushMatrix();

        // Translation
        if ((_translation.x != 0.0) || (_translation.y != 0.0) || (_translation.z != 0.0)) {
            glTranslatef(_translation.x, _translation.y, _translation.z);
        }

        // Scaling
        if ((_scaling.x != 1.0) || (_scaling.y != 1.0) || (_scaling.z != 1.0)) {
            glScalef(_scaling.x, _scaling.y, _scaling.z);
        }
        
        // Rotation
        glTranslatef(-_rotationCenter.x, -_rotationCenter.y, -_rotationCenter.z);

        if (_rotation.x != 0.0)
            glRotatef(_rotation.x, 1.0f, 0.0f, 0.0f);
        if (_rotation.y != 0.0)
            glRotatef(_rotation.y, 0.0f, 1.0f, 0.0f);
        if (_rotation.z != 0.0)
            glRotatef(_rotation.z, 0.0f, 0.0f, 1.0f);

        glTranslatef(_rotationCenter.x, _rotationCenter.y, _rotationCenter.z);
    }

    if ([camera selectionMode]) {
        GLint	entityName;

        sscanf([[self name] cString], "Node-%ld", &entityName);
        glPushName(entityName);
    }
    else
        [_style apply];

    [_geometry draw];

    if ([camera selectionMode])
        glPopName();
    else {
        [_style restore];
        // Selected mode, redraw as wireframe
        if (0 /*[self selected]*/ ) {
            GLfloat		lineWidth;

            glDisable(GL_TEXTURE_2D);
            
            glGetFloatv(GL_LINE_WIDTH, &lineWidth);
            glLineWidth(3.0);
            glPolygonMode(GL_FRONT, GL_LINE);
            
            [_geometry draw];
            
            glPolygonMode(GL_FRONT, GL_FILL);
            glLineWidth(lineWidth);
            
            glEnable(GL_TEXTURE_2D);
        }
    }

    // C3DTEntity is a "leaf" node, i.e. it doesn't have any children,
    // therefore it doesn't use [super ...] to draw subtrees
    if (_hasTransform) {
        glPopMatrix();
    }


}

/*
 * The default tries to call "animate" on its geometry (for example, fountains)
 * A subclass could operate on itself, for example rotating its geometry
 */
- (void)animate
{
    [_geometry animate];
}

- (C3DTGeometry *)geometry
{
    return _geometry;
}

- (void)setGeometry: (C3DTGeometry *)aGeometry
{
    [aGeometry retain];
    [_geometry release];

    _geometry = aGeometry;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        [coder encodeObject: _geometry forKey: @"EntityGeometry"];
    } else {
        [coder encodeObject: _geometry];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        _geometry	= [[coder decodeObjectForKey: @"EntityGeometry"] retain];
    } else {
        _geometry	= [[coder decodeObject] retain];
    }

    return self;
}




@end
