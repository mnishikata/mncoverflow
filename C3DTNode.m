//
//  C3DTNode.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTNode.m,v 1.2 2003/06/29 13:52:50 pmanna Exp $
//
// $Log: C3DTNode.m,v $
// Revision 1.2  2003/06/29 13:52:50  pmanna
// Added recursive name search
//
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTNode.h"

static int		nodeCounterId = 0;

@implementation C3DTNode

/*
 * Overridden methods
 */

- (id)init
{
    return [self initWithName: [NSString stringWithFormat: @"Node-%ld", nodeCounterId++]];
}

- (void)dealloc
{
    [self removeAllChildren];

    [super dealloc];
}

/*
 * Accessor methods
 */

// Dynamically calculated, purpose is to limit certain operations up to a certain level
- (int)depth
{
    if (_parent == nil)
        return 0;
    else
        return ([_parent depth] + 1);
}

/*
 * Parent accessors
 */
- (C3DTNode *)parent
{
    return _parent;
}

- (void)setParent: (C3DTNode *)newParent
{
    // We don't retain it here, children are not responsible for the parent
    _parent = newParent;
}

/*
 * Children accessors
 */
- (NSArray *)children
{
    return _children;
}

- (void)setChildren: (NSMutableArray *)newChildren
{
    [newChildren retain];
    [self removeAllChildren];

    _children = newChildren;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        [coder encodeObject: _children forKey: @"NodeChildren"];
    } else {
        [coder encodeObject: _children];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        _children = [[coder decodeObjectForKey: @"NodeChildren"] retain];
    } else {
        _children = [[coder decodeObject] retain];
    }
    
    // Dynamically set parent
    NSEnumerator	*en;
    C3DTNode		*child;

    en = [_children objectEnumerator];
    while ((child = (C3DTNode *)[en nextObject]) != nil)
        [child setParent: self];

    return self;
}

/*
 * Handling of the tree structure
 */

// Finds the (first) node with a given name
- (C3DTObject *)findNamedObject: (NSString *)objName inArray: (NSArray *)array
{
    NSEnumerator	*en;
    C3DTObject		*obj;

    if ((array != nil) && (objName != nil)) {
        en = [array objectEnumerator];
        while ((obj = [en nextObject]) != nil)
            if ([[obj name] isEqualToString: objName]) {
                return obj;
            }
    }
        return nil;
}

// Adds a new node to the children array (and creates it if needed)
- (void)addChild: (C3DTNode *)child
{
    if (child == nil)
        return;

    if (_children == nil)
        if ((_children = [[NSMutableArray arrayWithCapacity: 1] retain]) == nil)
            // $$$ Generate the error here
            return;

    [_children addObject: child];
    [child setParent: self];
}

// Inserts a node above a given one
- (void)insertAboveNode: (C3DTNode *)aNode
{
    C3DTNode	*oldParent;

    if (aNode == nil)
        // $$$ Generate appropriate error
        return;
    if ((oldParent = [aNode parent]) == nil)
        // $$$ Scene node, cannot insert above, generate appropriate error
        return;

    [[oldParent children] removeObject: aNode];
    [[oldParent children] addObject: self];

    [self addChild: aNode];
    [self setParent: oldParent];
}

// Removes all the children recursively
- (void)removeAllChildren
{
    if (_children != nil) {
        NSEnumerator	*en = [_children objectEnumerator];
        C3DTNode		*child;

        while ((child = [en nextObject]) != nil)
            [child removeAllChildren];

        [_children removeAllObjects];

        [_children release];

        _children = nil;
    }
}

// Removes a subtree node, with all the children
- (void)removeChild: (C3DTNode *)child
{
    if ((_children != nil) && (child != nil)) {
        [child removeAllChildren];

        [_children removeObject: child];

        if ([_children count] == 0) {
            [_children release];

            _children = nil;
        }
    }
}

// Same as above, but with a named node
- (void)removeChildNamed: (NSString *)childName
{
    [self removeChild: (C3DTNode *)[self findNamedObject: childName inArray: [self children]]];
}

// Transfers a child subtree from this node to another: the subtree itself is not modified
- (void)transferChild: (C3DTNode *)child toNode: (C3DTNode *)newParent
{
    if ((_children != nil) && (child != nil)) {
        [newParent addChild: child];
        [_children removeObject: child];

        if ([_children count] == 0) {
            [_children release];

            _children = nil;
        }
    }
}

// Same as above, but with a named node
- (void)transferChildNamed: (NSString *)childName toNode: (C3DTNode *)newParent
{
    [self transferChild: (C3DTNode *)[self findNamedObject: childName inArray: [self children]] toNode: newParent];
}

// Removes a child node, but transfers the subtree to self
- (void)removeAndTransferChild: (C3DTNode *)child
{
    if ((_children != nil) && (child != nil)) {
        NSEnumerator	*en = [[child children] objectEnumerator];
        C3DTNode			*grandson;

        while ((grandson = [en nextObject]) != nil)
            [child transferChild: grandson toNode: self];

        [self removeChild: child];
    }
}

// Same as above, but with a named node
- (void)removeAndTransferChildNamed: (NSString *)childName
{
    [self removeAndTransferChild: (C3DTNode *)[self findNamedObject: childName inArray: [self children]]];
}

@end

@implementation C3DTNode(C3DTRecursiveOperations)

/*
 * This method is intended to be called to let the tree recalculate some internal data
 * The method receives an (untyped) object to work with
 * The base C3DTNode just calls "update" on all children
 */
- (void)updateWith: (id)anObject
{
    if (_children != nil) {
        NSEnumerator	*en;
        C3DTNode		*child;

        en = [_children objectEnumerator];
        while ((child = [en nextObject]) != nil)
            [child updateWith: anObject];
    }
}

/*
 * This method is intended to be called when the object must be "used"
 * This includes "drawing" for graphical objects, "applying" for textures & material
 * "transforming" for groups and so on, as other actions depend on the type of the node
 * The method receives an (untyped) object to work with
 * The base C3DTNode just calls "useWith:" on all children
 */
- (void)useWith: (id)anObject
{
    if (_children != nil) {
        NSEnumerator	*en;
        C3DTNode		*child;

        en = [_children objectEnumerator];
        while ((child = [en nextObject]) != nil)
            [child useWith: anObject];
    }
}

/*
 * This method is called when the object must go through an animation step
 * The base C3DTNode just calls "animate" on all children
 */
- (void)animate
{
    if (_children != nil) {
        NSEnumerator	*en;
        C3DTNode		*child;

        en = [_children objectEnumerator];
        while ((child = [en nextObject]) != nil)
            [child animate];
    }
}

- (C3DTObject *)findNamedObject: (NSString *)objName
{
    if (_children != nil) {
        NSEnumerator	*en;
        C3DTNode		*child;
        C3DTObject		*found;

        en = [_children objectEnumerator];
        while ((child = (C3DTNode *)[en nextObject]) != nil) {
            if ([[child name] isEqualToString: objName])
                return child;
            if ((found = [child findNamedObject: objName]) != nil)
                return found;
        }
    }

    return nil;
}

@end
