//
//  C3DTObject.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTObject.m,v 1.2 2003/06/21 19:22:35 pmanna Exp $
//
// $Log: C3DTObject.m,v $
// Revision 1.2  2003/06/21 19:22:35  pmanna
// Corrected with deletion of initWithCoder and encodeWithCoder to super
//
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTObject.h"


@implementation C3DTObject

- (id)initWithName: (NSString *)name
{
    if ((self = [super init]) != nil)
        _name = [name retain];

    return self;
}

- (void)dealloc
{
    [_name release];

    [super dealloc];
}

- (NSString *)name
{
    return _name;
}

- (void)setName: (NSString *)newName
{
    [newName retain];
    [_name release];
    _name = newName;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeObject: _name forKey: @"ObjName"];
    } else {
        [coder encodeObject: _name];
    }
    
    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    
    if ( [coder allowsKeyedCoding] ) {
        // Can decode keys in any order
        _name = [[coder decodeObjectForKey:@"ObjName"] retain];
    } else {
        // Must decode keys in same order as encodeWithCoder:
        _name = [[coder decodeObject] retain];
    }
    
    return self;
}

@end
