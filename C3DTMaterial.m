//
//  C3DTMaterial.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue May 20 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTMaterial.m,v 1.1.1.1 2003/06/10 18:09:24 pmanna Exp $
//
// $Log: C3DTMaterial.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTMaterial.h"

static	GLuint	materialIdCounter = 0;

@implementation C3DTMaterial

+ (id)material
{
    return [[[C3DTMaterial alloc] initWithName: [NSString stringWithFormat: @"Material-%ld", materialIdCounter++]] autorelease];
}

- (id)initWithName: (NSString *)aName
{
    if ((self = [super initWithName: aName]) != nil) {
        // Generates the standard values, according to the OpenGL Reference
        _ambient.x = _ambient.y = _ambient.z = 0.2;		_ambient.w = 1.0;
        _diffuse.x = _diffuse.y = _diffuse.z = 0.8;		_diffuse.w = 1.0;
        _specular.x = _specular.y = _specular.z = 0.0;	_specular.w = 1.0;
        _emission.x = _emission.y = _emission.z = 0.0;	_emission.w = 1.0;
    }

    return self;
}

- (void)apply
{
    glGetMaterialfv(GL_FRONT, GL_AMBIENT, (GLfloat *)&_savedAmbient);
    glGetMaterialfv(GL_FRONT, GL_DIFFUSE, (GLfloat *)&_savedDiffuse);
    glGetMaterialfv(GL_FRONT, GL_SPECULAR, (GLfloat *)&_savedSpecular);
    glGetMaterialfv(GL_FRONT, GL_EMISSION, (GLfloat *)&_savedEmission);
    
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (GLfloat *)&_ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (GLfloat *)&_diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (GLfloat *)&_specular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GLfloat *)&_emission);
}

- (void)restore
{
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (GLfloat *)&_savedAmbient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (GLfloat *)&_savedDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (GLfloat *)&_savedSpecular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GLfloat *)&_savedEmission);
}

- (_C3DTVector)ambient
{
    return _ambient;
}

- (void)setAmbient: (_C3DTVector)aColor
{
    _ambient = aColor;
}

- (void)setAmbientRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _ambient.x = redColor;
    _ambient.y = greenColor;
    _ambient.z = blueColor;
    _ambient.w = alphaValue;
}

- (_C3DTVector)diffuse
{
    return _diffuse;
}

- (void)setDiffuse: (_C3DTVector)aColor
{
    _diffuse = aColor;
}

- (void)setDiffuseRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _diffuse.x = redColor;
    _diffuse.y = greenColor;
    _diffuse.z = blueColor;
    _diffuse.w = alphaValue;
}

- (_C3DTVector)specular
{
    return _specular;
}

- (void)setSpecular: (_C3DTVector)aColor
{
    _specular = aColor;
}

- (void)setSpecularRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _specular.x = redColor;
    _specular.y = greenColor;
    _specular.z = blueColor;
    _specular.w = alphaValue;
}

- (_C3DTVector)emission
{
    return _emission;
}

- (void)setEmission: (_C3DTVector)aColor
{
    _emission = aColor;
}

- (void)setEmissionRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _emission.x = redColor;
    _emission.y = greenColor;
    _emission.z = blueColor;
    _emission.w = alphaValue;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_ambient length: sizeof(_C3DTVector) forKey: @"MaterialAmbient"];
        [coder encodeBytes: (const uint8_t *)&_diffuse length: sizeof(_C3DTVector) forKey: @"MaterialDiffuse"];
        [coder encodeBytes: (const uint8_t *)&_specular length: sizeof(_C3DTVector) forKey: @"MaterialSpecular"];
        [coder encodeBytes: (const uint8_t *)&_emission length: sizeof(_C3DTVector) forKey: @"MaterialEmission"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_ambient length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_diffuse length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_specular length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_emission length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        _ambient	= *(_C3DTVector *)[coder decodeBytesForKey: @"MaterialAmbient" returnedLength: &len];
        _diffuse	= *(_C3DTVector *)[coder decodeBytesForKey: @"MaterialDiffuse" returnedLength: &len];
        _specular	= *(_C3DTVector *)[coder decodeBytesForKey: @"MaterialSpecular" returnedLength: &len];
        _emission	= *(_C3DTVector *)[coder decodeBytesForKey: @"MaterialEmission" returnedLength: &len];
    } else {
        _ambient	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _diffuse	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _specular	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _emission	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }

    return self;
}

@end
