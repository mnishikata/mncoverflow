//
//  C3DTGroup.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTGroup.m,v 1.2 2003/06/29 13:53:35 pmanna Exp $
//
// $Log: C3DTGroup.m,v $
// Revision 1.2  2003/06/29 13:53:35  pmanna
// Added selection flag
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTGroup.h"


@implementation C3DTGroup

- (_C3DTBounds)bounds
{
    return _bounds;
}

- (void)setBounds: (_C3DTBounds) newBounds
{
    _bounds = newBounds;
}

- (BOOL)selected {
    return _selected;
}

- (void)setSelected: (BOOL)newSelected {
    _selected = newSelected;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeInt: _visibility forKey: @"GroupVisibility"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_bounds length: sizeof(_C3DTBounds) forKey: @"GroupBounds"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_bounds length: sizeof(_C3DTBounds)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;
    
    self = [super initWithCoder: coder];

    _visibility = [coder decodeIntForKey: @"GroupVisibility"];
    if ( [coder allowsKeyedCoding] ) {
        _bounds		= *(_C3DTBounds *)[coder decodeBytesForKey: @"GroupBounds" returnedLength: &len];
    } else {
        _bounds		= *(_C3DTBounds *)[coder decodeBytesWithReturnedLength: &len];
    }

    return self;
}

/*
 * Returns a number of spheres that approximates the bounding box of the node
 * First implementation returns just the largest sphere that contains it all
 * The pointer passed in MUST be already allocated!! This is to avoid responsability for the memory
 */
- (int)spheresForBounds: (_C3DTSpheroid **)spheres
{
    if (*spheres) {
        if (*spheres = (_C3DTSpheroid *)realloc(*spheres, sizeof(_C3DTSpheroid))) {
            **spheres = sphereFromBounds([self bounds]);

            return 1;
        }
    }
    return 0;
}


@end
