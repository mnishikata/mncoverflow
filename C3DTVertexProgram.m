//
//  C3DTVertexProgram.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue May 20 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTVertexProgram.m,v 1.1.1.1 2003/06/10 18:09:25 pmanna Exp $
//
// $Log: C3DTVertexProgram.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:25  pmanna
// Initial import
//
//

#import "C3DTVertexProgram.h"
#import "C3DTGeometry.h"
#import <Cocoa/Cocoa.h>

@interface C3DTVertexProgram (PrivateMethods)
- (BOOL)_loadProgram: (NSString *)programFile;
@end

@implementation C3DTVertexProgram

+ (C3DTVertexProgram *)programWithFile: (NSString *)aName
{
    return [[[C3DTVertexProgram alloc] initWithName: aName] autorelease];
}

- (id)initWithName: (NSString *)aName
{
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];

    if (aName == nil)
        return nil;

    if (self = [super initWithName: aName]) {

        if( [self _loadProgram: [resourcePath stringByAppendingFormat:@"/%@", aName ]])
        {
            glGenProgramsARB( 1, &_programId );   // Create the program ID

            glBindProgramARB( GL_VERTEX_PROGRAM_ARB, _programId );
            glProgramStringARB( GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB,
                                _programSize, [_programData cString] );
            
            // Reset the program, we want to be clean going out
            glBindProgramARB( GL_VERTEX_PROGRAM_ARB, 0 );
        }

        // Allocate memory for the parameters and their saved counterparts
        glGetIntegerv(GL_MAX_PROGRAM_LOCAL_PARAMETERS_ARB, &_maxLocalParams);
        _localParams = (_C3DTVector *)calloc(_maxLocalParams, sizeof(_C3DTVector));
        _savedLocalParams = (_C3DTVector *)calloc(_maxLocalParams, sizeof(_C3DTVector));
        
        glGetIntegerv(GL_MAX_PROGRAM_ENV_PARAMETERS_ARB, &_maxLocalParams);
        _envParams = (_C3DTVector *)calloc(_maxEnvParams, sizeof(_C3DTVector));
        _savedEnvParams = (_C3DTVector *)calloc(_maxEnvParams, sizeof(_C3DTVector));
    }

    return self;
}

- (void)dealloc
{
    if (_programSize) {
        glDeleteProgramsARB(1, &_programId);
        [_programData release];
    }

    if (_localParams)
        free(_localParams);
    if (_savedLocalParams)
        free(_savedLocalParams);
    
    if (_envParams)
        free(_envParams);
    if (_savedEnvParams)
        free(_savedEnvParams);
    
    [super dealloc];
}

- (void)apply
{
    GLuint		programToApply = (_programSize ? _programId : 0);
    int			ii;
    _C3DTVector	*param;
    _C3DTVector	*savedParam;
    
    glGetIntegerv(GL_PROGRAM_BINDING_ARB, (GLint *)&_savedProgramId);

    if (programToApply != _savedProgramId) {
        _programSaved = YES;
        glBindProgramARB( GL_VERTEX_PROGRAM_ARB, _programId );
        if (_programId)
            glEnable(GL_VERTEX_PROGRAM_ARB);
        else
            glDisable(GL_VERTEX_PROGRAM_ARB);

        // Save and set local params
        for (ii = 0, param = _localParams, savedParam = _savedLocalParams;
             ii < _numLocalParams;
             ii++, param++, savedParam++) {
            glGetProgramLocalParameterfvARB(GL_VERTEX_PROGRAM_ARB, ii, (GLfloat *)savedParam);
            glProgramLocalParameter4fvARB(GL_VERTEX_PROGRAM_ARB, ii, (GLfloat *)param);
        }

        // Save and set environmental params
        for (ii = 0, param = _envParams, savedParam = _savedEnvParams;
             ii < _numEnvParams;
             ii++, param++, savedParam++) {
            glGetProgramEnvParameterfvARB(GL_VERTEX_PROGRAM_ARB, ii, (GLfloat *)savedParam);
            glProgramEnvParameter4fvARB(GL_VERTEX_PROGRAM_ARB, ii, (GLfloat *)param);
        }
        
    } else {
        _programSaved = NO;
    }
}

- (void)restore
{
    int			ii;
    _C3DTVector	*param;
    
    if (_programSaved) {
        glBindProgramARB( GL_VERTEX_PROGRAM_ARB, _savedProgramId);
        if (_savedProgramId)
            glEnable(GL_VERTEX_PROGRAM_ARB);
        else
            glDisable(GL_VERTEX_PROGRAM_ARB);

        // Restore local params
        for (ii = 0, param = _savedLocalParams;
             ii < _numLocalParams;
             ii++, param++) {
            glProgramLocalParameter4fvARB(GL_VERTEX_PROGRAM_ARB, ii, (GLfloat *)param);
        }

        // Save and set environmental params
        for (ii = 0, param = _savedEnvParams;
             ii < _numEnvParams;
             ii++, param++) {
            glProgramEnvParameter4fvARB(GL_VERTEX_PROGRAM_ARB, ii, (GLfloat *)param);
        }
    }
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeInt: _numLocalParams forKey: @"VertexProgramNumLocalParams"];
    [coder encodeInt: _numEnvParams forKey: @"VertexProgramNumEnvParams"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)_localParams
                    length: sizeof(_C3DTVector) * _maxLocalParams
                    forKey: @"VertexProgramLocalParams"];
        [coder encodeBytes: (const uint8_t *)_envParams
                    length: sizeof(_C3DTVector) * _maxEnvParams
                    forKey: @"VertexProgramEnvParams"];
    } else {
        [coder encodeBytes: (const uint8_t *)_localParams
                    length: sizeof(_C3DTVector) * _maxLocalParams];
        [coder encodeBytes: (const uint8_t *)_envParams
                    length: sizeof(_C3DTVector) * _maxEnvParams];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;
    const void	*codedData;
    
    self = [super initWithCoder: coder];

    _numLocalParams = [coder decodeIntForKey: @"VertexProgramNumLocalParams"];
    _numEnvParams = [coder decodeIntForKey: @"VertexProgramNumEnvParams"];
    if ( [coder allowsKeyedCoding] ) {
        codedData		= (const void *)[coder decodeBytesForKey: @"VertexProgramLocalParams" returnedLength: &len];
        _localParams	= (_C3DTVector *)calloc(len, 1);
        memcpy(_localParams, codedData, len);
        
        codedData		= (const void *)[coder decodeBytesForKey: @"VertexProgramEnvParams" returnedLength: &len];
        _envParams		= (_C3DTVector *)calloc(len, 1);
        memcpy(_envParams, codedData, len);
    } else {
        codedData	= [coder decodeBytesWithReturnedLength: &len];
        _localParams = (_C3DTVector *)calloc(len, 1);
        memcpy(_localParams, codedData, len);

        codedData	= [coder decodeBytesWithReturnedLength: &len];
        _envParams = (_C3DTVector *)calloc(len, 1);
        memcpy(_envParams, codedData, len);
    }
    
    if( [self _loadProgram: [[[NSBundle mainBundle] resourcePath] stringByAppendingFormat:@"/%@", [self name]]])
    {
        glGenProgramsARB( 1, &_programId );   // Create the program ID

        glBindProgramARB( GL_VERTEX_PROGRAM_ARB, _programId );
        glProgramStringARB( GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB,
                            _programSize, [_programData cString] );

        // Reset the program, we want to be clean going out
        glBindProgramARB( GL_VERTEX_PROGRAM_ARB, 0 );
    }

    // Allocate memory for the saved parameters
    glGetIntegerv(GL_MAX_PROGRAM_LOCAL_PARAMETERS_ARB, &_maxLocalParams);
    _savedLocalParams = (_C3DTVector *)calloc(_maxLocalParams, sizeof(_C3DTVector));

    glGetIntegerv(GL_MAX_PROGRAM_ENV_PARAMETERS_ARB, &_maxLocalParams);
    _savedEnvParams = (_C3DTVector *)calloc(_maxEnvParams, sizeof(_C3DTVector));
    
    return self;
}


- (_C3DTVector)localParamAtIndex: (int)anIndex
{
    _C3DTVector	result = {{0.0, 0.0, 0.0, 0.0}};
    
    if (!_localParams || (anIndex >= _maxLocalParams) || (anIndex < 0))
        return result;
    return _localParams[anIndex];
}

- (void)setLocalParam: (_C3DTVector)aParam atIndex: (int)anIndex
{
    if (!_localParams || (anIndex >= _maxLocalParams) || (anIndex < 0))
        return;

    _localParams[anIndex] = aParam;

    if (_numLocalParams <= anIndex)
        _numLocalParams = anIndex + 1;
}


- (_C3DTVector)envParamAtIndex: (int)anIndex
{
    _C3DTVector	result = {{0.0, 0.0, 0.0, 0.0}};

    if (!_envParams || (anIndex >= _maxEnvParams) || (anIndex < 0))
        return result;
    return _envParams[anIndex];
}

- (void)setEnvParam: (_C3DTVector)aParam atIndex: (int)anIndex
{
    if (!_envParams || (anIndex >= _maxEnvParams) || (anIndex < 0))
        return;

    _envParams[anIndex] = aParam;

    if (_numEnvParams <= anIndex)
        _numEnvParams = anIndex + 1;
}

@end

@implementation C3DTVertexProgram (PrivateMethods)

- (BOOL)_loadProgram: (NSString *)programFile
{
    if (_programData = [NSString stringWithContentsOfFile: programFile])
    {
        [_programData retain];
        _programSize = [_programData cStringLength];
        
        return YES;
    }

    return NO;
}

@end
