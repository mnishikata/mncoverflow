//
//  C3DTCylinder.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue May 20 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTCylinder.m,v 1.3 2003/06/29 13:55:55 pmanna Exp $
//
// $Log: C3DTCylinder.m,v $
// Revision 1.3  2003/06/29 13:55:55  pmanna
// Corrected translation during cylinder creation
//
// Revision 1.2  2003/06/15 09:39:11  pmanna
// Added a -draw method to deal with top- or bottom-less cylinder/cone (i.e. internal faces are visible)
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTCylinder.h"


@interface C3DTCylinder (PrivateMethods)
- (void)buildCylinderWithBase: (float)baseRadius top: (float)topRadius height: (float)height;
@end

@implementation C3DTCylinder

+ (C3DTCylinder *)cylinderWithBase: (float)baseRadius top: (float)topRadius height: (float)height
                            slices: (int)aSliceNumber stacks: (int)aStackNumber
{
    return [[[C3DTCylinder alloc] initWithBase: baseRadius
                                           top: topRadius
                                        height: height
                                        slices: aSliceNumber
                                        stacks: aStackNumber] autorelease];
}

- (id)initWithBase: (float)baseRadius top: (float)topRadius height: (float)height
            slices: (int)aSliceNumber stacks: (int)aStackNumber
{
    if ((self = [super init]) != nil) {
        if (aSliceNumber > 1)
            _slices = aSliceNumber;
        else
            _slices = 32;

        if (aStackNumber > 1)
            _stacks = aStackNumber;
        else
            _stacks = 32;

        _baseRadius	= baseRadius;
        _topRadius	= topRadius;
        _height		= height;

        [self buildCylinderWithBase: _baseRadius
                                top: _topRadius
                             height: _height];
    }

    return self;
}

- (float)baseRadius
{
    return _baseRadius;
}

- (void)setBaseRadius: (float)aRadius
{
    if (aRadius != _baseRadius) {
        _baseRadius = aRadius;
        
        [self buildCylinderWithBase: _baseRadius
                                top: _topRadius
                             height: _height];
    }
}

- (float)topRadius
{
    return _topRadius;
}

- (void)setTopRadius: (float)aRadius
{
    if (aRadius != _topRadius) {
        _topRadius = aRadius;

        [self buildCylinderWithBase: _baseRadius
                                top: _topRadius
                             height: _height];
    }
}

- (float)height
{
    return _height;
}

- (void)setHeight: (float)aValue
{
    if (aValue != _height) {
        _height = fabs(aValue);

        [self buildCylinderWithBase: _baseRadius
                                top: _topRadius
                             height: _height];
    }
}

- (void)draw
{
    if ((_baseRadius < 0.0) || (_topRadius < 0.0))	// If it's an open cylinder/cone...
        glDisable(GL_CULL_FACE);

    [super draw];
    
    if ((_baseRadius < 0.0) || (_topRadius < 0.0))	// If it's an open cylinder/cone...
        glEnable(GL_CULL_FACE);
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _baseRadius forKey: @"CylinderBase"];
    [coder encodeFloat: _topRadius forKey: @"CylinderTop"];
    [coder encodeFloat: _height forKey: @"CylinderHeight"];
    [coder encodeInt: _slices forKey: @"CylinderSlices"];
    [coder encodeInt: _stacks forKey: @"CylinderStacks"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    _baseRadius	= [coder decodeFloatForKey: @"CylinderBase"];
    _topRadius	= [coder decodeFloatForKey: @"CylinderTop"];
    _height		= [coder decodeFloatForKey: @"CylinderHeight"];
    _slices		= [coder decodeIntForKey: @"CylinderSlices"];
    _stacks		= [coder decodeIntForKey: @"CylinderStacks"];

    [self buildCylinderWithBase: _baseRadius
                            top: _topRadius
                         height: _height];
    return self;
}

@end

@implementation C3DTCylinder (PrivateMethods)

- (void)buildCylinderWithBase: (float)baseRadius top: (float)topRadius height: (float)height
{
    [self startDisplayList];

    gluQuadricDrawStyle([C3DTGeometry quadric], GLU_FILL);
    gluQuadricNormals([C3DTGeometry quadric], GLU_SMOOTH);

    gluQuadricOrientation([C3DTGeometry quadric], GLU_INSIDE);
    if (baseRadius > 0.0)
        gluDisk([C3DTGeometry quadric], 0.0, baseRadius, _slices, 3);
    gluQuadricOrientation([C3DTGeometry quadric], GLU_OUTSIDE);

    gluCylinder([C3DTGeometry quadric], fabs(baseRadius), fabs(topRadius), height,
                _slices, _stacks);

    if (topRadius > 0.0) {
        glTranslatef(0.0, 0.0, height);
        gluDisk([C3DTGeometry quadric], 0.0, topRadius, _slices, 3);
        glTranslatef(0.0, 0.0, -height);
    }

    [self stopDisplayList];
}

@end
