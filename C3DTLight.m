//
//  C3DTLight.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTLight.m,v 1.2 2003/06/28 10:47:04 pmanna Exp $
//
// $Log: C3DTLight.m,v $
// Revision 1.2  2003/06/28 10:47:04  pmanna
// Corrected light naming
//
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTLight.h"
#import <OpenGL/glu.h>

static	GLint	lightIdCounter = GL_LIGHT0;
static	GLint	maxLights = 0;

// $$$ To do: verify the ranges of colors, alpha & attenuations

@implementation C3DTLight

+ (int)nextLightId;
{
    if (maxLights == 0) {
        // Get the maximum number of lights we can use
        glGetIntegerv(GL_MAX_LIGHTS, &maxLights);
    }

    if (lightIdCounter >= GL_LIGHT0 + maxLights) {
        // $$$ Generate appropriate error

        return -1;
    }

    return (int)lightIdCounter++;
}

/*
 * Creates and enables a new light: preferred method to get a light
 * $$$ To change: we just create new lights, there is no recycling of light IDs that are no more used,
 * therefore it's easy to pass over the available lights if we change the lights at runtime
 * not a big problem anyway, a smart program can just disable, reenable and move lights at will
 */
+ (id)light
{
    int		aLightId = [C3DTLight nextLightId];

    if (aLightId >= GL_LIGHT0)
        return [[[C3DTLight alloc] initWithId: aLightId] autorelease];
    else
        return nil;
}

- (id)initWithId: (int)lightId
{
    if ((self = [super initWithName: [NSString stringWithFormat: @"GL_LIGHT%d", lightId - GL_LIGHT0]]) != nil) {
        _lightId = lightId;

        // Generates the standard values, according to the OpenGL Reference
        _pos.x = _pos.y = _pos.w = 0.0;					_pos.z = 1.0;
        _ambient.x = _ambient.y = _ambient.z = 0.0;		_ambient.w = 1.0;
        if (_lightId == GL_LIGHT0) {
            _diffuse.x = _diffuse.y = _diffuse.z = _diffuse.w = 1.0;
        }
        else {
            _diffuse.x = _diffuse.y = _diffuse.z = 0.0;		_diffuse.w = 1.0;
        }
        _specular = _diffuse;
        _attenuation.x = 1.0;	_attenuation.y = _attenuation.z = _attenuation.w = 0.0;

        glEnable(_lightId);

        // As these are the defaults, applying them is not needed...
    }

    return self;
}

- (void)dealloc
{
    // $$$ Possibly recycle the ID

    glDisable(_lightId);

    [super dealloc];
}

- (_C3DTVector)pos
{
    return _pos;
}

- (void)setPos: (_C3DTVector)aPos
{
    _pos = aPos;

    glLightfv(_lightId, GL_POSITION, (float *)&_pos);
}

- (void)setPosX: (float)x Y: (float)y Z: (float)z directional: (BOOL)dFlag
{
    _pos.x = x;
    _pos.y = y;
    _pos.z = z;
    _pos.w = (dFlag ? 0.0 : 1.0);

    glLightfv(_lightId, GL_POSITION, (float *)&_pos);
}

- (_C3DTVector)ambient
{
    return _ambient;
}

- (void)setAmbient: (_C3DTVector)aColor
{
    _ambient = aColor;

    glLightfv(_lightId, GL_AMBIENT, (float *)&_ambient);
}

- (void)setAmbientRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _ambient.x = redColor;
    _ambient.y = greenColor;
    _ambient.z = blueColor;
    _ambient.w = alphaValue;

    glLightfv(_lightId, GL_AMBIENT, (float *)&_ambient);
}

- (_C3DTVector)diffuse
{
    return _diffuse;
}

- (void)setDiffuse: (_C3DTVector)aColor
{
    _diffuse = aColor;

    glLightfv(_lightId, GL_DIFFUSE, (float *)&_diffuse);
}

- (void)setDiffuseRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _diffuse.x = redColor;
    _diffuse.y = greenColor;
    _diffuse.z = blueColor;
    _diffuse.w = alphaValue;

    glLightfv(_lightId, GL_DIFFUSE, (float *)&_diffuse);
}

- (_C3DTVector)specular
{
    return _specular;
}

- (void)setSpecular: (_C3DTVector)aColor
{
    _specular = aColor;

    glLightfv(_lightId, GL_SPECULAR, (float *)&_specular);
}

- (void)setSpecularRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _specular.x = redColor;
    _specular.y = greenColor;
    _specular.z = blueColor;
    _specular.w = alphaValue;

    glLightfv(_lightId, GL_SPECULAR, (float *)&_specular);
}


- (_C3DTVector)attenuation
{
    return _attenuation;
}

- (void)setAttenuationConstant: (float)c linear: (float)l quadratic: (float)q
{
    // Let's try to be smart and only applying what's really needed
    if (_attenuation.x != c) {
        _attenuation.x = c;
        glLightf(_lightId, GL_CONSTANT_ATTENUATION, c);
    }

    if (_attenuation.y != l) {
        _attenuation.y = l;
        glLightf(_lightId, GL_LINEAR_ATTENUATION, l);
    }

    if (_attenuation.z != q) {
        _attenuation.z = q;
        glLightf(_lightId, GL_QUADRATIC_ATTENUATION, q);
    }
}

- (void)apply
{
    // It seems necessary to apply lighting every time... because of positional, maybe?
    glLightfv(_lightId, GL_POSITION, (float *)&_pos);
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_pos length: sizeof(_C3DTVector) forKey: @"LightPos"];
        [coder encodeBytes: (const uint8_t *)&_ambient length: sizeof(_C3DTVector) forKey: @"LightAmbient"];
        [coder encodeBytes: (const uint8_t *)&_diffuse length: sizeof(_C3DTVector) forKey: @"LightDiffuse"];
        [coder encodeBytes: (const uint8_t *)&_specular length: sizeof(_C3DTVector) forKey: @"LightSpecular"];
        [coder encodeBytes: (const uint8_t *)&_attenuation length: sizeof(_C3DTVector) forKey: @"LightAttenuation"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_pos length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_ambient length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_diffuse length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_specular length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_attenuation length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        _pos			= *(_C3DTVector *)[coder decodeBytesForKey: @"LightPos" returnedLength: &len];
        _ambient		= *(_C3DTVector *)[coder decodeBytesForKey: @"LightAmbient" returnedLength: &len];
        _diffuse		= *(_C3DTVector *)[coder decodeBytesForKey: @"LightDiffuse" returnedLength: &len];
        _specular		= *(_C3DTVector *)[coder decodeBytesForKey: @"LightSpecular" returnedLength: &len];
        _attenuation	= *(_C3DTVector *)[coder decodeBytesForKey: @"LightAttenuation" returnedLength: &len];
    } else {
        _pos			= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _ambient		= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _diffuse		= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _specular		= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _attenuation	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }

    _lightId = [C3DTLight nextLightId];

    [self setPos: _pos];
    [self setAmbient: _ambient];
    [self setDiffuse: _diffuse];
    [self setSpecular: _specular];
    [self setAttenuationConstant: _attenuation.x
                          linear: _attenuation.y
                       quadratic: _attenuation.z];
    
    glEnable(_lightId);
    
    return self;
}

@end
