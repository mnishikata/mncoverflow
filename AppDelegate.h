/* AppDelegate */

#import <Cocoa/Cocoa.h>
//#import "MNNodeObject.h"
//#import "AngleAnimation.h"
#import <MNCoverFlow/MNCoverFlow.h>

@interface AppDelegate : NSObject <FlowViewDataSource>
{
    IBOutlet id linearView;
    IBOutlet id roundView;
	IBOutlet id window;
	IBOutlet id view;
	
	double Nmax_plus_1;
	int R;
	double Ns;

	double Ns_previous;
	double Ncenter;

	
	NSMutableArray* nodes;
	
	
	AngleAnimation* animation;
	
	
	NSImage *unknownImage;
	//Datasource
	
	NSArray* paths;
	
	
	// debug
	
	double animationTimeForDebug;
	
	
	//thread
	
	NSLock *lock;
	NSMutableArray *iconRequest;
}

-(double)DURATION1;
-(double)SIDE_SPACE_FACTOR;
-(double)SIDE_ANGLE;
-(double)Z_FRONT;
-(void)awakeFromNib;
- (void)willChangeValueForKey:(NSString *)key;
-(void)updateView;
-(NSArray*)nodes;
-(double)Ns_previous;
-(double)Ns;
-(int)R;
-(double)Ncenter;
- (void)animationDidEnd:(AngleAnimation*)animation;
- (void)animationDidStop:(AngleAnimation*)animation;
- (void)didChangeValueForKey:(NSString *)key;
-(void)	drawRoundView:(NSRect)rect;
-(unsigned)numberOfCovers;
-(NSImage*)imageForCoverAtIndex:(unsigned)index;
-(NSString*)titleForCoverAtIndex:(unsigned)index;
-(NSImage*)imageForCoverAtIndexImmedeately:(unsigned)index;

@end
