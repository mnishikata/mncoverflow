//
//  C3DTGeometry.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTGeometry.h,v 1.2 2003/06/21 19:21:17 pmanna Exp $
//
// $Log: C3DTGeometry.h,v $
// Revision 1.2  2003/06/21 19:21:17  pmanna
// Corrected with addition of abstract -animate
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTObject.h"
#import "C3DTMath.h"
#import <OpenGL/glu.h>


@interface C3DTGeometry : C3DTObject {
    int		_displayListId;
    BOOL	_isReference;
}

+ (GLUquadricObj *)quadric;

- (void)draw;
- (void)animate;

- (void)startDisplayList;
- (void)stopDisplayList;

- (BOOL)isReference;
- (void)setIsReference:(BOOL)newIsReference;

@end
