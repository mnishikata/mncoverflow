/*
 *  Functions.c
 *  MNC3DT
 *
 *  Created by Masatoshi Nishikata on 08/02/28.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

#include "Functions.h"
#import "MNNodeObject.h"


//#define FAST_CALC YES
double FAST_CALC_CACHE0 = 0;
double FAST_CALC_CACHE_TIME0 = 0;
double FAST_CALC_CACHE1 = 0;
double FAST_CALC_CACHE_TIME1 = 0;


static double template[101] = {-0.000000, 0.141067, 0.198997, 0.243105, 0.280000, 0.312250, 0.341174, 0.367560, 0.391918, 0.414608, 0.435890, 0.455961, 0.474974, 0.493052, 0.510294, 0.526783, 0.542586, 0.557763, 0.572364, 0.586430, 0.600000, 0.613107, 0.625780, 0.638044, 0.649923, 0.661438, 0.672607, 0.683447, 0.693974, 0.704202, 0.714143, 0.723809, 0.733212, 0.742361, 0.751266, 0.759934, 0.768375, 0.776595, 0.784602, 0.792401, 0.800000, 0.807403, 0.814616, 0.821645, 0.828493, 0.835165, 0.841665, 0.847998, 0.854166, 0.860174, 0.866025, 0.871722, 0.877268, 0.882666, 0.887919, 0.893029, 0.897998, 0.902829, 0.907524, 0.912086, 0.916515, 0.920815, 0.924986, 0.929032, 0.932952, 0.936750, 0.940425, 0.943981, 0.947418, 0.950737, 0.953939, 0.957027, 0.960000, 0.962860, 0.965609, 0.968246, 0.970773, 0.973191, 0.975500, 0.977701, 0.979796, 0.981784, 0.983667, 0.985444, 0.987117, 0.988686, 0.990152, 0.991514, 0.992774, 0.993932, 0.994987, 0.995942, 0.996795, 0.997547, 0.998198, 0.998749, 0.999200, 0.999550, 0.999800, 0.999950, 1.000000,  1.0};



/*
 
 GLOBAL
 
 */
double DURATION0 = 0.3;
double DURATIOND = 0.6;
double DURATION1 = 1.0;
// must be more than 1 sec

double SIDE_SPACE_FACTOR = 40;
double SIDE_ANGLE = 3.14159265358979323846/3;
double Z_FRONT = 50;

double PI_POSITION = 0;

static double rawIntegralValue = 0;

 int Calc_r(double N, int R)
{
	return floor( (N+1)/(double)R)+1;	
}


double Calc_N(int R,  double r, int n)
{
	return R*(r-1)+n;
}

int Calc_n(int R,  double N)
{
	return fmod(N, R);
}
// Functions

double PatternTemplate(double time)
{
	////NSLog(@"x %f, y %f",time, RegularizedIntegral( time,  DURATION0/DURATION1, 1.0,  DURATION0/DURATION1));
	//return RegularizedIntegral( time,  DURATION0/DURATION1, 1.0,  DURATIOND/DURATION1);
	
#ifdef FAST_CALC
	if( time = FAST_CALC_CACHE_TIME0 )
		return FAST_CALC_CACHE0;
	else
	{
		time = FAST_CALC_CACHE_TIME0;
		//FAST_CALC_CACHE0 = sqrt(1- pow(time-1, 2));
		FAST_CALC_CACHE0 = sin(time);
		return FAST_CALC_CACHE0;
	}
#endif
	//
	
	return template[ (int) (time*100.0 ) ];
	
	//return sqrt(1- pow(time-1, 2));
	//return sqrt(time);
	//return time;
}

double PatternTemplateForRotation(double time)
{
#ifdef FAST_CALC
	if( time = FAST_CALC_CACHE_TIME1 )
		return FAST_CALC_CACHE1;
	else
	{
		time = FAST_CALC_CACHE_TIME1;
		FAST_CALC_CACHE1 = sqrt(1- pow(time*4-1, 2));
		return FAST_CALC_CACHE1;
	}
#endif
	
	double  p;
	if( time < 0.2 ) p= sqrt(1- pow(time*4-1, 2));
	else p = 1.0;
	//
	return p;
}

/*
double  __Spacer(double progress, Boolean flag)
//flag = true: increase , false: cancel spacer
{
	
	if( progress < 0) progress = 0;
	if( progress >1.0) progress = 1.0;
	
	//use template
	if( flag )	return PatternTemplate(progress) * SIDE_SPACE_FACTOR;
	else	return (1-PatternTemplate(progress)) * SIDE_SPACE_FACTOR;
		
	

}
*/

/*
double Z(double time, double N, double Ns, double Ns_previous)
{
	if( N == Ns_previous ) return Z_Animation(time, Z_FRONT, 0);
	if( N == Ns ) return Z_Animation(time, 0, Z_FRONT );	
	return 0.0;
}*/
double Spacing_Animation(double time, double start, double end )
{
	
	double progress = time/DURATION1;
	if( progress < 0) progress = 0;
	if( progress >1.0) progress = 1.0;
	
	
	if( end > start )	return (end - start)*PatternTemplate(progress) + start;
	else	return -(end - start)*(1-PatternTemplate(progress)) + end;

	
	
	
//	return (end - start)*progress + start; // linear

}

double Z_Animation(double time, double startz, double endz )
{
	
	double progress = time/DURATION1;
	if( progress < 0) progress = 0;
	if( progress >1.0) progress = 1.0;
	
	return (endz - startz)*PatternTemplate(progress) + startz;
	
}

/*
double Rotation(double time, double N, double Ns, double Ns_previous)
{
	double side_angle = SIDE_ANGLE;
	
	if( Ns_previous < Ns )
	{
		if( N < Ns_previous) return -side_angle;
		if( N == Ns_previous ) return Rotaion_Animation(time, 0, -side_angle, 0);
		if( (Ns_previous<N) && (N < Ns) ) return Rotaion_Animation(time, side_angle, -side_angle, 1);
		if( N == Ns ) return Rotaion_Animation(time, side_angle, 0, 0);
		if( N >Ns ) return side_angle;

		
	}else
	{
		if( N < Ns ) return -side_angle;
		if( N == Ns ) return Rotaion_Animation(time, -side_angle, 0,1 );
		if( (Ns<N) && (N < Ns_previous) ) return Rotaion_Animation(time, -side_angle, side_angle,1);
		if( N == Ns_previous ) return Rotaion_Animation(time, 0, side_angle,1);
		if( N > Ns_previous ) return side_angle;
	}
	
}*/

double Rotaion_Animation(double time, double startAngle, double endAngle, Boolean animateClockwise)
{
	/*
	 animateClockwise 
	 
	 -45 -> -90 -> 90 -> 45
	 */
	double vEndAngle = endAngle;
	if( (animateClockwise == true )&& (endAngle<startAngle) )
	{
		vEndAngle += pi; 
	}
	
	if( (animateClockwise == true )&& (endAngle>startAngle) )
	{
		vEndAngle -= pi; 
	}
	
	double progress = time/DURATION1;
	if( progress < 0) progress = 0;
	if( progress >1.0) progress = 1.0;
	
	
	double currentRot = (vEndAngle - startAngle)*PatternTemplateForRotation(progress) + startAngle;

	if( (animateClockwise == true )&& (endAngle<startAngle) )
	{
		if( currentRot > pi/2 ) currentRot -= pi;
	}
	
	if( (animateClockwise == true )&& (endAngle>startAngle) )
	{
		if( currentRot < pi/2 ) currentRot += pi;
	}
	
	
	return 	currentRot;
}

CoverCoordinate CoverCoordinateForN(double N, int R)
{
	CoverCoordinate coordinate;
	coordinate.r = Calc_r(N,R);
	coordinate.angle = ceil(N/R);
	coordinate.z = 0;
	
	return coordinate;
}

double Angle( double time, double Ns, double Ns_previous, int R )
/*
 普通に場所を移動するときの角度とラウンドの、初期値に対する差を返す。スクロール中にクリックして止めるときは該当しない
 */
{

	
	double progress = time/DURATION1;
	if( progress < 0) progress = 0;
	if( progress >1.0) progress = 1.0;

	
	// Ns_previous angle = pi
	
	double angle;
	if( Ns>Ns_previous)
	angle = (double)(Ns - Ns_previous)*PatternTemplate(progress) * (2*pi/R) ;
	else
		angle = -(double)(Ns_previous - Ns)*PatternTemplate(progress) * (2*pi/R) ;

	return angle;
	
}


CoverCoordinate Angle_Animation( double progress, double startN,  double endN, int R, double *progressN )
/*
 普通に場所を移動するときの角度とラウンドの、初期値に対する差を返す。スクロール中にクリックして止めるときは該当しない

 
 
 
 
 */
{
	
	CoverCoordinate c;
	c.r = 0;
	
	if( progress < 0) progress = 0;
	if( progress >1.0) progress = 1.0;
	
	
	// Ns_previous angle = pi
	*progressN =  ( endN  - startN) * PatternTemplate(progress ) ;
	
	//convert N to angle
	c.angle = 	(*progressN) * (2*pi / R );
	return RegularizeCoordinate(c);
		
}


CoverCoordinate RegularizeCoordinate(CoverCoordinate c)
{
	CoverCoordinate cc = c;
	
	if( cc.angle >= 2*pi )
	{
		cc.r++;
		cc.angle -= 2*pi ;
		
		cc = RegularizeCoordinate(cc);
	}
	
	else if( c.angle < 0 )
	{
		cc.r--;
		cc.angle += 2*pi ;
		
		cc = RegularizeCoordinate(cc);

	}
	
	return cc;
}

CoverCoordinate AddCoordinates(CoverCoordinate c1, CoverCoordinate c2)
{
	
	CoverCoordinate c3 = c1;
	

	
	c3.angle += c2.angle;
	c3.r += c2.r;
	

	
	return  RegularizeCoordinate(c3);
	
}


CoverCoordinate SubtractCoordinates(CoverCoordinate c1, CoverCoordinate c2)
{
	
	CoverCoordinate c3 = c1;
	
	
	
	c3.angle -= c2.angle;
	c3.r -= c2.r;
	
	
	
	return  RegularizeCoordinate(c3);
	
}



NSComparisonResult CompareCoordinates(CoverCoordinate c1, CoverCoordinate c2)
{
	if( c1.r > c2.r ) return NSOrderedAscending;
	if( c1.r < c2.r ) return NSOrderedDescending;
	// ↑ここの不等号が逆だった！

	if( c1.angle < c2.angle ) return NSOrderedAscending;
	if( c1.angle > c2.angle ) return NSOrderedDescending;

	return NSOrderedSame;
}

/*
 
 座標変換
 
 */

void ConvertToLinear_ResetPiPosition(double min, double max)
{
	//PI_POSITION = (min+max)/2;
	// ** 
}

double ConvertToLinear( double time, CoverCoordinate coordinate, NSArray* nodeObjects, double Ns, double Ns_previous, double min, double max )

{
	
	
	if( coordinate.r == pi ) return (min+max)/2 - SIDE_SPACE_FACTOR;

	double relativeX = ConvertToLinear_RelativeXFromMin(  time,  coordinate, nodeObjects,  Ns,  Ns_previous,  min,  max );
	
	CoverCoordinate midPointCoordinate;
	midPointCoordinate.r = 0;
	midPointCoordinate.angle = pi;
	double relativeMidX = ConvertToLinear_RelativeXFromMin(  time,  midPointCoordinate, nodeObjects,  Ns,  Ns_previous,  min,  max );
	
	double offset = relativeMidX - (min+max)/2;
	//NSLog(@"time %f,Ns %f,Ns_previous %f, offset %f, relativeX %f",time,Ns,Ns_previous, PI_POSITION,relativeX);
	
	return -(relativeX - offset)- SIDE_SPACE_FACTOR;
}


double _ConvertToLinear( double time, CoverCoordinate coordinate, NSArray* nodeObjects, double Ns, double Ns_previous, double min, double max )

{

		
		
	double relativeX = ConvertToLinear_RelativeXFromMin(  time,  coordinate, nodeObjects,  Ns,  Ns_previous,  min,  max );

	CoverCoordinate midPointCoordinate;
	midPointCoordinate.r = 0;
	midPointCoordinate.angle = pi;
	//double relativeMidX = ConvertToLinear_RelativeXFromMin(  time,  midPointCoordinate, nodeObjects,  Ns,  Ns_previous,  min,  max );

	//double offset = relativeMidX - (min+max)/2;
	//NSLog(@"time %f,Ns %f,Ns_previous %f, offset %f, relativeX %f",time,Ns,Ns_previous, PI_POSITION,relativeX);
	
	return -(relativeX - PI_POSITION);
}




double ConvertToLinear_RelativeXFromMin( double time, CoverCoordinate coordinate, NSArray* nodeObjects, double Ns, double Ns_previous, double min, double max )
//min からのそうたいてきな距離を求める
{
	
	
	// angle 0 =min, angle 2*pi = max
	double linearXBeforeSpacing = (max- min)* (  coordinate.angle/ (2*pi)) + min;
	
	
	int R = [nodeObjects count];
	
	double standardNodeSpacing = 2*pi/ R;
	CoverCoordinate standardNodeSpacingCoordinate;
	standardNodeSpacingCoordinate.angle = 2*pi/ R;
	standardNodeSpacingCoordinate.r = 0;
	
	int hoge;
	for( hoge = 0; hoge < R;hoge++ )
	{
		
		MNNodeObject *node = [nodeObjects objectAtIndex:hoge];
		CoverCoordinate aCoord = [node coordinate];
		
		/*
		 if( aCoord.angle < coordinate.angle )
		 {
			 linearXBeforeSpacing += aCoord.spacing*2;
		 }else 		if( aCoord.angle == coordinate.angle )
		 {
			 linearXBeforeSpacing += aCoord.spacing;
			 
		 }
		 
		 */
		double anN =  Calc_N( R, aCoord.r , [node n] );
		
		
		/*
		 
		 
		 if(  anN == Ns ) 
		 {
			 //NSLog(@"Ns Spacing %f",aCoord.spacing);
			 if( (aCoord.angle +standardNodeSpacing) <= coordinate.angle ) //A
			 {
				 linearXBeforeSpacing += aCoord.spacing * 2;
				 
				 
			 }else 	if( ( (aCoord.angle +standardNodeSpacing) > coordinate.angle) &&
						( aCoord.angle <= coordinate.angle ) ) //B
			 {
				 double angleProgress = ( coordinate.angle - aCoord.angle ) / standardNodeSpacing;
				 linearXBeforeSpacing += aCoord.spacing * (1+angleProgress);
				 
				 
			 }else if( ( (aCoord.angle -standardNodeSpacing) < coordinate.angle) &&
					   ( aCoord.angle > coordinate.angle ) ) //C
			 {
				 double angleProgress = ( coordinate.angle - (aCoord.angle - standardNodeSpacing)  ) / standardNodeSpacing;
				 linearXBeforeSpacing += aCoord.spacing * angleProgress;
				 
				 
			 }else if( aCoord.angle > coordinate.angle + standardNodeSpacing ) //D
			 {
			 }
			 
		 }else if(  anN == Ns_previous ) 
		 {
			 //NSLog(@"Ns_previous Spacing %f\n",aCoord.spacing);
			 
			 if( (aCoord.angle +standardNodeSpacing) <= coordinate.angle ) //A
			 {
				 linearXBeforeSpacing += aCoord.spacing * 2;
				 
				 
			 }else 	if( ( (aCoord.angle +standardNodeSpacing) > coordinate.angle) &&
						( aCoord.angle <= coordinate.angle ) ) //B
			 {
				 double angleProgress = ( coordinate.angle - aCoord.angle ) / standardNodeSpacing;
				 linearXBeforeSpacing += aCoord.spacing * (1+angleProgress);
				 
				 
			 }else if( ( (aCoord.angle -standardNodeSpacing) < coordinate.angle) &&
					   ( aCoord.angle > coordinate.angle ) ) //C
			 {
				 double angleProgress = ( coordinate.angle - (aCoord.angle - standardNodeSpacing)  ) / standardNodeSpacing;
				 linearXBeforeSpacing += aCoord.spacing * angleProgress;
				 
				 
			 }else if( aCoord.angle > coordinate.angle + standardNodeSpacing ) //D
			 {
			 }
			 
		 }
		 */
		
		
		
		/*
		 
		 スペーシングやローテーションはノードが持つ。周期毎に重複してしまうが、どうせ必要なのは選択部分周囲なので大丈夫
		 */
		
		//NSLog(@"Ns %f, N=%f",Ns, anN );
		
		
		BOOL calcSpacing = NO;
		
		if( abs(Ns-Ns_previous)>R )
			calcSpacing = ( Ns==anN ) || ( Ns_previous==anN );
		else
			calcSpacing = ( abs(Ns - anN)<4 || abs(Ns_previous - anN)<4 );
		
		
		if( calcSpacing )//if( aCoord.spacing != 0 )
		{
			CoverCoordinate aCoordPlusStandardNodeSpacing = AddCoordinates(aCoord, standardNodeSpacingCoordinate);
			CoverCoordinate aCoordMinusStandardNodeSpacing = SubtractCoordinates(aCoord, standardNodeSpacingCoordinate);
			
			if( CompareCoordinates( aCoordPlusStandardNodeSpacing, coordinate ) != NSOrderedDescending  ) //A
			{
				//CoverCoordinate sa = SubtractCoordinates(aCoord, coordinate);
				//if( sa.r == 0 )
				linearXBeforeSpacing += aCoord.spacing * 2;
				
				
			}else 	if( ( CompareCoordinates( aCoordPlusStandardNodeSpacing, coordinate ) == NSOrderedDescending) &&
						( CompareCoordinates( aCoord, coordinate) != NSOrderedDescending ) )
			{
				double angleProgress = SubtractCoordinates(coordinate,aCoord).angle / standardNodeSpacing;
				linearXBeforeSpacing += aCoord.spacing * (1+angleProgress);
				
				
			}else if( ( CompareCoordinates( aCoordMinusStandardNodeSpacing, coordinate ) == NSOrderedAscending ) &&
					  ( CompareCoordinates( aCoord, coordinate) == NSOrderedDescending ) ) //C
			{
				double angleProgress = SubtractCoordinates( coordinate, aCoordMinusStandardNodeSpacing).angle / standardNodeSpacing;
				linearXBeforeSpacing += aCoord.spacing * angleProgress;
				
				
				
				
			}else if( aCoord.angle > coordinate.angle + standardNodeSpacing ) //D
			{
			}
		}
		
		
		
		
		
	}
	
	
	
	
	// linearXBeforeSpacing ... relative x position from min;
	
	return linearXBeforeSpacing;
	
}


double RawVelocity(double x, double d0, double d1, double dd)
// x = Duration0, duration1, y max is 1
{
	if( x <= d0 ) return (1/d0) *x;
	if( (x > d0) && (x < dd ) )
	{
		return 1;
	}
	
	if( x >= dd )
	{
		return -x/(d1-dd)+ d1/(d1-dd);
	}
}

double RawIntegral(double x, double d0, double d1, double dd)
// x = Duration0, duration1, y max is 1
{
	if( x <= d0 ) return (1/(d0*2)) *pow(x,2);
	if( (x > d0) && (x < dd ) )
	{
		return  d0/2 + (x-d0);
	}
	
	if( x >= dd )
	{
		return d0/2 + (dd-d0) + ( 1 +( -x/(d1-dd)+ d1/(d1-dd))) * (x-dd)/2;
	}
}

double RegularizedIntegral(double x, double d0, double d1, double dd)
{
	if( rawIntegralValue == 0 ) rawIntegralValue = RawIntegral( d1,  d0,  d1,  dd);
	//NSLog(@"limitValue %f",limitValue);
	
	return RawIntegral( x,  d0,  d1,  dd)/rawIntegralValue;
	
}


/*
double RawVelocity(double x, double d0, double d1, double dd)
// x = Duration0, duration1, y max is 1
{
	if( x <= d0 ) return (1/d0) *x;
	if( (x > d0) && (x < dd ) )
	{
		return 1;
	}
	
	if( x >= dd )
	{
		return -sin(pi*x/(2*d1))+1;
	}
}

double RawIntegral(double x, double d0, double d1, double dd)
// x = Duration0, duration1, y max is 1
{
	if( x <= d0 ) return (1/(d0*2)) *pow(x,2);
	if( (x > d0) && (x < dd ) )
	{
		return  d0/2 + (x-d0);
	}
	
	if( x >= dd )
	{
		return d0/2 + (dd-d0) + (- (2*d1/pi)*cos( pi*x/(2*d1))+x-d0/2 +2*d1*cos(pi*dd/(2*d1))/pi );
	}
}

double RegularizedIntegral(double x, double d0, double d1, double dd)
{
	double limitValue;
	
	limitValue = RawIntegral( d1,  d0,  d1,  dd);
	
	return RawIntegral( x,  d0,  d1,  dd)/limitValue;
	
}*/

#pragma mark ===============

/*
 状態Ａ→Ｂ
 
 途中でキャンセルされたら、その状態から新たな状態Ｃに遷移する。
 
 状態は、time、N, 以前のNs, 及び、新しいNsによって決まる
 
 */

double CalcRotationForN( double N, double Ns_start, double Ns_end,  double progress)
/*
N 番目のカバーの回転角を計算する。Ns_start -> Ns_end,  
 
 */
{
	double rotation;
	
	
	
	return rotation;
	
}
