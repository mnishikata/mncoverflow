//
//  C3DTPlainSurface.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Thu Jul 03 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTPlainSurface.m,v 1.2 2003/07/08 11:24:54 pmanna Exp $
//
// $Log: C3DTPlainSurface.m,v $
// Revision 1.2  2003/07/08 11:24:54  pmanna
// Corrected quad generation winding
//
// Revision 1.1  2003/07/04 13:11:25  pmanna
// Initial import in CVS
//
//

#import "MNCFCover.h"
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#include <GLUT/glut.h>

#define REFLECTION_DARK 0.1
#define REFLECTION_LIGHT 0.5
#define BODYCOLOR 1.0


@implementation MNCFCover

+ (MNCFCover *)planeWithWidth: (float)aWidth
                               depth: (float)aDepth
                           meridians: (int)tessX
                           parallels: (int)tessY
                       textureRepeat: (BOOL)repeat
{
    return [[[MNCFCover alloc] initWithWidth: aWidth
                                              depth: aDepth
                                          meridians: tessX
                                          parallels: tessY
                                      textureRepeat: repeat] autorelease];
}

- (id)initWithWidth: (float)aWidth
              depth: (float)aDepth
          meridians: (int)tessX
          parallels: (int)tessY
      textureRepeat: (BOOL)repeat
{
    if ((self = [super init]) != nil) {
        _width		= fabs(aWidth);
        _depth		= fabs(aDepth);
        _meridians	= (tessX <= 0 ? 1 : tessX);
        _parallels	= (tessY <= 0 ? 1 : tessY);
        _repeat		= repeat;
		
		
		visiblity = 1.0;

        [self buildPlaneWithWidth: _width
                            depth: _depth
                        meridians: _meridians
                        parallels: _parallels
                    textureRepeat: _repeat];
    }

    return self;
}


- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _width forKey: @"PlainSurfaceWidth"];
    [coder encodeFloat: _depth forKey: @"PlainSurfaceDepth"];
    [coder encodeInt: _meridians forKey: @"PlainSurfaceMeridians"];
    [coder encodeInt: _parallels forKey: @"PlainSurfaceParallels"];
    [coder encodeBool: _repeat forKey: @"PlainSurfaceRepeat"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    _width		= [coder decodeFloatForKey: @"PlainSurfaceWidth"];
    _depth		= [coder decodeFloatForKey: @"PlainSurfaceDepth"];
    _meridians	= [coder decodeIntForKey: @"PlainSurfaceMeridians"];
    _parallels	= [coder decodeIntForKey: @"PlainSurfaceParallels"];
    _repeat		= [coder decodeBoolForKey: @"PlainSurfaceRepeat"];

    [self buildPlaneWithWidth: _width
                        depth: _depth
                    meridians: _meridians
                    parallels: _parallels
                textureRepeat: _repeat];
    return self;
}

- (void)draw
{
    glDisable(GL_CULL_FACE);
    [super draw];
    glEnable(GL_CULL_FACE);
}

- (float)width {
    return _width;
}

- (void)setWidth: (float)newWidth {
    _width = newWidth;
}

- (float)depth {
    return _depth;
}

- (void)setDepth: (float)newDepth {
    _depth = newDepth;
}

- (int)meridians {
    return _meridians;
}

- (void)setMeridians: (int)newMeridians {
    _meridians = newMeridians;
}

- (int)parallels {
    return _parallels;
}

- (void)setParallels: (int)newParallels {
    _parallels = newParallels;
}

- (void)buildPlaneWithWidth: (float)aWidth
                      depth: (float)aDepth
                  meridians: (int)tessX
                  parallels: (int)tessY
              textureRepeat: (BOOL)repeat
{
	visiblity = 1.0;

    int			ii, jj;
    float		stepX		= aWidth / (float)tessX;
    float		stepY		= aDepth / (float)tessY;
    float		texStepX	= 1.0 / (float)tessX;
    float		texStepY	= 1.0 / (float)tessY;
    float		normal[3]	= { 0.0, 1.0, 0.0 };	// Normal is fixed
    float		vertex[3]	= { -aWidth / 2.0, 0.0, -aDepth / 2.0 };
    float		texCoord[2]	= { 0.0, 0.0 };
    
    [self startDisplayList];
    
	/*
	glEnable(GL_BLEND);
	glBlendFunc(  GL_ONE, GL_SRC_ALPHA);
	*/
	
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	//glEnable(GL_DEPTH_TEST);
	
	

	glBegin(GL_QUADS);

	//GLfloat dark[] = { 0.0, 0.0, 0.0, 1.0 }; 
	
	
//	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, dark); 
	

	
	// main body
	vertex[0]	=   -aWidth / 2.0;
	vertex[2] = -aDepth / 2.0 ;
    for (ii = 0; ii < tessX; ii++, vertex[0] += stepX) {
        vertex[2]	= -aDepth / 2.0;
        texCoord[1]	= 0.0;
        for (jj = 0; jj < tessY; jj++, vertex[2] += stepY) {
            glNormal3fv(normal);

            if (repeat)
                glTexCoord2f(0.0, 0.0);
            else
				glTexCoord2f(texCoord[0], texCoord[1] + texStepY);
			
			glColor3f(BODYCOLOR*visiblity , BODYCOLOR*visiblity , BODYCOLOR*visiblity);
            glVertex3f(vertex[0], vertex[1], vertex[2]);

            if (repeat)
                glTexCoord2f(0.0, 1.0);
            else
				glTexCoord2f(texCoord[0], texCoord[1]);
			
			glColor3f(BODYCOLOR*visiblity , BODYCOLOR*visiblity , BODYCOLOR*visiblity);
            glVertex3f(vertex[0], vertex[1], vertex[2] + stepY);
            
            if (repeat)
                glTexCoord2f(1.0, 1.0);
            else
				glTexCoord2f(texCoord[0] + texStepX, texCoord[1]);

			glColor3f(BODYCOLOR*visiblity , BODYCOLOR*visiblity , BODYCOLOR*visiblity);
            glVertex3f(vertex[0] + stepX, vertex[1], vertex[2] + stepY);

            if (repeat)
                glTexCoord2f(1.0, 0.0);
            else
				glTexCoord2f(texCoord[0] + texStepX, texCoord[1] + texStepY);

			glColor3f(BODYCOLOR*visiblity , BODYCOLOR*visiblity , BODYCOLOR*visiblity);
            glVertex3f(vertex[0] + stepX, vertex[1], vertex[2]);

            if (!repeat)
                texCoord[1] += texStepY;
        }
        if (!repeat)
            texCoord[0] += texStepX;
    }
	
	
	vertex[0]	=   -aWidth / 2.0;
	vertex[2] = -aDepth / 2.0 ;

	
    for (ii = 0; ii < tessX; ii++, vertex[0] += stepX) {
        vertex[2]	= -aDepth / 2.0;
        texCoord[1]	= 0.0;
        for (jj = 0; jj < tessY; jj++, vertex[2] += stepY) {
            glNormal3fv(normal);
			
            if (repeat)
                glTexCoord2f(0.0, 0.0);
            else
				glTexCoord2f(texCoord[0], texCoord[1]);

			glColor3f(REFLECTION_LIGHT *visiblity, REFLECTION_LIGHT*visiblity , REFLECTION_LIGHT*visiblity);
            glVertex3f(vertex[0], vertex[1], vertex[2] - vertex[2]*2);
			
            if (repeat)
                glTexCoord2f(0.0, 1.0);
            else
				glTexCoord2f(texCoord[0], texCoord[1] + texStepY);

			glColor3f(REFLECTION_DARK*visiblity , REFLECTION_DARK*visiblity , REFLECTION_DARK*visiblity);
            glVertex3f(vertex[0], vertex[1], vertex[2] + stepY - vertex[2]*2);
            
            if (repeat)
                glTexCoord2f(1.0, 1.0);
            else
				glTexCoord2f(texCoord[0] + texStepX, texCoord[1] + texStepY);

			glColor3f(REFLECTION_DARK*visiblity , REFLECTION_DARK*visiblity , REFLECTION_DARK*visiblity);
            glVertex3f(vertex[0] + stepX, vertex[1], vertex[2] + stepY - vertex[2]*2);
			
            if (repeat)
                glTexCoord2f(1.0, 0.0);
            else
				glTexCoord2f(texCoord[0] + texStepX, texCoord[1]);

			glColor3f(REFLECTION_LIGHT*visiblity , REFLECTION_LIGHT*visiblity , REFLECTION_LIGHT*visiblity);
            glVertex3f(vertex[0] + stepX, vertex[1], vertex[2] - vertex[2]*2);
			
            if (!repeat)
                texCoord[1] += texStepY;
        }
        if (!repeat)
            texCoord[0] += texStepX;
    }	


    glEnd();


    [self stopDisplayList];
}
-(void)setVisibity:(float)v
{
	visiblity = v;
}
@end
