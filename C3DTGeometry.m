//
//  C3DTGeometry.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTGeometry.m,v 1.2 2003/06/21 19:21:17 pmanna Exp $
//
// $Log: C3DTGeometry.m,v $
// Revision 1.2  2003/06/21 19:21:17  pmanna
// Corrected with addition of abstract -animate
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTGeometry.h"

static GLUquadricObj	*quadObj = NULL;
static int				geometryCounterId = 0;

@implementation C3DTGeometry

+ (GLUquadricObj *)quadric
{
    if (quadObj == NULL) {
        quadObj = gluNewQuadric();
        // $$$ Generate appropriate error code if NULL
        gluQuadricNormals( quadObj, GL_SMOOTH );   // Generate smooth normals
        gluQuadricTexture( quadObj, GL_TRUE );
    }

    return quadObj;
}

- (id)init
{
    if ((self = [super initWithName: [NSString stringWithFormat: @"Geometry-%ld", geometryCounterId++]]) != nil) {
        _displayListId = -1;
    }

    return self;
}

- (void)dealloc
{
    if (_displayListId != -1) {
        glDeleteLists(_displayListId, 1);
    }

    [super dealloc];
}

- (void)draw
{
    if (_displayListId != -1) {
        glCallList(_displayListId);
    }
}

- (void)animate
{
    // This is abstract, subclasses can override it
}

- (void)startDisplayList
{
    if (_displayListId != -1) {
        glDeleteLists(_displayListId, 1);
    }
    
    glNewList(_displayListId = glGenLists(1), GL_COMPILE);
}

- (void)stopDisplayList
{
    glEndList();
}

- (BOOL)isReference {
    return _isReference;
}

- (void)setIsReference: (BOOL)newIsReference
{
    _isReference = newIsReference;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeBool: _isReference forKey: @"GeometryIsReference"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    _isReference = [coder decodeBoolForKey: @"GeometryIsReference"];

    return self;
}

@end
