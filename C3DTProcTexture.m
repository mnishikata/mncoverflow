//
//  C3DTProcTexture.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Mon Jun 16 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTProcTexture.m,v 1.6 2003/07/08 11:21:11 pmanna Exp $
//
// $Log: C3DTProcTexture.m,v $
// Revision 1.6  2003/07/08 11:21:11  pmanna
// Modified to use a more generic generateTexture
// Moved texture generation to public
//
// Revision 1.5  2003/07/04 13:11:00  pmanna
// Consolidated color interpolation
//
// Revision 1.4  2003/06/30 05:30:13  pmanna
// Prepared code to be common with AnimatedProcTexture
//
// Revision 1.3  2003/06/22 09:39:36  pmanna
// Refactoring of texture objects to consolidate common tasks
//
// Revision 1.2  2003/06/19 20:36:33  pmanna
// Added Apple extension to optimize memory usage
//
// Revision 1.1  2003/06/17 07:06:48  pmanna
// Added code for Perlin noise & modiified Tutorial 2
//
//

#import "C3DTProcTexture.h"


@interface C3DTProcTexture (PrivateMethods)
- (BOOL)_createCheckerTextureWithSize: (NSSize)aSize fromColor: (_C3DTVector)startColor toColor: (_C3DTVector)endColor
                             cellSize: (int)bits;
@end


@implementation C3DTProcTexture

+ (C3DTProcTexture * )perlinTextureWithSize: (NSSize)aSize
                                  fromColor: (_C3DTVector)startColor
                                    toColor: (_C3DTVector)endColor
                                    density: (float)aDensity
                                persistence: (float)aPersistence
                                    octaves: (int)octaves
{
    return [[[C3DTProcTexture alloc] initTextureWithType: PERLIN_TYPE size: aSize
                                               fromColor: startColor toColor: endColor
                                                 density: aDensity persistence: aPersistence
                                                 octaves: octaves] autorelease];
}

+ (C3DTProcTexture * )waveTextureWithSize: (NSSize)aSize
                                fromColor: (_C3DTVector)startColor
                                  toColor: (_C3DTVector)endColor
                                  density: (float)aDensity
                              persistence: (float)aPersistence
                                  octaves: (int)octaves
{
    return [[[C3DTProcTexture alloc] initTextureWithType: WAVE_TYPE size: aSize
                                               fromColor: startColor toColor: endColor
                                                 density: aDensity persistence: aPersistence
                                                 octaves: octaves] autorelease];
}

+ (C3DTProcTexture * )grainTextureWithSize: (NSSize)aSize
                                 fromColor: (_C3DTVector)startColor
                                   toColor: (_C3DTVector)endColor
                                   density: (float)aDensity
                               persistence: (float)aPersistence
                                   octaves: (int)octaves
{
    return [[[C3DTProcTexture alloc] initTextureWithType: GRAIN_TYPE size: aSize
                                               fromColor: startColor toColor: endColor
                                                 density: aDensity persistence: aPersistence
                                                 octaves: octaves] autorelease];
}

+ (C3DTProcTexture * )checkerTextureWithSize: (NSSize)aSize
                                 fromColor: (_C3DTVector)startColor
                                   toColor: (_C3DTVector)endColor
                                   cellSize: (int)octaves
{
    return [[[C3DTProcTexture alloc] initTextureWithType: CHECKER_TYPE size: aSize
                                               fromColor: startColor toColor: endColor
                                                 density: 0.0 persistence: 0.0
                                                 octaves: octaves] autorelease];
}

- (id)initTextureWithType: (procTextureType)aType size: (NSSize)aSize
                fromColor: (_C3DTVector)startColor toColor: (_C3DTVector)endColor
                  density: (float)aDensity persistence: (float)aPersistence octaves: (int)octaves
{
    BOOL	success	= NO;
    
    if (self = [super initWithName: [NSString stringWithFormat: @"ProcTexture-%d-%.0fx%.0f",
        aType, aSize.width, aSize.height]]) {
        _C3DTVector	totalSize;
        
        _color.x = _color.y = _color.z = _color.w = 1.0;
        
        totalSize.x		= aSize.width;
        totalSize.y		= aSize.height;
        totalSize.z		= 1.0;
        
        _textureType	= aType;
        _startColor		= startColor;
        _endColor		= endColor;
        _density		= aDensity;
        _persistence	= aPersistence;
        _octaves		= octaves;
        
        switch (aType) {
            case PERLIN_TYPE:
                success = [self createPerlinTextureWithSize: totalSize
                                                  fromColor: startColor
                                                    toColor: endColor
                                                    density: aDensity
                                                persistence: aPersistence
                                                     octaves: octaves];
                break;
            case WAVE_TYPE:
                success = [self createWaveTextureWithSize: totalSize
                                                fromColor: startColor
                                                  toColor: endColor
                                                  density: aDensity
                                              persistence: aPersistence
                                                  octaves: octaves];
                break;
            case GRAIN_TYPE:
                success = [self createGrainTextureWithSize: totalSize
                                                 fromColor: startColor
                                                   toColor: endColor
                                                   density: aDensity
                                               persistence: aPersistence
                                                   octaves: octaves];
                break;
            case CHECKER_TYPE:
                success = [self _createCheckerTextureWithSize: aSize
                                                    fromColor: startColor
                                                      toColor: endColor
                                                     cellSize: octaves];
                break;
        }
        
        if (success)
        {
            _textureFormat = GL_RGBA;

            [self generateTextureWithBytes: _textureBytes refresh: NO];
        }
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeInt: _textureType forKey: @"ProcTextureType"];
    [coder encodeFloat: _textureSize.width forKey: @"ProcTextureWidth"];
    [coder encodeFloat: _textureSize.height forKey: @"ProcTextureHeight"];
    [coder encodeFloat: _density forKey: @"ProcTextureDensity"];
    [coder encodeFloat: _persistence forKey: @"ProcTexturePersistence"];
    [coder encodeInt: _octaves forKey: @"ProcTextureOctaves"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_startColor length: sizeof(_C3DTVector) forKey: @"ProcTextureStartColor"];
        [coder encodeBytes: (const uint8_t *)&_endColor length: sizeof(_C3DTVector) forKey: @"ProcTextureEndColor"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_startColor length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_endColor length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;
    BOOL		success	= NO;
    _C3DTVector	totalSize;

    self = [super initWithCoder: coder];

    _textureType		= [coder decodeIntForKey: @"ProcTextureType"];
    _textureSize.width	= [coder decodeFloatForKey: @"ProcTextureWidth"];
    _textureSize.height	= [coder decodeFloatForKey: @"ProcTextureHeight"];
    _density			= [coder decodeFloatForKey: @"ProcTextureDensity"];
    _persistence		= [coder decodeFloatForKey: @"ProcTexturePersistence"];
    _octaves			= [coder decodeIntForKey: @"ProcTextureOctaves"];
    if ( [coder allowsKeyedCoding] ) {
        _startColor	= *(_C3DTVector *)[coder decodeBytesForKey: @"ProcTextureStartColor" returnedLength: &len];
        _endColor	= *(_C3DTVector *)[coder decodeBytesForKey: @"ProcTextureEndColor" returnedLength: &len];
    } else {
        _startColor	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _endColor	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }

    totalSize.x		= _textureSize.width;
    totalSize.y		= _textureSize.height;
    totalSize.z		= 1.0;

    switch (_textureType) {
        case PERLIN_TYPE:
            success = [self createPerlinTextureWithSize: totalSize
                                              fromColor: _startColor
                                                toColor: _endColor
                                                density: _density
                                            persistence: _persistence
                                                octaves: _octaves];
            break;
        case WAVE_TYPE:
            success = [self createWaveTextureWithSize: totalSize
                                            fromColor: _startColor
                                              toColor: _endColor
                                              density: _density
                                          persistence: _persistence
                                              octaves: _octaves];
            break;
        case GRAIN_TYPE:
            success = [self createGrainTextureWithSize: totalSize
                                             fromColor: _startColor
                                               toColor: _endColor
                                               density: _density
                                           persistence: _persistence
                                               octaves: _octaves];
            break;
        case CHECKER_TYPE:
            success = [self _createCheckerTextureWithSize: _textureSize
                                                fromColor: _startColor
                                                  toColor: _endColor
                                                 cellSize: _octaves];
            break;
    }

    if (success)
    {
        _textureFormat = GL_RGBA;

        [self generateTextureWithBytes: _textureBytes refresh: NO];
    }

    return self;
}

inline GLuint interpolateColor(float value, _C3DTVector startColor, _C3DTVector endColor)
{
    GLuint	red		= (GLubyte)((startColor.x + (endColor.x - startColor.x) * value) * 255.0);
    GLuint	green	= (GLubyte)((startColor.y + (endColor.y - startColor.y) * value) * 255.0);
    GLuint	blue	= (GLubyte)((startColor.z + (endColor.z - startColor.z) * value) * 255.0);
    GLuint	alpha	= (GLubyte)((startColor.w + (endColor.w - startColor.w) * value) * 255.0);
    
    return (red << 24) | (green << 16) | (blue << 8) | alpha;
}

- (BOOL)allocateTextureBytesForSize: (_C3DTVector)aSize
{
    GLint				maxTextureSize;

    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);

    _textureSize.width	= aSize.x;
    _textureSize.height	= aSize.y;

    // Check if texture dimensions are valid
    if (((int)_textureSize.width > maxTextureSize) || ((int)_textureSize.height > maxTextureSize) ||
        ((int)_textureSize.width <= 0) || ((int)_textureSize.height <= 0) ||
        (!isPowerOfTwo((int)_textureSize.width)) || (!isPowerOfTwo((int)_textureSize.height))) {
        // !!!:pmanna:20030616 Generate aprropriate error
        return NO;
    }

    _textureBytes	= (GLubyte * )calloc((int)_textureSize.width  * (int)_textureSize.height * (int)aSize.z, sizeof(GLuint));

    return (_textureBytes != NULL);
}

- (BOOL)createPerlinTextureWithSize: (_C3DTVector)aSize
                          fromColor: (_C3DTVector)startColor
                            toColor: (_C3DTVector)endColor
                             density: (float)aDensity
                        persistence: (float)aPersistence
                            octaves: (int)octaves
{
    if ([self allocateTextureBytesForSize: aSize]) {
        float	wavelength		= ((aSize.x > aSize.y) ? aSize.x : aSize.y) / aDensity;
        GLuint	*pTextureBytes	= (GLuint *)_textureBytes;
        int		x, y, z;
        int		maxx	= (int)aSize.x;
        int		maxy	= (int)aSize.y;
        int		maxz	= (int)aSize.z;
        
        if (!cachedNoise)	initNoiseBuffer();

        for(z = 0; z < maxz; z++) {
            for(y = 0; y < maxy; y++) {
                for(x = 0; x < maxx; x++){
                    float	value	= (maxz > 1 ?
                                   perlinNoise3d(x, y, z, maxx-1, maxy-1, maxz-1, wavelength, aPersistence, octaves) :
                                   perlinNoise2d(x, y, maxx-1, maxy-1, wavelength, aPersistence, octaves));

                    *pTextureBytes++ = interpolateColor(value, startColor, endColor);
                }
            }
        }
        return YES;
    }
    
    return NO;
}

- (BOOL)createWaveTextureWithSize: (_C3DTVector)aSize
                        fromColor: (_C3DTVector)startColor
                          toColor: (_C3DTVector)endColor
                           density: (float)aDensity
                      persistence: (float)aPersistence
                          octaves: (int)octaves
{
    if ([self allocateTextureBytesForSize: aSize]) {
        float	wavelength		= ((aSize.x > aSize.y)? aSize.x : aSize.y) / aDensity;
        GLuint	*pTextureBytes	= (GLuint *)_textureBytes;
        int		x, y, z;
        int		maxx	= (int)aSize.x;
        int		maxy	= (int)aSize.y;
        int		maxz	= (int)aSize.z;
        
        if (!cachedNoise)	initNoiseBuffer();

        for(z = 0; z < maxz; z++) {
            for(y = 0; y < maxy; y++) {
                for(x = 0; x < maxx; x++){
                    float	value	= cos(8 * M_PI * (maxz > 1 ?
                                            perlinNoise3d(x, y, z, maxx-1, maxy-1, maxz-1, wavelength, aPersistence, octaves) :
                                            perlinNoise2d(x, y, maxx-1, maxy-1, wavelength, aPersistence, octaves))) * 0.5 + 0.5;

                    *pTextureBytes++ = interpolateColor(value, startColor, endColor);
                }
            }
        }
        return YES;
    }

    return NO;
}

- (BOOL)createGrainTextureWithSize: (_C3DTVector)aSize
                         fromColor: (_C3DTVector)startColor
                           toColor: (_C3DTVector)endColor
                            density: (float)aDensity
                       persistence: (float)aPersistence
                           octaves: (int)octaves
{
    if ([self allocateTextureBytesForSize: aSize]) {
        float	wavelength		= ((aSize.x > aSize.y)? aSize.x : aSize.y) / aDensity;
        GLuint	*pTextureBytes	= (GLuint *)_textureBytes;
        int		x, y, z;
        int		maxx	= (int)aSize.x;
        int		maxy	= (int)aSize.y;
        int		maxz	= (int)aSize.z;
        
        if (!cachedNoise)	initNoiseBuffer();

        for(z = 0; z < maxz; z++) {
            for(y = 0; y < maxy; y++) {
                for(x = 0; x < maxx; x++){
                    float	noise	= 7.0 * (maxz > 1 ?
                                         perlinNoise3d(x, y, z, maxx-1, maxy-1, maxz-1, wavelength, aPersistence, octaves) :
                                         perlinNoise2d(x, y, maxx-1, maxy-1, wavelength, aPersistence, octaves));
                    float	value	= noise - (float)((int)noise);
                    
                    *pTextureBytes++ = interpolateColor(value, startColor, endColor);
                }
            }
        }
        return YES;
    }

    return NO;
}

@end

@implementation C3DTProcTexture (PrivateMethods)

- (BOOL)_createCheckerTextureWithSize: (NSSize)aSize fromColor: (_C3DTVector)startColor toColor: (_C3DTVector)endColor
                             cellSize: (int)cellbits
{
    _C3DTVector	totalSize;

    totalSize.x		= aSize.width;
    totalSize.y		= aSize.height;
    totalSize.z		= 1.0;
    
    if ([self allocateTextureBytesForSize: totalSize]) {
        GLuint	*pTextureBytes	= (GLuint *)_textureBytes;
        int		x, y;
        
        if (!cachedNoise)	initNoiseBuffer();

        for(y = 0; y < (int)aSize.height; y++) {
            for(x = 0; x < (int)aSize.width; x++) {
                BOOL	value	= (((x >> cellbits) + (y >> cellbits)) & 1);
                GLuint	red		= (GLubyte)((value ? endColor.x : startColor.x) * 255.0);
                GLuint	green	= (GLubyte)((value ? endColor.y : startColor.y) * 255.0);
                GLuint	blue	= (GLubyte)((value ? endColor.z : startColor.z) * 255.0);
                GLuint	alpha	= (GLubyte)((value ? endColor.w : startColor.w) * 255.0);

                *pTextureBytes++ = (red << 24) | (green << 16) | (blue << 8) | alpha;
            }
        }

        return YES;
    }

    return NO;
}

@end

