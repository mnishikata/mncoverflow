//
//  C3DTTexture.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue May 20 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTTexture.m,v 1.8 2003/07/08 11:19:10 pmanna Exp $
//
// $Log: C3DTTexture.m,v $
// Revision 1.8  2003/07/08 11:19:10  pmanna
// Modified to use a more generic generateTexture
//
// Revision 1.7  2003/06/29 20:22:40  pmanna
// Moved nextPowerOf2 to public
//
// Revision 1.6  2003/06/22 09:39:36  pmanna
// Refactoring of texture objects to consolidate common tasks
//
// Revision 1.5  2003/06/19 20:36:33  pmanna
// Added Apple extension to optimize memory usage
//
// Revision 1.4  2003/06/17 07:06:48  pmanna
// Added code for Perlin noise & modiified Tutorial 2
//
// Revision 1.3  2003/06/15 12:47:55  pmanna
// Added the possibility for source to have alpha channel
//
// Revision 1.2  2003/06/15 09:40:06  pmanna
// Moved all texture initialization here
//
// Revision 1.1.1.1  2003/06/10 18:09:25  pmanna
// Initial import
//
//

#import "C3DTTexture.h"
#import "C3DTGeometry.h"

@interface C3DTTexture (PrivateMethods)
- (BOOL)_loadTexture: (NSString *)bitmapFile;
@end

int nextPowerOf2(int value)
{
    short	shift		= 0;
    int		newValue	= value;

    if (!(value & (value - 1)))
        return value;   // Already is power of 2

    while (newValue)
    {
        newValue >>= 1;
        shift++;
    }

    newValue = 1;
    while (shift)
    {
        newValue <<= 1;
        shift--;
    }

    return newValue;
}

@implementation C3DTTexture

+ (C3DTTexture *)textureWithFile: (NSString *)aName
{
    return [[[C3DTTexture alloc] initWithFile: aName] autorelease];
}

- (id)initWithFile: (NSString *)aName
{
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];

    if (aName == nil)
        return nil;

    if (self = [super initWithName: aName]) {
        _color.x = _color.y = _color.z = _color.w = 1.0;
        
        if( [self _loadTexture: [resourcePath stringByAppendingFormat:@"/%@", aName ]])
        {
            [self generateTextureWithBytes: _textureBytes refresh: NO];
        }
    }

    return self;
}

- (void)dealloc
{
    if (_textureBytes)
        glDeleteTextures(1, &_textureId);

    free(_textureBytes);
    
    [super dealloc];
}
- (id)initWithBitmapImageRep: (NSBitmapImageRep *)imageRep
{
	
	
    if (self = [super initWithName: @"imageRep"]) {
        _color.x = _color.y = _color.z = _color.w = 1.0;
        
        if( [self bitmapFromImageRep: imageRep] )
        {
            [self generateTextureWithBytes: _textureBytes refresh: NO];
        }
    }
	
    return self;
}

- (void)generateTextureWithBitmapImageRep: (NSBitmapImageRep *)imageRep
{
	if( [self bitmapFromImageRep: imageRep] )
	{
		[self generateTextureWithBytes: _textureBytes refresh: NO];
	}
	
}


- (void)generateTextureWithBytes: (void *)texBytes refresh: (BOOL)isRefresh
{
    if (!isRefresh)
        glGenTextures( 1, &_textureId );   // Create the textures

    glBindTexture( GL_TEXTURE_2D, _textureId );
    
    // TextureRange sample from Apple: avoids copying data back and forth, ~70% improvement
    glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, 1);
    // !!!:pmanna:20030523 This would have probably to be parametrized, to allow mipmap, etc.
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Setup sphere mapping for S & T
    glTexGeni( GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP );
    glTexGeni( GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP );

    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    if (isRefresh)
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,  _textureSize.width,
                        _textureSize.height, _textureFormat, GL_UNSIGNED_BYTE,
                        texBytes);
    else
        glTexImage2D( GL_TEXTURE_2D, 0, _textureFormat, _textureSize.width,
                      _textureSize.height, 0, _textureFormat,
                      GL_UNSIGNED_BYTE, texBytes);
    
    // Reset the texture, we want to be clean going out
    glBindTexture( GL_TEXTURE_2D, 0 );
}

- (BOOL)bitmapFromImageRep: (NSBitmapImageRep *)theImage
{
    int					bitsPPixel, bytesPRow;
    GLubyte				*theImageData;
    GLint				maxTextureSize = 0;
    int					rowNum, destRowNum;

    bitsPPixel	= [theImage bitsPerPixel];
    bytesPRow	= [theImage bytesPerRow];

    if (bitsPPixel == 24)        // No alpha channel
        _textureFormat = GL_RGB;
    else if( bitsPPixel == 32 )   // There is an alpha channel
        _textureFormat = GL_RGBA;

    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);

    _textureSize.width	= [theImage pixelsWide];
    _textureSize.height	= [theImage pixelsHigh];

    // Check if texture dimensions are valid
    if (((int)_textureSize.width > maxTextureSize) || ((int)_textureSize.height > maxTextureSize) ||
        (!isPowerOfTwo((int)_textureSize.width)) || (!isPowerOfTwo((int)_textureSize.height))) {
        // !!!:pmanna:20030523 Generate aprropriate error
        return NO;
    }

    _textureBytes		= calloc(bytesPRow * (int)_textureSize.height, 1);

    if (_textureBytes)
    {
        theImageData = [theImage bitmapData];

        for (rowNum = (int)_textureSize.height - 1, destRowNum = 0;
             rowNum >= 0;
             rowNum--, destRowNum++)
        {
            // Copy the entire row in one shot
            memcpy( _textureBytes + (destRowNum * bytesPRow),
                    theImageData + (rowNum * bytesPRow),
                    bytesPRow);
        }

        return YES;
    }

    return NO;
}

- (void)apply
{
    GLuint	textureToApply = (_textureBytes ? _textureId : 0);
    
    glGetFloatv(GL_CURRENT_COLOR, (GLfloat *)&_savedColor);
    glColor4fv((GLfloat *)&_color);

    glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint *)&_savedTextureId);

    // Switching textures is expensive, thus we operate only when needed
    if (textureToApply != _savedTextureId) {
        _textureSaved = YES;
        glBindTexture(GL_TEXTURE_2D, textureToApply);
    } else {
        _textureSaved = NO;
    }
}

- (void)restore
{
    if (_textureSaved)
        glBindTexture(GL_TEXTURE_2D, _savedTextureId);

    glColor4fv((GLfloat *)&_savedColor);
}

- (_C3DTVector)color;
{
    return _color;
}

- (void)setColor: (_C3DTVector)aColor
{
    _color = aColor;
}

- (void)setColorRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue
{
    _color.x = redColor;
    _color.y = greenColor;
    _color.z = blueColor;
    _color.w = alphaValue;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_color length: sizeof(_C3DTVector) forKey: @"TextureColor"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_color length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    if ( [coder allowsKeyedCoding] ) {
        _color	= *(_C3DTVector *)[coder decodeBytesForKey: @"TextureColor" returnedLength: &len];
    } else {
        _color	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }
    
    if( [self _loadTexture: [[[NSBundle mainBundle] resourcePath] stringByAppendingFormat:@"/%@", [self name]]])
    {
        [self generateTextureWithBytes: _textureBytes refresh: NO];
    }
    
    return self;
}
-(unsigned char*)textureBytes
{
	return _textureBytes;
}
@end

@implementation C3DTTexture (PrivateMethods)

- (BOOL)_loadTexture: (NSString *)bitmapFile
{
    NSBitmapImageRep	*theImage;

    if (theImage = [NSBitmapImageRep imageRepWithContentsOfFile: bitmapFile])
    {
        return [self bitmapFromImageRep: theImage]; 
    }

    return NO;
}

@end
