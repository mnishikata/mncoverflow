//
//  MNNodeObject.h
//  MNC3DT
//
//  Created by Masatoshi Nishikata on 08/02/28.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Functions.h"

#import "C3DTEntity.h"
#import "C3DTCamera.h"
#import "C3DTPlainSurface.h"
#import "C3DTView.h"
#import "C3DTTypes.h"
#import "FlowView.h"


@interface MNNodeObject : C3DTEntity {

	int n;
	
	FlowView *flowView;
	
	
	CoverCoordinate coordinate;
	
	double Nmax;
	int R;
	
	// other properties
	BOOL isSelected;
	BOOL isPreviouslySelected;
	
	
	
	// animation properties
	
	double animatingProgress;
	double animatingProgressOffset;

	double Ns;
	double Ns_previous;
	CoverCoordinate animationStartCoordinate; //  angle is excluded
	//CoverCoordinate animationEndCoordinate;
	BOOL rotateClockwise;
	

	NSPointPointer pointsOnView;
	
	BOOL textureUpdated;
	
}
+ (MNNodeObject *)entityWithStyle: (C3DTStyle *)aStyle;
+ (MNNodeObject *)entityWithStyle: (C3DTStyle *)aStyle geometry: (C3DTGeometry *)aGeometry;
- (id)initWithStyle: (C3DTStyle *)aStyle;
- (void)useWith: (id)anObject;
- (id) init ;
-(void)setNmax:(double)newNmax;
-(void)setR:(int)newR;
-(BOOL)isVisible;
-(void)restoreAngle;
-(void)storeAngle;
-(void)addCoordinate:(CoverCoordinate)c;
-(void)rChanged;
-(void)updateTexture:(id)sender;
-(void)dealloc;
-(void)setFlowView:(FlowView*)aView;
-(void)updateSelectionState;
-(BOOL)isSelected;
-(BOOL)isPreviouslySelected;
-(double)__spacer;
-(void)setAnimation:(double)newNs previousNs:(double)newNs_previous;
-(void)animate:(double)time;
-(void)setZ:(double)z;
-(void)setRotation:(double)rotation;
-(void)setSpacing:(double)spacing;
-(void)setCoordinate:(CoverCoordinate)c;
-(CoverCoordinate)coordinate;
-(void)setRotation:(double)newRotation;
-(void)setZ:(double)newZ;
-(int)n;
-(double)N;
- (NSComparisonResult)comparePosition:(MNNodeObject*)obj;
-(void)drawRound:(NSValue*)val;
-(void)projectedPlainVerticesWithCamera:(C3DTCamera*)camera intoPoints:(NSPointPointer)points inView:(C3DTView*)view;
- (NSPoint)convertToWindowCoords:(_C3DTVector)objVector;
- (_C3DTVector)convertFromWindowCoords:(NSPoint)localPoint ;
-(double)rotationAngle;
-(void)addCoordinate:(CoverCoordinate)c inAnimation:(BOOL)flag;

@end
