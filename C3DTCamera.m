//
//  C3DTCamera.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTCamera.m,v 1.4 2003/06/29 13:57:14 pmanna Exp $
//
// $Log: C3DTCamera.m,v $
// Revision 1.4  2003/06/29 13:57:14  pmanna
// Added support for object picking
//
// Revision 1.3  2003/06/24 22:30:50  pmanna
// Moved position to spherical coordinates to avoid gimbal lock
//
// Revision 1.2  2003/06/14 09:23:24  pmanna
// Added option for orthographic view
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTCamera.h"
#import <OpenGL/glu.h>

@implementation C3DTCamera

- (id)initWithBounds: (NSRect)boundsRect
{
    if ((self = [super init]) != nil) {
        [self setBounds: boundsRect];
        [self setFov: 45.0f];
    }

    return self;
}

- (_C3DTVector)pos
{
    return vectorAdd(sphericalToCartesian(_pos), _lookAt);
}

- (void)setPos: (_C3DTVector)newPos
{
    //_pos = newPos;
    _pos = cartesianToSpherical(vectorSubtract(newPos, _lookAt));
}

- (void)setPosX: (float)x Y: (float)y Z: (float)z
{
    _C3DTVector	newPos;
    
    newPos.x	= x;
    newPos.y	= y;
    newPos.z	= z;
    newPos.w	= 0.0;	// This is mathematically NOT correct, we just ignore quaternions at this moment

    [self setPos: newPos];
}

- (_C3DTVector)lookAt
{
    return _lookAt;
}

- (void)setLookAt: (_C3DTVector)newPos
{
    _lookAt = newPos;

    // WARNING: This is necessary, but is not elegant
    [self setPos: vectorAdd(sphericalToCartesian(_pos), _lookAt)];
}

- (void)setLookAtX: (float)x Y: (float)y Z: (float)z
{
    _lookAt.x = x;
    _lookAt.y = y;
    _lookAt.z = z;
    _lookAt.w = 0.0;
    
    [self setPos: vectorAdd(sphericalToCartesian(_pos), _lookAt)];
}

- (void)rotateToAzimuth: (float)theta elevation: (float)phi distance: (float)d
{
    //_C3DTVector	newPos	= cartesianToSpherical(vectorSubtract(_pos, _lookAt));
    _C3DTVector	newPos	= _pos;
    float		pi2		= 2.0 * M_PI;

    // Limit angles to +/- 180 degrees
    if ((newPos.theta = theta) > M_PI)
        newPos.theta -= pi2;
    else if (newPos.theta < -M_PI)
        newPos.theta += pi2;

    if ((newPos.phi = phi) > M_PI) {
        newPos.phi -= pi2;
    }
    if (newPos.phi < -M_PI) {
        newPos.phi += pi2;
    }

    if ((newPos.r = d) <= 0.0)
        newPos.r = EPSILON;

    _pos = newPos;
}

- (void)rotateByDegreesX: (float)theta Y: (float)phi
{
    //_C3DTVector	newPos	= cartesianToSpherical(vectorSubtract(_pos, _lookAt));
    _C3DTVector	newPos	= _pos;

    newPos.phi		+= deg2rad(phi);
    newPos.theta	+= deg2rad(theta);

    [self rotateToAzimuth: newPos.theta elevation: newPos.phi distance: newPos.r];
}

- (float)distance
{
    return _pos.r;
}

- (void)setDistance: (float)d
{
    if ((_pos.r = d) <= 0.0)
        _pos.r = EPSILON;
}

- (_C3DTVector)frustum
{
    return _frustum;
}

- (void)setFrustum: (_C3DTVector)newPos
{
    _frustum = newPos;
}

- (void)setFrustumWidth: (float)w height: (float)h depth: (float)d
{
    _frustum.x = w ;
    _frustum.y = (h != 0.0f ? h : .000001);		// To avoid problems in calculating aspect factor
    _frustum.z = (d != 0.0f ? d : .01);			// To avoid problems in calculating view depth
    _frustum.w = 0.0;
}

- (float)fov
{
    return _fov;
}

- (void)setFov: (float)newFov
{
    if ((_fov = newFov) < 0.1)
        _fov = 0.1;
    else if (_fov > 179.0)
        _fov = 179.0;
}

- (BOOL)useOrtho
{
    return _useOrtho;
}

- (void)setUseOrtho: (BOOL)ortho
{
    _useOrtho = ortho;
}

- (BOOL)selectionMode {
    return _selectionMode;
}

- (void)setSelectionMode: (BOOL)newSelectionMode {
    _selectionMode = newSelectionMode;
}

- (NSPoint)selectionPoint {
    return _selectionPoint;
}

- (void)setSelectionPoint: (NSPoint)newSelectionPoint {
    _selectionPoint = newSelectionPoint;
}

- (NSSize)selectionSize {
    return _selectionSize;
}

- (void)setSelectionSize: (NSSize)newSelectionSize {
    _selectionSize = newSelectionSize;
}

- (void)setBounds: (NSRect)boundsRect
{
    // Set camera parameters based on the view frame
    // As a default, we put depth == height
    [self setFrustumWidth: boundsRect.size.width height: boundsRect.size.height depth: 500/*boundsRect.size.height*/];
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _fov forKey: @"CameraFov"];
    [coder encodeBool: _useOrtho forKey: @"CameraUseOrtho"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_pos length: sizeof(_C3DTVector) forKey: @"CameraPos"];
        [coder encodeBytes: (const uint8_t *)&_lookAt length: sizeof(_C3DTVector) forKey: @"CameraLookAt"];
        [coder encodeBytes: (const uint8_t *)&_frustum length: sizeof(_C3DTVector) forKey: @"CameraFrustum"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_pos length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_lookAt length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_frustum length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    _fov			= [coder decodeFloatForKey: @"CameraFov"];
    _useOrtho		= [coder decodeBoolForKey: @"CameraUseOrtho"];
    if ( [coder allowsKeyedCoding] ) {
        _pos		= *(_C3DTVector *)[coder decodeBytesForKey: @"CameraPos" returnedLength: &len];
        _lookAt		= *(_C3DTVector *)[coder decodeBytesForKey: @"CameraLookAt" returnedLength: &len];
        _frustum	= *(_C3DTVector *)[coder decodeBytesForKey: @"CameraFrustum" returnedLength: &len];
    } else {
        _pos		= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _lookAt		= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _frustum	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }
    
    return self;
}

- (void)useWith: (id)anObject
{
    //_C3DTVector	offset = cartesianToSpherical(vectorSubtract(_pos, _lookAt));
    _C3DTVector		offset = _pos;
    
    glViewport(0, 0, _frustum.x, _frustum.y);
    
    glMatrixMode( GL_PROJECTION );   // Select the projection matrix
    glLoadIdentity();                // and reset it

    if (_selectionMode) {
        GLint	viewPort[4];

        glInitNames();
        glGetIntegerv(GL_VIEWPORT, viewPort);
        gluPickMatrix(_selectionPoint.x, _selectionPoint.y,
                      _selectionSize.width, _selectionSize.height,
                      viewPort);
    }
    
    // By default, view depth starts at 1/1000th of total depth
    if (_useOrtho)
        // Sets the ortho mode
        glOrtho(-(_frustum.x / 2.0), (_frustum.x / 2.0),
                -(_frustum.y / 2.0), (_frustum.y / 2.0),
                _frustum.z / 1000.0f, _frustum.z);
    else
        // Sets the perspective mode (default)
        gluPerspective(_fov, _frustum.x / _frustum.y, _frustum.z / 1000.0f, _frustum.z);

    glMatrixMode( GL_MODELVIEW );    // Select the modelview matrix
    glLoadIdentity();                // and reset it

    // Sets the camera position ad orientation: by default, we're always standing up
    //gluLookAt( _pos.x, _pos.y, _pos.z,
    //           _lookAt.x, _lookAt.y, _lookAt.z,
    //           0.0f, 1.0f, 0.0f);

    // Translate to the looking point
    glTranslatef(_lookAt.x, _lookAt.y, _lookAt.z);

    // Move away from the center
    glTranslatef(0.0, 0.0, -offset.r);
    // Rotate elevation
    glRotatef(rad2deg(offset.phi), 1.0, 0.0, 0.0);
    // Rotate azimuth (- ｹ/2, 0 is on Z while spherical 0 is on X)
    glRotatef(rad2deg(offset.theta - M_PI_2), 0.0, 1.0, 0.0);
}

@end
