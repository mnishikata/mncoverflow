//
//  C3DTInteractiveView.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue Jun 24 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTInteractiveView.m,v 1.2 2003/06/29 13:56:47 pmanna Exp $
//
// $Log: C3DTInteractiveView.m,v $
// Revision 1.2  2003/06/29 13:56:47  pmanna
// Added object picking
//
// Revision 1.1  2003/06/24 22:29:03  pmanna
// Initial release
//
//

#import "C3DTInteractiveView.h"
#import "C3DTGroup.h"

@implementation C3DTInteractiveView

- (void)mouseDown:(NSEvent *)anEvent
{
    NSPoint		location	= [self convertPoint: [anEvent locationInWindow]
                                  fromView: [[self window] contentView]];
    C3DTCamera	*theCamera	= [self camera];

    if ([anEvent modifierFlags] & NSCommandKeyMask) {
        if (theCamera) {
            [theCamera setSelectionMode: YES];

            [theCamera setSelectionPoint: location];
            [theCamera setSelectionSize: NSMakeSize(3.0, 3.0)];

            [[self scene] useWith: theCamera];

			
            [theCamera setSelectionMode: NO];
        }


        [self setNeedsDisplay: YES];
    }
    
    [super mouseDown: anEvent];
}
-(void)setNeedsCleaning:(BOOL)flag
{
	needsCleaning = flag;	
	long  order = 0;
	[[self openGLContext] setValues:&order forParameter: NSOpenGLCPSurfaceOpacity];
	
}
-(void)drawRect:(NSRect)rect
{
	if( needsCleaning  )
	{
		// NSRect bounds = [self bounds];
		//	//NSLog(@"clearView in view");
		
		NSColor* color = [NSColor colorWithCalibratedWhite:0.0 alpha:0.5];
		[color set];
		NSRectFill(rect);
		needsCleaning = NO;
	}
	
	
	[super drawRect:rect];
	
	NSArray* selectedObjects = [[self scene] selectedObjects];
	NSEnumerator	*enumSelection = [selectedObjects objectEnumerator];
	C3DTGroup		*selectedObj;
	
	while ((selectedObj = (C3DTGroup *)[enumSelection nextObject]) != nil)
	{
		NSPointPointer points = malloc(  sizeof(NSPoint)*4 );
		[selectedObj projectedPlainVerticesWithCamera:[self camera] intoPoints:points inView:(C3DTView*)self] ;
	
		if( !(NSEqualPoints( points[0] , NSZeroPoint  )&&
			NSEqualPoints( points[1] , NSZeroPoint  )&&
			NSEqualPoints( points[2] , NSZeroPoint  )&&
			NSEqualPoints( points[3] , NSZeroPoint  ) ))
		{


			NSBezierPath* path = [NSBezierPath bezierPathWithRect:rect];
			NSColor* color = [NSColor colorWithCalibratedWhite:0.0 alpha:0.5];
			[color set];
			[path fill];
			
			[NSBezierPath setDefaultLineWidth:5.0];
			[[NSColor redColor] set];
			path = [NSBezierPath bezierPath];
			
			[path moveToPoint:points[0]];
			[path lineToPoint: points[1]];
			[path lineToPoint: points[2]];
			[path lineToPoint: points[3]];
			[path lineToPoint: points[0]];
			[path stroke];

			//[NSBezierPath strokeLineFromPoint:points[0] toPoint:points[1] ];
			//[NSBezierPath strokeLineFromPoint:points[1] toPoint:points[2] ];
			//[NSBezierPath strokeLineFromPoint:points[2] toPoint:points[3] ];
			//[NSBezierPath strokeLineFromPoint:points[3] toPoint:points[0] ];

			
		}
		
		free(points);
			
	}
					
					
}

- (void)mouseDragged:(NSEvent *)anEvent
{
    NSPoint		newLocation	= [anEvent locationInWindow];
    float		angleX, angleY;

    angleX = newLocation.x - lastClick.x;
    angleY = newLocation.y - lastClick.y;

    if ([anEvent modifierFlags] & NSAlternateKeyMask) {
        [[self camera] setDistance: [[self camera] distance] - angleY];
        [[self camera] setFov: [[self camera] fov] + angleX];
    } else {
        [[self camera] rotateByDegreesX: angleX Y: -angleY];
    }

    [self setNeedsDisplay: YES];

    [super mouseDragged: anEvent];
}
@end
