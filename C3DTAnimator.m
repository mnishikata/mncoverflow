//
//  C3DTAnimator.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Fri May 30 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTAnimator.m,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTAnimator.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTAnimator.h"


@implementation C3DTAnimator

+ (C3DTAnimator *)animatorWithInterval: (NSTimeInterval)anInterval
{
    return [[[C3DTAnimator alloc] initWithInterval: anInterval] autorelease];
}


- (id)initWithInterval: (NSTimeInterval)anInterval
{
    if (self = [super initWithName: [NSString stringWithFormat: @"Animator-%.3f", anInterval]]) {
        [self setAnimateInterval: anInterval];
    }

    return self;
}


- (void)dealloc
{
    [self stop];
    
    [super dealloc];
}

- (NSTimeInterval)animateInterval
{
    return _animateInterval;
}

- (void)setAnimateInterval: (NSTimeInterval)anInterval
{
    _animateInterval = anInterval;
}


- (void)start
{
    if (!_animateTimer && (_animateInterval > 0.0)) {
        _animateTimer	= [[NSTimer scheduledTimerWithTimeInterval: _animateInterval
                                                          target: self
                                                        selector: @selector( doAnimate: )
                                                        userInfo: nil
                                                         repeats: YES] retain];
        [[NSRunLoop currentRunLoop] addTimer: _animateTimer
                                     forMode: NSEventTrackingRunLoopMode];
        [[NSRunLoop currentRunLoop] addTimer: _animateTimer
                                     forMode: NSModalPanelRunLoopMode];
    }
}

- (void)stop
{
    if (_animateTimer) {
        [_animateTimer invalidate];
        [_animateTimer release];
        _animateTimer = nil;
    }
}


- (void)doAnimate: (NSTimer *)timer
{
    // Spawn animation message to all our children
    [self animate];

    // Tell interested views to refresh
    [[NSNotificationCenter defaultCenter] postNotificationName: C3DT_REFRESH object: self userInfo: nil];
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _animateInterval forKey: @"AnimationInterval"];
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    _animateInterval = [coder decodeFloatForKey: @"AnimationInterval"];

    return self;
}

@end
