//
//  C3DTBox.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Fri May 23 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTBox.m,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTBox.m,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTBox.h"

@implementation C3DTBox

+ (C3DTBox *)boxWithWidth: (float)aWidth height: (float)aHeight depth: (float)aDepth
{
    return [[[C3DTBox alloc] initWithWidth: aWidth
                                    height: aHeight
                                     depth: aDepth] autorelease];
}

- (id)initWithWidth: (float)aWidth height: (float)aHeight depth: (float)aDepth
{
    if ((self = [super init]) != nil) {
        _width	= aWidth;
        _height = aHeight;
        _depth	= aDepth;

        [self buildBoxWithWidth: _width
                         height: _height
                          depth: _depth];
    }

    return self;
}

- (float)width
{
    return _width;
}

- (void)setWidth: (float)aSize
{
    _width = aSize;
    
    [self buildBoxWithWidth: _width
                     height: _height
                      depth: _depth];
    
}

- (float)height
{
    return _height;
}

- (void)setHeight: (float)aSize
{
    _height = aSize;

    [self buildBoxWithWidth: _width
                     height: _height
                      depth: _depth];

}

- (float)depth
{
    return _depth;
}

- (void)setDepth: (float)aSize
{
    _depth = aSize;

    [self buildBoxWithWidth: _width
                     height: _height
                      depth: _depth];

}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _width forKey: @"BoxWidth"];
    [coder encodeFloat: _height forKey: @"BoxHeight"];
    [coder encodeFloat: _depth forKey: @"BoxDepth"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder: coder];

    _width = [coder decodeFloatForKey: @"BoxWidth"];
    _height = [coder decodeFloatForKey: @"BoxHeight"];
    _depth = [coder decodeFloatForKey: @"BoxDepth"];

    [self buildBoxWithWidth: _width
                     height: _height
                      depth: _depth];
    return self;
}

- (void)buildBoxWithWidth: (float)aWidth height: (float)aHeight depth: (float)aDepth
{
    // Pre-calculated normals
    
    static GLfloat	n[6][3] =
    {
    {-1.0, 0.0, 0.0},	// Left
    {0.0, -1.0, 0.0},	// Bottom
    {0.0, 0.0, -1.0},	// Back
    {1.0, 0.0, 0.0},	// Right
    {0.0, 1.0, 0.0},	// Top
    {0.0, 0.0, 1.0}		// Front
    };
    
    // Texture
    // !!!:pmanna:20030522 To verify the mapping! Some faces look "right", others not...
    static GLfloat	t[6][4][2] =
    {
    {{0.0, 0.0}, {1.0, 0.0}, {1.0, 1.0}, {0.0, 1.0}},
    {{1.0, 0.0}, {1.0, 1.0}, {0.0, 1.0}, {0.0, 0.0}},
    {{1.0, 1.0}, {0.0, 1.0}, {0.0, 0.0}, {1.0, 0.0}},
    {{0.0, 1.0}, {0.0, 0.0}, {1.0, 0.0}, {1.0, 1.0}},
    {{0.0, 0.0}, {1.0, 0.0}, {1.0, 1.0}, {0.0, 1.0}},
    {{1.0, 0.0}, {1.0, 1.0}, {0.0, 1.0}, {0.0, 0.0}}
    };
    
    // Definition of indexes for the faces
    static GLubyte	f[6][4] =
    {
    {0, 1, 2, 3},
    {4, 5, 1, 0},
    {7, 4, 0, 3},
    {7, 6, 5, 4},
    {3, 2, 6, 7},
    {5, 6, 2, 1}
    };

    // Definition of the vertices
    GLfloat			v[8][3];

    v[0][0] = v[1][0] = v[2][0] = v[3][0] = -aWidth / 2;	// Left
    v[4][0] = v[5][0] = v[6][0] = v[7][0] = aWidth / 2;		// Right
    v[0][1] = v[1][1] = v[4][1] = v[5][1] = -aHeight / 2;	// Bottom
    v[2][1] = v[3][1] = v[6][1] = v[7][1] = aHeight / 2;	// Top
    v[0][2] = v[3][2] = v[4][2] = v[7][2] = -aDepth / 2;	// Back
    v[1][2] = v[2][2] = v[5][2] = v[6][2] = aDepth / 2;		// Front
    
    [self startDisplayList];
    
    GLbyte			ii;
    
    glBegin(GL_QUADS);
    for (ii = 5; ii >= 0; ii--) {
        glNormal3fv(n[ii]);
        glTexCoord2fv(t[ii][0]);	glVertex3fv(v[f[ii][0]]);
        glTexCoord2fv(t[ii][1]);	glVertex3fv(v[f[ii][1]]);
        glTexCoord2fv(t[ii][2]);	glVertex3fv(v[f[ii][2]]);
        glTexCoord2fv(t[ii][3]);	glVertex3fv(v[f[ii][3]]);
    }
    glEnd();
    
    [self stopDisplayList];
}

@end
