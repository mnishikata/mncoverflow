//
//  C3DTTransform.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTTransform.m,v 1.2 2003/06/29 13:55:13 pmanna Exp $
//
// $Log: C3DTTransform.m,v $
// Revision 1.2  2003/06/29 13:55:13  pmanna
// Added handling of picking objects
//
// Revision 1.1.1.1  2003/06/10 18:09:25  pmanna
// Initial import
//
//

#import "C3DTTransform.h"
#import "C3DTCamera.h"
#import "C3DTColor.h"

@interface C3DTTransform (PrivateMethods)

- (BOOL)checkTransform;

@end

@implementation C3DTTransform

- (id)init
{
    if ((self = [super init]) != nil) {
        _rotationCenter.x = _rotationCenter.y = _rotationCenter.z = _rotationCenter.w = 0.0f;
        _rotation.x = _rotation.y = _rotation.z = _rotation.w = 0.0f;
        _translation.x = _translation.y = _translation.z = _translation.w = 0.0f;
        _scaling.x = _scaling.y = _scaling.z = 1.0f;
        _scaling.w = 0.0f;
    }

    return self;
}

- (void)dealloc
{
    [_style release];

    [super dealloc];
}

- (id)initWithStyle: (C3DTStyle *)aStyle
{
    if ([self init] != nil) {
        if (aStyle != nil)
            [self setStyle: aStyle];
    }

    return self;
}

- (void)setParent: (C3DTNode *)newParent
{
    [super setParent: newParent];

    if (_style == nil) {
        if ([_parent isKindOfClass: [C3DTTransform class]])
            [self setStyle: [(C3DTTransform *)_parent style]];
        else
            [self setStyle: [C3DTColor defaultColor]];
    }
}

- (_C3DTVector)rotationCenter
{
    return _rotationCenter;
}

- (void)setRotationCenter: (_C3DTVector)aRotationCenter
{
    _rotationCenter = aRotationCenter;
}

- (void)setRotationCenterX: (float)xPos Y: (float)yPos Z: (float)zPos
{
    _rotationCenter.x = xPos;
    _rotationCenter.y = xPos;
    _rotationCenter.z = xPos;
    _rotationCenter.w = 0.0f;
}

- (_C3DTVector)rotation
{
    return _rotation;
}

- (void)setRotation: (_C3DTVector)aRotation
{
    _rotation = aRotation;

    _hasTransform = [self checkTransform];
}

- (void)setRotationX: (float)xAngle Y: (float)yAngle Z: (float)zAngle
{
    _rotation.x = fmod(xAngle, 360.0f);
    _rotation.y = fmod(yAngle, 360.0f);
    _rotation.z = fmod(zAngle, 360.0f);
    _rotation.w = 0.0f;

    _hasTransform = [self checkTransform];
}

- (_C3DTVector)translation
{
    return _translation;
}

- (void)setTranslation: (_C3DTVector)aTranslation
{
    _translation = aTranslation;

    _hasTransform = [self checkTransform];
}

- (void)setTranslationX: (float)xDist Y: (float)yDist Z: (float)zDist
{
    _translation.x = xDist;
    _translation.y = yDist;
    _translation.z = zDist;
    _translation.w = 0.0f;

    _hasTransform = [self checkTransform];
}

- (_C3DTVector)scaling
{
    return _scaling;
}

- (void)setScaling: (_C3DTVector)aScaling
{
    _scaling = aScaling;

    _hasTransform = [self checkTransform];
}

- (void)setScalingX: (float)xScale Y: (float)yScale Z: (float)zScale
{
    _scaling.x = xScale;
    _scaling.y = yScale;
    _scaling.z = zScale;
    _scaling.w = 0.0f;

    _hasTransform = [self checkTransform];
}

- (C3DTStyle *)style
{
    return _style;
}

- (void)setStyle: (C3DTStyle *)aStyle
{
    [aStyle retain];
    [_style release];

    _style = aStyle;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeBool: _hasTransform forKey: @"HasTransform"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_rotationCenter length: sizeof(_C3DTVector) forKey: @"TransformRotCenter"];
        [coder encodeBytes: (const uint8_t *)&_rotation length: sizeof(_C3DTVector) forKey: @"TransformRotation"];
        [coder encodeBytes: (const uint8_t *)&_translation length: sizeof(_C3DTVector) forKey: @"TransformTranslation"];
        [coder encodeBytes: (const uint8_t *)&_scaling length: sizeof(_C3DTVector) forKey: @"TransformScaling"];
        [coder encodeObject: _style forKey: @"TransformStyle"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_rotationCenter length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_rotation length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_translation length: sizeof(_C3DTVector)];
        [coder encodeBytes: (const uint8_t *)&_scaling length: sizeof(_C3DTVector)];
        [coder encodeObject: _style];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;

    self = [super initWithCoder: coder];

    _hasTransform = [coder decodeBoolForKey: @"HasTransform"];
    if ( [coder allowsKeyedCoding] ) {
        _rotationCenter	= *(_C3DTVector *)[coder decodeBytesForKey: @"TransformRotCenter" returnedLength: &len];
        _rotation		= *(_C3DTVector *)[coder decodeBytesForKey: @"TransformRotation" returnedLength: &len];
        _translation	= *(_C3DTVector *)[coder decodeBytesForKey: @"TransformTranslation" returnedLength: &len];
        _scaling		= *(_C3DTVector *)[coder decodeBytesForKey: @"TransformScaling" returnedLength: &len];
        _style			= [[coder decodeObjectForKey: @"TransformStyle"] retain];
    } else {
        _rotationCenter	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _rotation		= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _translation	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _scaling		= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
        _style			= [[coder decodeObject] retain];
    }

    return self;
}

- (void)useWith: (id)anObject
{
    C3DTCamera	*camera	= (C3DTCamera *)anObject;
    
    if (_hasTransform) {
        glPushMatrix();

        // As we are using an absolute coord system, transformations are applied reversed
        
        // Translation
        if ((_translation.x != 0.0) || (_translation.y != 0.0) || (_translation.z != 0.0)) {
            glTranslatef(_translation.x, _translation.y, _translation.z);
        }

        // Scaling
        if ((_scaling.x != 1.0) || (_scaling.y != 1.0) || (_scaling.z != 1.0)) {
            glScalef(_scaling.x, _scaling.y, _scaling.z);
        }
        
        // Rotation
        glTranslatef(-_rotationCenter.x, -_rotationCenter.y, -_rotationCenter.z);

        if (_rotation.x != 0.0)
            glRotatef(_rotation.x, 1.0f, 0.0f, 0.0f);
        if (_rotation.y != 0.0)
            glRotatef(_rotation.y, 0.0f, 1.0f, 0.0f);
        if (_rotation.z != 0.0)
            glRotatef(_rotation.z, 0.0f, 0.0f, 1.0f);

        glTranslatef(_rotationCenter.x, _rotationCenter.y, _rotationCenter.z);
    }

    if ([camera selectionMode]) {
        GLint	entityName;

        sscanf([[self name] cString], "Node-%ld", &entityName);
        glPushName(entityName);
    }
    else
        [_style apply];

    [super useWith: anObject];

    if ([camera selectionMode])
        glPopName();
    else
        [_style restore];
    
    if (_hasTransform) {
        glPopMatrix();
    }
}

@end

@implementation C3DTTransform (PrivateMethods)

- (BOOL)checkTransform
{
    return ((_rotation.x != 0.0) || (_rotation.y != 0.0) || (_rotation.z != 0.0) ||
            (_translation.x != 0.0) || (_translation.y != 0.0) || (_translation.z != 0.0) ||
            (_scaling.x != 1.0) || (_scaling.y != 1.0) || (_scaling.z != 1.0));
}

@end
