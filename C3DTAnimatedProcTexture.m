//
//  C3DTAnimatedProcTexture.m
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Mon Jul 07 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTAnimatedProcTexture.m,v 1.1 2003/07/08 11:25:19 pmanna Exp $
//
// $Log: C3DTAnimatedProcTexture.m,v $
// Revision 1.1  2003/07/08 11:25:19  pmanna
// Initial import in CVS
//
//

#import "C3DTAnimatedProcTexture.h"



@implementation C3DTAnimatedProcTexture

+ (C3DTAnimatedProcTexture * )perlinTextureWithSize: (_C3DTVector)aSize
                                          fromColor: (_C3DTVector)startColor
                                            toColor: (_C3DTVector)endColor
                                            density: (float)aDensity
                                        persistence: (float)aPersistence
                                            octaves: (int)octaves
                                           interval: (NSTimeInterval)anInterval
{
    return [[[C3DTAnimatedProcTexture alloc] initTextureWithType: PERLIN_TYPE size: aSize
                                                       fromColor: startColor toColor: endColor
                                                         density: aDensity persistence: aPersistence
                                                         octaves: octaves interval: anInterval] autorelease];
}

+ (C3DTAnimatedProcTexture * )waveTextureWithSize: (_C3DTVector)aSize
                                        fromColor: (_C3DTVector)startColor
                                          toColor: (_C3DTVector)endColor
                                          density: (float)aDensity
                                      persistence: (float)aPersistence
                                          octaves: (int)octaves
                                         interval: (NSTimeInterval)anInterval
{
    return [[[C3DTAnimatedProcTexture alloc] initTextureWithType: WAVE_TYPE size: aSize
                                                       fromColor: startColor toColor: endColor
                                                         density: aDensity persistence: aPersistence
                                                         octaves: octaves interval: anInterval] autorelease];
}

+ (C3DTAnimatedProcTexture * )grainTextureWithSize: (_C3DTVector)aSize
                                 fromColor: (_C3DTVector)startColor
                                   toColor: (_C3DTVector)endColor
                                   density: (float)aDensity
                               persistence: (float)aPersistence
                                   octaves: (int)octaves
                                  interval: (NSTimeInterval)anInterval
{
    return [[[C3DTAnimatedProcTexture alloc] initTextureWithType: GRAIN_TYPE size: aSize
                                                       fromColor: startColor toColor: endColor
                                                         density: aDensity persistence: aPersistence
                                                         octaves: octaves interval: anInterval] autorelease];
}

- (id)initTextureWithType: (procTextureType)aType size: (_C3DTVector)aSize
                fromColor: (_C3DTVector)startColor toColor: (_C3DTVector)endColor
                  density: (float)aDensity persistence: (float)aPersistence
                  octaves: (int)octaves interval: (NSTimeInterval)anInterval
{
    BOOL	success	= NO;
    
    if (self = [super initWithName: [NSString stringWithFormat: @"AnimatedProcTexture-%d-%.0fx%.0fx%.0f",
        aType, aSize.x, aSize.y, aSize.y]]) {
        
        _color.x = _color.y = _color.z = _color.w = 1.0;

        _totTextureSize		= aSize;
        _textureType		= aType;
        _startColor			= startColor;
        _endColor			= endColor;
        _density			= aDensity;
        _persistence		= aPersistence;
        _octaves			= octaves;
        _animateInterval	= anInterval;
        
        switch (aType) {
            case PERLIN_TYPE:
                success = [self createPerlinTextureWithSize: aSize
                                                  fromColor: startColor
                                                    toColor: endColor
                                                    density: aDensity
                                                persistence: aPersistence
                                                     octaves: octaves];
                break;
            case WAVE_TYPE:
                success = [self createWaveTextureWithSize: aSize
                                                fromColor: startColor
                                                  toColor: endColor
                                                  density: aDensity
                                              persistence: aPersistence
                                                  octaves: octaves];
                break;
            case GRAIN_TYPE:
                success = [self createGrainTextureWithSize: aSize
                                                 fromColor: startColor
                                                   toColor: endColor
                                                   density: aDensity
                                               persistence: aPersistence
                                                   octaves: octaves];
                break;
            default:
                // Do nothing, 3D checker isn't implemented
                break;
        }
        
        if (success)
        {
            _textureFormat = GL_RGBA;

            [self generateTextureWithBytes: _textureBytes refresh: NO];
        }
    }

    return self;
}

- (void)dealloc
{
    [self stop];

    [super dealloc];
}

- (NSTimeInterval)animateInterval
{
    return _animateInterval;
}

- (void)setAnimateInterval: (NSTimeInterval)anInterval
{
    _animateInterval = anInterval;
}

- (void)start
{
    if (!_animateTimer && (_animateInterval > 0.0)) {
        _animateTimer	= [[NSTimer scheduledTimerWithTimeInterval: _animateInterval
                                                          target: self
                                                        selector: @selector( doAnimate: )
                                                        userInfo: nil
                                                         repeats: YES] retain];
        [[NSRunLoop currentRunLoop] addTimer: _animateTimer
                                     forMode: NSEventTrackingRunLoopMode];
        [[NSRunLoop currentRunLoop] addTimer: _animateTimer
                                     forMode: NSModalPanelRunLoopMode];
    }
}

- (void)stop
{
    if (_animateTimer) {
        [_animateTimer invalidate];
        [_animateTimer release];
        _animateTimer = nil;
    }
}


- (void)doAnimate: (NSTimer *)timer
{
    int		textureIncrement;
    
    if (++_animationStep >= (int)_totTextureSize.z)
        _animationStep = 0;

    textureIncrement = (long)_textureSize.width  * (long)_textureSize.height * sizeof(GLuint) * _animationStep;
    [self generateTextureWithBytes: _textureBytes + textureIncrement refresh: YES];
    
    // Tell interested views to refresh
    [[NSNotificationCenter defaultCenter] postNotificationName: C3DT_REFRESH object: self userInfo: nil];
}


- (void)encodeWithCoder:(NSCoder *)coder
{
    [super encodeWithCoder: coder];

    [coder encodeFloat: _animateInterval forKey: @"AnimatedProcTextureInterval"];
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeBytes: (const uint8_t *)&_totTextureSize length: sizeof(_C3DTVector) forKey: @"AnimatedProcTextureSize"];
    } else {
        [coder encodeBytes: (const uint8_t *)&_totTextureSize length: sizeof(_C3DTVector)];
    }

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
    unsigned	len;
    BOOL		success	= NO;

    self = [super initWithCoder: coder];

    _animateInterval	= [coder decodeFloatForKey: @"AnimatedProcTextureInterval"];
    if ( [coder allowsKeyedCoding] ) {
        _totTextureSize	= *(_C3DTVector *)[coder decodeBytesForKey: @"AnimatedProcTextureSize" returnedLength: &len];
    } else {
        _totTextureSize	= *(_C3DTVector *)[coder decodeBytesWithReturnedLength: &len];
    }

    switch (_textureType) {
        case PERLIN_TYPE:
            success = [self createPerlinTextureWithSize: _totTextureSize
                                              fromColor: _startColor
                                                toColor: _endColor
                                                density: _density
                                            persistence: _persistence
                                                octaves: _octaves];
            break;
        case WAVE_TYPE:
            success = [self createWaveTextureWithSize: _totTextureSize
                                            fromColor: _startColor
                                              toColor: _endColor
                                              density: _density
                                          persistence: _persistence
                                              octaves: _octaves];
            break;
        case GRAIN_TYPE:
            success = [self createGrainTextureWithSize: _totTextureSize
                                             fromColor: _startColor
                                               toColor: _endColor
                                               density: _density
                                           persistence: _persistence
                                               octaves: _octaves];
            break;
        default:
            // Do nothing, 3D checker isn't implemented
            break;
    }

    if (success)
    {
        _textureFormat = GL_RGBA;

        [self generateTextureWithBytes: _textureBytes refresh: NO];
    }

    return self;
}

@end

